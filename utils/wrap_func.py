from functools import wraps


def exec_col_func(func):
    """
    Description:
        데코레이터 함수로 함수 실행 전후 공통 내용 출력.
        self 사용할 수 있도록 설정.
        함수명과 self 다음 첫 번째 인자를 출력.
    """
    @wraps(func)
    def _impl(self, *args, **kwargs):
        if isinstance(args[1], dict):
            cols = list(args[1].keys())
        elif isinstance(args[1], tuple):
            cols = list(args[1])
        elif isinstance(args[1], list) or isinstance(args[1], str):
            cols = args[1]
        else:
            cols = None
        self.log.info("Process column: {0}".format(cols))
        self.log.debug("Execute function: {0} / Cols: {1}".format(func.__name__, cols))
        result = func(self, *args, **kwargs)
        return result
    return _impl