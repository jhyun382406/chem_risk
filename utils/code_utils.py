import os
import yaml
import joblib
from collections import namedtuple


def convert(
    file_path: str=None,
    dictionary: dict=None
    ):
    """
    Description:
        코드에 적용할 설정 YAML 파일 불러오기.
    Args:
        file_path, str, yaml 파일 경로 (default=None)
        dictionary, dict, 반환시킬 딕셔너리 (default=None)
    Returns:
        GenericDict: namedtuple, yaml 파일을 GenericDict로 바꾼 것(수정불가)
    """
    if file_path is not None:
        assert file_path.endswith("yaml"), "the file should be .yaml format"
        with open(file_path, "r") as f:
            dictionary = yaml.full_load(f)
    if dictionary is not None:
        for k in dictionary.keys():
            if isinstance(dictionary[k], dict):
                dictionary[k] = convert(dictionary=dictionary[k])
        return namedtuple("GenericDict", dictionary.keys())(**dictionary)


def isnamedtupleinstance(x):
    """
    Description:
        namedtuple 인지 확인.
    Args:
        x: python instance, 타입 확인할 인스턴스
    Returns:
        result: bool, True or False
    """
    _type = type(x)
    bases = _type.__bases__
    if len(bases) != 1 or bases[0] != tuple:
        return False
    fields = getattr(_type, "_fields", None)
    if not isinstance(fields, tuple):
        return False
    return all(type(i)==str for i in fields)


def unpack(obj):
    """
    Description:
        Nested instance를 각각 타입에 맞게 변환.
            - dictionary -> dictionary
            - namedtuple -> dictionary
            - etc -> original value
    Args:
        obj: python instance, namedtuple 내의 개별 객체
    Returns:
        obj: python instance, nested 가 풀린 결과
    """
    if isinstance(obj, dict):
        return {key: unpack(value) for key, value in obj.items()}
    elif isnamedtupleinstance(obj):
        return {key: unpack(value) for key, value in obj._asdict().items()}
    else:
        return obj


def flatten_iter(obj):
    """
    Description:
        List, Tuple을 풀어놓기.
    Args:
        obj: list or tuple, 개별 객체
    Returns:
        obj: list or tuple, flatten된 결과
    """
    if isinstance(obj, list):
        return [value for elm in obj for value in flatten_iter(elm)]
    elif isinstance(obj, tuple):
        return tuple([value for elm in obj for value in flatten_iter(elm)])
    else:
        return [obj]

def get_arguments(
    params: namedtuple=None
    ):
    args = {}
    for k in params._asdict().keys():
        args[k.lower()] = params._asdict()[k]
    return args


def return_dir(is_making=False, *args):
    """
    Description:
        디렉토리 경로 반환.
        해당 경로에 디렉토리가 없으면 생성.
    Args:
        is_making: bool, 실제 경로로 디렉토리 생성 여부 (default=False)
        *args: str, 디렉토리 생성할 개별 명칭
    Returns:
        dir_path: str, 디렉토리 경로
    """
    dir_path = os.path.join(*args)
    if not os.path.isdir(dir_path) and is_making:
        os.makedirs(dir_path)
    return dir_path


def convert_cols_type(cols_type):
    """
    Description:
        YAML에 저장된 dataframe 칼럼 타입 파이썬에 맞게 변환.
        namedtuple은 dict로 변환 후 반영.
    Args:
        cols_type: dict or namedtuple, YAML 파일에 저장된 칼럼 타입
    Returns:
        cols_type: dict, 파이썬에서 사용 가능한 칼럼 타입
    """
    def _convert_notation(col_type):
        if col_type == "str":
            return str
        elif col_type == "int":
            return int
        elif col_type == "float":
            return float
        else:
            return str
    
    if isinstance(cols_type, tuple):
        cols_type = cols_type._asdict()
    
    return {k: _convert_notation(v) for k, v in cols_type.items()}


def concat_dict(*dict_args):
    """
    Description:
        여러 딕셔너리를 1개의 딕셔너리로 합침.
    Args:
        *args: dict, 합치려고 하는 개별 딕셔너리
    Returns:
        total_dict: dict, 합쳐진 딕셔너리
    """
    total_dict = dict()
    for one_dict in dict_args:
        total_dict.update(one_dict)
    return total_dict


def save_pickle(instance, pkl_path):
    """
    Description:
        joilib을 이용하여 pickle 파일 저장.
    Args:
        instance: instance, pickle 파일에 저장할 객체
        pkl_path: str, pickle 파일 저장 경로
    Returns:
        None
    """
    joblib.dump(instance, pkl_path)


def load_pickle(pkl_path):
    """
    Description:
        joilib을 이용하여 pickle 파일 불러오기.
    Args:
        pkl_path: str, pickle 파일 불러올 경로
    Returns:
        None
    """
    return joblib.load(pkl_path)