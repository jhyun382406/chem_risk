"""
log 저장.

다른 모듈에서 log 및 error_log 찍을 때 사용
"""
import logging
import os
import time
from datetime import datetime
from pytz import timezone


class LogConfig():
    """
    Description:
        로그 설정값.
    """
    def __init__(self, upper_dir, file_prefix):
        """Class initial."""
        super(LogConfig, self).__init__()
        self.log_path = os.path.join(upper_dir, "log")
        self.err_log_path = os.path.join(upper_dir, "err_log")
        self.log_prefix = "{0}_log".format(file_prefix)
        self.err_log_prefix = "{0}_err_log".format(file_prefix)
        

class CustomFormatter(logging.Formatter):
    """
    Description:
        KST 시간대를 사용자 정의 포맷으로 변경.
    """
    def converter(self, timestamp):
        """
        Description:
            KST로 시간대 변경.
        Args:
            timestamp: timestamp, UTC 기준 시간대
        Returns:
            dt: timestamp, KST로 변경한 시간대
        """
        dt = datetime.fromtimestamp(timestamp)
        return dt.astimezone(timezone("Asia/Seoul"))

    def formatTime(self, record, datefmt=None):
        """
        Description:
            사용자 정의 포맷으로 시간대 변경.
        Args:
            record: record, UTC 기준 시간대 객체
            datefmt: str, 문자열로 시간대를 변경시킬 포맷 (default=None)
        Returns:
            s: str, 사용자 정의 포맷으로 변경한 시간대
        """
        dt = self.converter(record.created)
        datefmt = "%Y-%m-%d %H:%M:%S"
        if datefmt:
            s = dt.strftime(datefmt)
        else:
            try:
                s = dt.isoformat(timespec="milliseconds")
            except TypeError:
                s = dt.isoformat()
        return s


def get_log_view(lc, log_level="INFO", error_log=False):
    """
    Description:
        로그 스트림 출력 및 파일 저장.
        연/월 폴더로 저장.
        propagate 옵션으로 2번 표출 안되도록 설정.
    Args:
        lc: LogConfig, 설정 클래스
        log_level: str, 로그 레벨 (default="INFO")
            - DEBUG
            - INFO
            - WARNING
            - ERROR
            - CRITICAL
        error_log: bool, 에러로그 여부 (default=False)
    Returns:
        set_logger: logger, 로깅할 수 있는 객체
    """
    now_kst = datetime.now(timezone("Asia/Seoul"))
    log_date = now_kst.strftime("%Y%m%d")
    if error_log:
        log_path = lc.err_log_path
        log_name = lc.err_log_prefix
    else:
        log_path = lc.log_path
        log_name = lc.log_prefix
    log_path = os.path.join(log_path, log_date[:4], log_date[4:6])
    if not os.path.isdir(log_path):
        os.makedirs(log_path)
    log_file_name = "{0}_{1}.log".format(log_name, log_date)
    log_file_path = os.path.join(log_path, log_file_name)

    # 로그 레벨
    set_logger = logging.getLogger(log_name)
    if log_level == "CRITICAL":
        set_logger.setLevel(logging.CRITICAL)
    elif log_level == "ERROR":
        set_logger.setLevel(logging.ERROR)
    elif log_level == "WARNING":
        set_logger.setLevel(logging.WARNING)
    elif log_level == "DEBUG":
        set_logger.setLevel(logging.DEBUG)
    else:
        set_logger.setLevel(logging.INFO)
    set_logger.handlers = []

    # 로그 출력 형식
    log_form_front = "[%(levelname)s][%(filename)s:%(lineno)s][%(asctime)s]"
    stream_formatter = CustomFormatter(log_form_front + "%(message)s")
    
    # 스트림 출력
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(stream_formatter)

    # 파일 저장
    file_handler = logging.FileHandler(log_file_path)
    file_handler.setFormatter(stream_formatter)

    set_logger.addHandler(stream_handler)
    set_logger.addHandler(file_handler)
    set_logger.propagate = False

    return set_logger


def print_elapsed_time(start_time):
    """
    Description:
        소요시간 계산 (시, 분, 초).
    Args:
        start_time: time.time, 시작 시각
    Returns:
        hours: int, 소요시간의 시간
        minutes: int, 소요시간의 분
        seconds: float, 소요시간의 초
    """
    end_time = time.time() - start_time
    hours = end_time // 3600
    minutes = (end_time % 3600) // 60
    seconds = round((end_time % 3600) % 60, 4)
    return hours, minutes, seconds


if __name__ == "__main__":
    import os

    print(__file__)
    print(os.path.abspath(__file__))
    print(os.path.dirname(os.path.abspath(__file__)))
    
    upper_dir = os.path.dirname(os.path.abspath(__file__))
    file_prefix = "logtest"
    lc = LogConfig(upper_dir, file_prefix)
    
    log = get_log_view(lc)
    err_log = get_log_view(lc, log_level="WARNING", error_log=True)
    
    
    #log.info("{0} / {1} ..... {2}".format(123132, 1561, ["test"]))
    #err_log.error("{0} error on this file.".format(__file__))