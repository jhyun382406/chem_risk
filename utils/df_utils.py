import os
import json
import pandas as pd


class DataframeUtils(object):
    """Pandas dataframe 관련 공통 함수."""
    def __init__(self, log, err_log):
        """
        Description:
            Pandas dataframe 관련 공통 함수
        Args:
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(DataframeUtils, self).__init__()
        self.log, self.err_log = log, err_log

    def load_cols_type(self, json_path, encoding="utf-8"):
        """
        Description:
            불러올 dataframe에 적용할 칼럼 타입 불러오기.
        Args:
            json_path: str, 불러올 json 파일 경로
            encoding: str, json 파일 인코딩 방식 (default="utf-8")
        Returns:
            cols_type: dict, 불러올 csv 칼럼 별 타입
        """
        try:
            with open(json_path, "r", encoding=encoding) as f:
                cols_type = json.load(f)
        except Exception as e:
            self.log.info("Load json file incompletely")
            self.err_log.error("Load json file incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Load json file completely")
            return cols_type

    def load_csv(self, data_path, cols_type, encoding="utf-8"):
        """
        Description:
            CSV 파일 불러오기.
        Args:
            data_path: str, 불러올 csv 파일 경로
            cols_type: str 또는 dict, 불러올 csv 칼럼 별 타입
            encoding: str, csv 파일 인코딩 방식 (default="utf-8")
        Returns:
            df: dataframe, 파일에서 불러온 전체 데이터
        """
        try:
            filename = os.path.basename(data_path)
            df = pd.read_csv(data_path, encoding=encoding, dtype=cols_type)
        except Exception as e:
            self.log.info("Load csv file incompletely: {0}".format(filename))
            self.err_log.error("Load csv file incompletely: {0}".format(filename))
            self.err_log.error("{0}".format(e))
            raise e
        else:
            if isinstance(cols_type, dict):
                df = df[list(cols_type.keys())]
            self.log.info("Load csv file completely: {0}".format(filename))
            return df
    
    def make_cols_type(self, df):
        """
        Description:
            칼럼별 타입 json 형식 만들기.
        Args:
            df: dataframe, 칼럼 추출할 dataframe
        Returns:
            cols_type: dict, '칼럼: 타입' 꼴의 딕셔너리
        """
        cols_type = dict()
        for col, dtype in zip(df.dtypes.index, list(df.dtypes.values)):
            dtype_str = str(dtype)
            if dtype_str.startswith("float"):
                dtype_str = "float"
            elif dtype_str.startswith("int"):
                dtype_str = "int"
            elif dtype_str.startswith("bool"):
                dtype_str = "bool"
            elif dtype_str.startswith("category"):
                dtype_str = "category"
            else:
                dtype_str = "str"
            cols_type[col] = dtype_str
        return cols_type
    
    def save_cols_type(self, cols_type, json_path, encoding="utf-8", indent="\t"):
        """
        Description:
            칼럼별 타입 json 저장 (utf-8 인코딩으로 저장).
        Args:
            cols_type: dict, '칼럼: 타입' 꼴의 딕셔너리
            json_path: str, 저장할 json 파일 경로
            encoding: str, json 파일 인코딩 방식 (default="utf-8")
            indent: str, json 파일의 indent (default="\t")
        Returns:
            None
        """
        try:
            with open(json_path, "w", encoding=encoding) as p:
                json.dump(cols_type, p, ensure_ascii=False, indent=indent)
        except Exception as e:
            self.log.info("Save json file incompletely")
            self.err_log.error("Save json file incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save json file completely")
    
    def save_csv(self, df, save_path, encoding="utf-8"):
        """
        Description:
            Dataframe을 CSV 파일로 저장.
        Args:
            df: dataframe, 저장할 데이터
            save_path: str, 저장할 csv 파일 경로
            encoding: str, csv 파일 인코딩 방식 (default="utf-8")
        Returns:
            None
        """
        try:
            df.to_csv(save_path, index=False, encoding=encoding)
        except Exception as e:
            self.log.info("Save csv file incompletely")
            self.err_log.error("Save csv file incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save csv file completely")
    
    def return_notnull_cols(self, df):
        """
        Description:
            DataFrame에서 빈 값이 없는 칼럼만 추출.
        Args:
            df: dataframe, 칼럼 추출할 dataframe
        Returns:
            notnull_cols: list, 빈 값이 없는 칼럼 리스트
        """
        return [col for col in df.columns if df[col].isna().sum() == 0]
    
    def return_null_cols(self, df):
        """
        Description:
            DataFrame에서 빈 값이 있는 칼럼만 추출.
        Args:
            df: dataframe, 칼럼 추출할 dataframe
        Returns:
            null_cols: list, 빈 값이 있는 칼럼 리스트
        """
        return [col for col in df.columns if df[col].isna().sum() != 0]
    
    def check_null_datas(self, df):
        """
        Description:
            DataFrame에서 빈 값이 있는 칼럼 및 빈 값 row 개수 반환.
        Args:
            df: dataframe, 데이터 추출할 dataframe
        Returns:
            check_nulls: dict, 빈 값이 있는 칼럼 및 칼럼별 비어 있는 row 개수
        """
        check_nulls = dict()
        for col in df.columns:
            na_nums = df[col].isna().sum()
            if na_nums > 0:
                check_nulls[col] = na_nums
        return check_nulls