FROM pytorch/pytorch:1.13.1-cuda11.6-cudnn8-runtime

RUN apt-get update && apt-get install -y vim sudo libgl1-mesa-glx libglib2.0-0 gcc libc6-dev g++

# USER 추가 및 sudo 권한 설정 (CHPASSWD는 계정:비밀번호 구조)
ARG USER="wizai" \
  CHPASSWD="wizai:wizai"
RUN adduser --disabled-password --gecos "" $USER \
  && echo $CHPASSWD | chpasswd \
  && adduser $USER sudo \
  && echo '$USER ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers \
# root의 .bashrc 복사 후 USER 변경
  && cat /root/.bashrc >> /home/$USER/.bashrc \
  && chown $USER:$USER /home/$USER/.bashrc

# pip PATH 지정 안되어 추가
RUN python -m pip install --upgrade pip \
  && pip install joblib==1.2.0 \
  opencv-python==4.5.5.64 \
  pandas==1.5.2 \
  category-encoders==2.6.0 \
  dask==2022.10.2 \
  deepchem==2.8.0 \
  easydict==1.10 \
  imbalanced-learn==0.11.0 \
  rsa==4.9 \
  scikit-learn==1.2.2 \
  scipy==1.10.1 \
  setproctitle==1.3.2 \
  statsmodels==0.13.5 \
  matplotlib==3.7.1 \
  rdkit==2023.3.3 \
  pytorch-lightning==1.9.5 \
  torchinfo==1.7.2 \
  tensorflow[and-cuda]==2.13.0 \
  oauthlib==3.2.2 \
  requests-oauthlib==1.3.1 \
  Flask==3.0.0 \
  Flask-Cors==4.0.0 \
  flask-restx==1.3.0 \
  gunicorn==21.2.0 \
  ipykernel==6.15.0 \
  lightning-utilities==0.8.0 \
  Markdown==3.4.4 \
  multidict==6.0.4 \
  opt-einsum==3.3.0 \
  protobuf==4.24.2
USER $USER
ENV PATH=/home/$USER/.local/bin:$PATH

# key value 식으로 이미지에 코멘트 작성
LABEL title="dna_chem_risk_torch" \
  version="240621" \
  baseimage="pytorch/pytorch:1.13.1-cuda11.6-cudnn8-runtime" \
  description="DNA chemical risk AI model deployment environment"
