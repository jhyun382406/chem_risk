"""
화학사고 예측 및 리스크 평가.

방법
- 수집데이터 사용하여 진행
"""

import os
import datetime
import argparse
from setproctitle import setproctitle

from utils.log_module import LogConfig, get_log_view
from utils.code_utils import convert

import warnings
warnings.filterwarnings(action="ignore")


def train(cfg, log, err_log):
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    from utils.torch_settings import set_gpu_torch, set_random_seed, set_float32_matmul
    from utils.code_utils import load_pickle
    from src_m2.preprocess import PrepDatas
    from src_m2.model_hparams import make_add_hparams, make_hparams, get_pretrn_mat_params
    from src_m2.network_dnn import DNNClsV2
    from src_m2.network_mat import ModifyMATV2
    from src_m2.network_dnnmat import DnnMatClsV2
    from src_m2.model import DnnMat
    from src_m2.dataloader import DnnMATDataloader
    from src_m2.train import TrainModel
    
    # GPU 설정, Random Seed 고정
    device = set_gpu_torch(cfg.TRAIN.GPU.IDX, cfg.TRAIN.GPU.MEMORY)
    set_random_seed(cfg.TRAIN.HPARAMS.RANDOM_SEED, if_cudnn=True)
    set_float32_matmul("high")
    
    # 데이터 전처리 불러오기
    prep = PrepDatas(cfg, log, err_log)
    cols_type = prep.load_cols_type(prep.save_dtypepath)
    train_df = prep.load_csv(prep.save_train_filepath, cols_type)
    saved_dict = load_pickle(prep.encoder_path)
    
    # 모델에 적용할 hyperparameter
    add_hparams, dnn_input_cols = make_add_hparams(
        train_df, [prep.cas_col, prep.tgt_col], cfg
    )
    hparams = make_hparams(add_hparams, cfg)
    
    # 기저 모델 네트워크 생성
    dnn_net = DNNClsV2(dnn_input_cols, hparams)
    # mat_args = {k[4:]: v for k, v in hparams.items() if k.startswith("mat")}
    # mat_args["batch_size"] = hparams.batch_size
    # mat = ModifyMAT(**mat_args)
    pretrn_path = cfg.TRAIN.PRETRAINED.MAT.PATH
    pretrn_mat_params = get_pretrn_mat_params(
        train_df, prep.cas_col, prep.tgt_col, hparams
    )
    mat = ModifyMATV2(pretrn_mat_params, hparams.batch_size, pretrn_path)
    
    # 통합 네트워크 구축 후 Pytorch-lightning 모듈로 생성
    dnnmat_net = DnnMatClsV2(mat, dnn_net, device, hparams)
    dnnmat = DnnMat(dnnmat_net.hparams, dnnmat_net, log)
    
    # Dataloader (MAT 메서드를 사용함)
    dl = DnnMATDataloader(
        dnn_input_cols, prep.tgt_col, prep.cas_col, hparams, log, err_log
    )
    train_dl, valid_dl = dl(train_df, mat, saved_dict["smiles"])
    
    # 학습
    trn = TrainModel(hparams, cfg, log, err_log)
    trn(
        dnnmat, add_hparams,
        train_dl, valid_dl,
        device, cfg.TRAIN.LOAD_CKPT_PATH
    )


def test(cfg, log, err_log):
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    from utils.torch_settings import set_gpu_torch, set_random_seed, set_float32_matmul
    from utils.code_utils import load_pickle
    from src_m2.preprocess import PrepDatas
    from src_m2.model_hparams import make_add_hparams, load_hparams, get_pretrn_mat_params
    from src_m2.network_dnn import DNNClsV2
    from src_m2.network_mat import ModifyMATV2
    from src_m2.network_dnnmat import DnnMatClsV2
    from src_m2.model import DnnMat
    from src_m2.dataloader import DnnMATDataloader
    from src_m2.test import TestModel
    
    device = set_gpu_torch(cfg.TEST.GPU.IDX, cfg.TEST.GPU.MEMORY)
    set_random_seed(cfg.TRAIN.HPARAMS.RANDOM_SEED, if_cudnn=True)
    set_float32_matmul("high")
    
    # 데이터 전처리 불러오기
    prep = PrepDatas(cfg, log, err_log)
    cols_type = prep.load_cols_type(prep.save_dtypepath)
    test_df = prep.load_csv(prep.save_test_filepath, cols_type)
    saved_dict = load_pickle(prep.encoder_path)
    
    # 모델에 적용할 hyperparameter
    add_hparams, dnn_input_cols = make_add_hparams(
        test_df, [prep.cas_col, prep.tgt_col], cfg
    )
    hparams = load_hparams(cfg)
    
    # 기저 모델 네트워크 생성
    dnn_net = DNNClsV2(dnn_input_cols, hparams)
    # mat_args = {k[4:]: v for k, v in hparams.items() if k.startswith("mat")}
    # mat_args["batch_size"] = hparams.batch_size
    # mat = ModifyMAT(**mat_args)
    pretrn_mat_params = get_pretrn_mat_params(
        test_df, prep.cas_col, prep.tgt_col, hparams
    )
    mat = ModifyMATV2(pretrn_mat_params, hparams.batch_size)
    
    # 통합 네트워크 구축 후 Pytorch-lightning 모듈로 생성
    dnnmat_net = DnnMatClsV2(mat, dnn_net, device, hparams)
    dnnmat = DnnMat(dnnmat_net.hparams, dnnmat_net, log)
    
    # Dataloader (MAT 메서드를 사용함)
    dl = DnnMATDataloader(
        dnn_input_cols, prep.tgt_col, prep.cas_col, hparams, log, err_log
    )
    test_dnn_df, test_cas_df, test_label, test_dl = dl(
        test_df, mat, saved_dict["smiles"], is_test=True
    )
    
    # 테스트 결과 산출
    test = TestModel(cfg, log, err_log)
    test(
        dnnmat, hparams,
        len(test_dnn_df), test_label, test_dl,
        device
    )


def predict(input_data, cfg, log, err_log):
    # log.info("--------------------------------------------------")
    # err_log.error("--------------------------------------------------")
    pass


def validate(input_data, cfg, log, err_log):
    # log.info("--------------------------------------------------")
    # err_log.error("--------------------------------------------------")
    pass


def data_extract(cfg, log, err_log):
    from src_m2.extract_data import ChemAcciSSU
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # 화학사고 수집 파일
    ssu = ChemAcciSSU(cfg, log, err_log)
    ssu()


def preprocess(cfg, log, err_log):
    from utils.torch_settings import set_random_seed
    from src_m2.preprocess import PrepDatas
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # 랜덤 시드 고정
    set_random_seed(cfg.TRAIN.HPARAMS.RANDOM_SEED)
    
    # 데이터 전처리
    prep = PrepDatas(cfg, log, err_log)
    prep()


def setting_log(
    cfg,
    upper_dir,
    log_level="INFO",
    err_log_level="WARNING"
    ):
    """
    Description:
        로그 객체 생성.
    Args:
        cfg: Namedtuple, YAML 파일에서 읽어온 설정
        upper_dir: str, 해당 파일의 상위 디렉토리 경로
        log_level: str, 로그 레벨 (default="INFO")
            - DEBUG -> INFO -> WARNING -> ERROR -> CRITICAL
        err_log_level: str, 에러 로그 레벨 (default="WARNING")
            - DEBUG -> INFO -> WARNING -> ERROR -> CRITICAL
    Returns:
        log: logging.logger, 로그 객체
        err_log: logging.logger, 에러 로그 객체
    """
    file_prefix = cfg.LOG_PREFIX
    lc = LogConfig(upper_dir, file_prefix)
    log = get_log_view(lc, log_level=log_level)   # default: INFO
    err_log = get_log_view(lc, log_level=err_log_level, error_log=True)
    return log, err_log


def main(args):
    """
    Description:
        메인 모듈 실행함수.
        data 때는 tensorflow 또는 torch를 import 하지 않도록 분기 설정.

        사용 예
          - 사용 데이터 추출: python main_m2.py -c /path/of/config/yaml -m extract
    Args:
        args: 파이썬 실행할 때, 입력한 값
              "-c", "--config": 설정 yaml 파일 경로
              "-m", "--mode": train, test, predict, extract 모드 변경
    """
    upper_dir = os.path.dirname(os.path.abspath(__file__))
    cfg_file = args.config
    cfg = convert(cfg_file)

    if args.mode == "train":
        setproctitle("chem_risk_train_m2")
        log, err_log = setting_log(cfg.TRAIN, upper_dir)
        train(cfg, log, err_log)
        
    elif args.mode == "test":
        setproctitle("chem_risk_test_m2")
        log, err_log = setting_log(cfg.TEST, upper_dir)
        test(cfg, log, err_log)
        
    elif args.mode == "predict":
        setproctitle("chem_risk_pred_m2")
        log, err_log = setting_log(cfg.PREDICT, upper_dir)
        input_data = None
        predict(input_data, cfg, log, err_log)
    
    elif args.mode == "valid":
        setproctitle("chem_risk_valid_m2")
        log, err_log = setting_log(cfg.VALID, upper_dir)
        input_data = None
        validate(input_data, cfg, log, err_log)
    
    elif args.mode == "extract":
        setproctitle("chem_risk_extract_m2")
        log, err_log = setting_log(cfg.EXTRACT, upper_dir)
        data_extract(cfg, log, err_log)
    
    elif args.mode == "preprocess":
        setproctitle("chem_risk_prep_m2")
        log, err_log = setting_log(cfg.PREPROCESS, upper_dir)
        preprocess(cfg, log, err_log)
    
    else:
        pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-c", "--config", type=str,
        help="a config file in yaml format to control experiment"
        )
    parser.add_argument(
        "-m", "--mode", type=str,
        help="executition mode",
        choices=["train", "test", "predict", "valid", "extract", "preprocess"]
        )

    args = parser.parse_args()

    main(args)
