import os
import numpy as np
from easydict import EasyDict as edict

from MAT.src.featurization.data_utils import load_data_from_smiles
from utils.code_utils import convert


def make_add_hparams(train_df, except_cols, cfg):
    """
    Description:
        hparams에 추가할 변수 생성.
    Args:
        train_df: dataframe, 학습 데이터
        except_cols: list, DNN 네트워크 학습 input에서 제외할 칼럼
        cfg: Namedtuple, YAML 파일에서 읽어온 설정
    Returns:
        add_hparams: dict, hparams에 추가할 딕셔너리
        dnn_input_cols: list, DNN 네트워크의 입력 features
    """
    dnn_input_cols = [col for col in train_df.columns if not col in except_cols]
    BATCH_SIZE = cfg.TRAIN.HPARAMS.BATCH_SIZE
    add_hparams = dict(
        input_features = len(dnn_input_cols) + 1,   # CAS column도 1개 추가
        output_categories = len(cfg.PREPROCESS.SSU.TARGET_BINS) + 1,   # 구간 수는 bins+1
        steps_per_epoch = len(train_df) // BATCH_SIZE + 1
    )
    return add_hparams, dnn_input_cols


def make_hparams(add_hparams, cfg):
    """
    Description:
        Trainer에 반영할 하이퍼 파라미터 생성.
        YAML 파일 외에도 계산이 필요한 부분이 있어 별도 생성.
    Args:
        add_hparams: dict, Dataloader에서 생성된 hparams에 추가할 딕셔너리
        cfg: Namedtuple, YAML 파일에서 읽어온 설정
    Returns:
        hparams: easyDict, 하이퍼 파라미터 딕셔너리
    """
    cfg_tr = cfg.TRAIN
    cfg_dnn = cfg_tr.HPARAMS.NETWORK.DNN
    cfg_mat = cfg_tr.HPARAMS.NETWORK.MAT
    cfg_last = cfg_tr.HPARAMS.NETWORK.LAST
    cfg_l = cfg_tr.HPARAMS.LOSS
    cfg_lr = cfg_tr.HPARAMS.LR_SCHEDULER
    cfg_cb = cfg_tr.HPARAMS.CALLBACKS
    input_features = add_hparams["input_features"]
    output_categories = add_hparams["output_categories"]
    steps_per_epoch = add_hparams["steps_per_epoch"]
    
    general_hparams = dict(
        train_valid_ratio = cfg_tr.HPARAMS.TRAIN_VALID_RATIO,
        random_seed = cfg_tr.HPARAMS.RANDOM_SEED,
        dl_workers = cfg_tr.HPARAMS.DL_WORKERS,
        batch_size = cfg_tr.HPARAMS.BATCH_SIZE,
        epochs = cfg_tr.HPARAMS.EPOCHS,
        input_features = input_features,
        output_categories = output_categories,
    )
    dnn_hparams = dict(
        dnn_layer_features = cfg_dnn.LAYER_FEATS,
        dnn_dropout_rate = cfg_dnn.DROP_RATE,
        dnn_terminal = cfg_dnn.TERMINAL,
    )
    if cfg_tr.PRETRAINED.MAT.USE:
        cfg_pretrn_mat = cfg_tr.PRETRAINED.MAT.HPARAMS
        mat_hparams = dict(
            mat_d_model = cfg_pretrn_mat.D_MODEL,
            mat_n = cfg_pretrn_mat.N,
            mat_h = cfg_pretrn_mat.H,
            mat_n_dense = cfg_pretrn_mat.N_DENSE,
            mat_lambda_attention = cfg_pretrn_mat.LAMBDA_ATTENTION,
            mat_lambda_distance = cfg_pretrn_mat.LAMBDA_DISTANCE,
            mat_leaky_relu_slope = cfg_pretrn_mat.LEAKY_RELU_SLOPE,
            mat_dense_output_nonlin = cfg_pretrn_mat.DENSE_OUTPUT_NONLIN,
            mat_dist_matrix_kernel = cfg_pretrn_mat.DIST_MATRIX_KERNEL,
            mat_dropout = cfg_pretrn_mat.DROPOUT,
            mat_aggregation_type = cfg_pretrn_mat.AGGREGATION_TYPE,
        )
    else:
        mat_hparams = dict(
            mat_dist_kernel = cfg_mat.DIST_KERNEL,
            mat_n_encoders = cfg_mat.N_ENCODERS,
            mat_lambda_attention = cfg_mat.LAMBDA_ATTENTION,
            mat_lambda_distance = cfg_mat.LAMBDA_DISTANCE,
            mat_h = cfg_mat.H,
            mat_sa_hsize = cfg_mat.SA_HSIZE,
            mat_sa_dropout_p = cfg_mat.SA_DROPOUT_P,
            mat_output_bias = cfg_mat.OUTPUT_BIAS,
            mat_d_input = cfg_mat.D_INPUT,
            mat_d_hidden = cfg_mat.D_HIDDEN,
            mat_d_output = cfg_mat.D_OUTPUT,
            mat_activation = cfg_mat.ACTIVATION,
            mat_n_layers = cfg_mat.N_LAYERS,
            mat_ff_dropout_p = cfg_mat.FF_DROPOUT_P,
            mat_encoder_hsize = cfg_mat.ENCODER_HSIZE,
            mat_encoder_dropout_p = cfg_mat.ENCODER_DROPOUT_P,
            mat_embed_input_hsize = cfg_mat.EMBED_INPUT_HSIZE,
            mat_embed_dropout_p = cfg_mat.EMBED_DROPOUT_P,
            mat_gen_aggregation_type = cfg_mat.GEN_AGGREGATION_TYPE,
            mat_gen_dropout_p = cfg_mat.GEN_DROPOUT_P,
            mat_gen_n_layers = cfg_mat.GEN_N_LAYERS,
            mat_gen_attn_hidden = cfg_mat.GEN_ATTN_HIDDEN,
            mat_gen_attn_out = cfg_mat.GEN_ATTN_OUT,
            mat_gen_d_output = cfg_mat.GEN_D_OUTPUT,
        )
    output_hparams = dict(
        last_class_nums = cfg_last.CLS_NUM,
        last_merge_method = cfg_last.MERGE_METHOD,
    )
    loss_hparams = dict(
        loss_kind = cfg_l.KIND,
        loss_scale = cfg_l.SCALE,
        loss_args = cfg_l.ARGS,
    )
    opt_hparams = dict(
        lr = cfg_lr.LR,   # Peak learning rate
        div_factor = cfg_lr.DIV_FACTOR,   # Using LR Scheduler
        steps_per_epoch = steps_per_epoch,
    )
    callback_hparams = dict(
        ckpt_monitor = cfg_cb.CKPT.MONITOR,
        ckpt_mode = cfg_cb.CKPT.MODE,
        ckpt_top_k = cfg_cb.CKPT.TOP_K,
        es_monitor = cfg_cb.EARLYSTOP.MONITOR,
        es_min_delta = cfg_cb.EARLYSTOP.MIN_DELTA,
        es_patience = cfg_cb.EARLYSTOP.PATIENCE,
        es_mode = cfg_cb.EARLYSTOP.MODE,
        lr_log_interval = cfg_cb.LR_MONITOR.INTERVAL,
        csv_logger_name = cfg_cb.CSV_LOGGER.NAME
    )
    hparams = dict(
        **general_hparams,
        **dnn_hparams,
        **mat_hparams,
        **output_hparams,
        **loss_hparams,
        **opt_hparams,
        **callback_hparams,
    )
    return edict(hparams)


def get_pretrn_mat_params(train_df, cas_col, tgt_col, hparams):
    """
    Description:
        Pretrained 적용할 MAT 하이퍼 파라미터 생성.
        input 생성 시, SMILES로부터 node_features 부분에서
        값을 가져와야 해서 입력 데이터에서 샘플로 계산.
    Args:
        cfg: Namedtuple, YAML 파일에서 읽어온 설정
    Returns:
        hparams: easyDict, 하이퍼 파라미터 딕셔너리
    """
    x_sample, _ = load_data_from_smiles(
        train_df[cas_col][:1],
        train_df[tgt_col][:1].astype(np.float32),
        add_dummy_node=True,
        one_hot_formal_charge=True   # pre-trained 안 쓸거면 False 권장
    )
    x_node_features = x_sample[0][0]
    pretrn_mat_params = {
        "d_atom": x_node_features.shape[1],
        "d_model": hparams.mat_d_model,
        "N": hparams.mat_n,
        "h": hparams.mat_h,
        "N_dense": hparams.mat_n_dense,
        "lambda_attention": hparams.mat_lambda_attention, 
        "lambda_distance": hparams.mat_lambda_distance,
        "leaky_relu_slope": hparams.mat_leaky_relu_slope, 
        "dense_output_nonlinearity": hparams.mat_dense_output_nonlin, 
        "distance_matrix_kernel": hparams.mat_dist_matrix_kernel, 
        "dropout": hparams.mat_dropout,
        "aggregation_type": hparams.mat_aggregation_type
    }
    return pretrn_mat_params


def load_hparams(cfg):
    """
    Description:
        학습에 사용했던 Trainer에 반영할 하이퍼 파라미터 생성.
        YAML 파일 외에도 계산이 필요한 부분이 있어 별도 생성.
    Args:
        cfg: Namedtuple, YAML 파일에서 읽어온 설정
    Returns:
        hparams: easyDict, 하이퍼 파라미터 딕셔너리
    """
    model_dir = cfg.TEST.LOAD_MODEL_DIR
    hparams_file = cfg.TEST.LOAD_CODE_HPARAMS_PATH
    load_hparams = convert(os.path.join(model_dir, hparams_file))
    return edict(load_hparams._asdict())
