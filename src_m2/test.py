import os
import time
import numpy as np
import pandas as pd
import torch
import torch.nn.functional as F
import matplotlib.pyplot as plt
from pytorch_lightning import Trainer
from sklearn.metrics import multilabel_confusion_matrix
from sklearn.metrics import roc_curve, auc

from utils.log_module import print_elapsed_time
from utils.df_utils import DataframeUtils

import warnings
warnings.filterwarnings(action="ignore")


class TestModel(object):
    """모델 테스트."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            Pytorch-lightning 이용한 테스트.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(TestModel, self).__init__()
        cfg_t = cfg.TEST
        # self.cfg_prep = cfg.PREPROCESS.PREP
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.load_model_dir = cfg_t.LOAD_MODEL_DIR
        self.load_model_file = cfg_t.LOAD_MODEL_FILE
        self.test_accel = cfg_t.TEST_ACCEL
        self.NUM_COLS = cfg_t.NUM_COLS
        self.METRIC_COLS = cfg_t.METRIC_COLS
        self.cfm_metrics_file = cfg_t.CFM_METRICS_FILE
        self.roc_cfg = cfg_t.ROC
        self.plot_dir = cfg_t.LOAD_MODEL_DIR
        self.plot_filename = cfg_t.PLOT_FILENAME
        self.plot_dpi = cfg_t.PLOT_DPI
        
        # 추가 생성 변수
        self.labels = [
            i+1 for i in range(len(cfg.PREPROCESS.SSU.TARGET_BINS) + 1)
        ]
        self.plot_dir = cfg_t.LOAD_MODEL_DIR
        self.dfutil = DataframeUtils(log, err_log)

    def __call__(self,
                 model,
                 hparams,
                 test_data_nums,
                 test_label,
                 test_dl,
                 device):
        """
        Description:
            Model, Dataloader를 이용하여 테스트 진행.
            모델 불러온 뒤, 테스트 결과 로그 생성.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            hparams: easyDict, 하이퍼 파라미터 딕셔너리
            test_data_nums: int, 테스트용 입력 데이터 수
            test_label: numpy array, 테스트용 입력 데이터의 label
            test_dl: dataloader, 테스트 데이터 dataloader
            device: torch.device, 할당된 장치
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to Test model")
        
        # Load model
        self.load_model_state(model)
        
        # Make trainer & Test model
        trainer = self.make_trainer(device)
        self.test_model(trainer, model, test_dl)
        
        # Predict model & Compute Metrics
        results = self.predict_model(trainer, model, hparams, test_dl)
        multi_cfm = self.make_multi_cfm(self.labels, test_label, results)
        cfm_df = self.compute_metrics(
            multi_cfm, self.labels, self.NUM_COLS, self.METRIC_COLS
        )
        cfm_df = self.compute_weighted_avg(
            cfm_df, test_data_nums, self.labels, self.METRIC_COLS
        )
        self.save_cfm_df(cfm_df, self.load_model_dir, self.cfm_metrics_file)
        
        # Make & Save plots
        self.draw_save_roc(self.labels, test_label, results, self.roc_cfg)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to Test model: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def load_model_state(self, model):
        """
        Description:
            학습된 모델 불러오기.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
        Returns:
            None
        """
        model_path = os.path.join(self.load_model_dir, self.load_model_file)
        model.load_state_dict(torch.load(model_path))
        model.eval()   # inference mode
        
        if self.load_model_dir.endswith("/"):
            model_ver = os.path.dirname(self.load_model_dir).split("/")[-1]
        else:
            model_ver = os.path.basename(self.load_model_dir)
        self.log.info("Load model state: {0}".format(model_ver))

    def make_trainer(self, device):
        """
        Description:
            Pytorch-lightning의 trainer 생성.
        Args:
            device: torch.device, 할당된 장치
        Returns:
            trainer: Trainer, Pytorch-lightning의 trainer
        """
        trainer = Trainer(
            accelerator=self.test_accel,
            devices=[device.index],
            enable_checkpointing=False,
            logger=False
        )
        return trainer

    def test_model(self,
                   trainer,
                   model, 
                   test_dl):
        """
        Description:
            테스트 진행.
        Args:
            trainer: Trainer, Pytorch-lightning의 trainer
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            test_dl: dataloader, 테스트 데이터 dataloader
        Returns:
            None
        """
        trainer.test(model, test_dl)

    def predict_model(self,
                      trainer,
                      model,
                      hparams,
                      pred_dl):
        """
        Description:
            추론 진행.
            CrossEntropy Loss는 Softmax가 내장되어 Layer에 없음.
                - Softmax를 추가해줘야 제대로 값이 나옴
        Args:
            trainer: Trainer, Pytorch-lightning의 trainer
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            hparams: easyDict, 하이퍼 파라미터 딕셔너리
            pred_dl: dataloader, 추론 데이터 dataloader
        Returns:
            results: numpy array, 추론 결과 raw 데이터
        """
        results = trainer.predict(model, pred_dl)
        results = np.concatenate(results, axis=0)
        if hparams.loss_kind == "cce":   # CrossEntropy
            results = F.softmax(torch.from_numpy(results), dim=1).numpy()
        return results
    
    def make_multi_cfm(self, labels, test_label, results):
        """
        Description:
            Multi-label Confusion Matrix 생성.
        Args:
            labels: list, 데이터 label unique 리스트
            test_label: numpy array, 테스트용 입력 데이터의 label
            results: numpy array, 추론 결과 raw 데이터
        Returns:
            multi_cfm: numpy array, .ravel() 하면 TN, FP, FN, TP 순
        """
        multi_cfm = multilabel_confusion_matrix(
            test_label.to_numpy().astype(int),
            np.argmax(results, axis=1) + 1,
            labels=labels
        )
        return multi_cfm
    
    def compute_metrics(self, multi_cfm, labels, NUM_COLS, METRIC_COLS):
        """
        Description:
            Multi-label Confusion Matrix로 Label마다 각각 Metrics 계산.
            Label을 인덱스로 두어 dataframe 형태로 정리.
            Label별로 값은 debug 쪽으로 설정.
        Args:
            multi_cfm: numpy array, .ravel() 하면 TN, FP, FN, TP 순
            labels: list, 데이터 label unique 리스트
            NUM_COLS: list, 개수만 세는 칼럼
            METRIC_COLS: list, Metrics 계산하는 칼럼
        Returns:
            cfm_df: dataframe, Label 별로 CFM 및 Metrics 정리
        """
        cfm_df = pd.DataFrame(
            columns=NUM_COLS+METRIC_COLS,
            index=labels
        )
        
        for idx, label in enumerate(labels):
            TN, FP, FN, TP = multi_cfm[idx].ravel()
            self.log.info("Label: {0} | TN: {1} / FP: {2} / FN: {3} / TP: {4}".format(label, TN, FP, FN, TP))
            
            Accuracy = (TP + TN) / (TP + FN + FP + TN)
            Precision = (TP) / (TP + FP)
            Recall = (TP) / (TP + FN)
            F1score = 2 * Precision * Recall / (Precision + Recall)
            Specificity = (TN) / (FP + TN)
            FalsePositiveRate = 1 - Specificity
            BalancedAccuracy = (Recall + Specificity) / 2

            self.log.info("Accuracy: {0:.4f}".format(Accuracy))
            self.log.info("Precision: {0:.4f}".format(Precision))
            self.log.info("Recall: {0:.4f}".format(Recall))
            self.log.info("F1score: {0:.4f}".format(F1score))
            self.log.info("Specificity: {0:.4f}".format(Specificity))
            self.log.info("FalsePositiveRate: {0:.4f}".format(FalsePositiveRate))
            self.log.info("BalancedAccuracy: {0:.4f}".format(BalancedAccuracy))
            
            # NUM_COLS + METRIC_COLS 여야 함
            cfm_df.loc[idx+1] = (
                TP+FN, TP, FP, FN, TN,
                Accuracy, Precision, Recall, F1score,
                Specificity, FalsePositiveRate, BalancedAccuracy
            )
        
        return cfm_df
    
    def compute_weighted_avg(self, cfm_df, TEST_LEN, labels, METRIC_COLS):
        """
        Description:
            Label 별로 가중치를 두어 Metrics의 가중 평균 계산.
            NaN은 0으로 치환하여 계산.
        Args:
            cfm_df: dataframe, Label 별로 CFM 및 Metrics 정리
            TEST_LEN: int, 테스트 데이터 갯수
            labels: list, 데이터 label unique 리스트
            METRIC_COLS: list, Metrics 계산하는 칼럼
        Returns:
            cfm_df: dataframe, Label 별로 CFM 및 Metrics 정리 + 가중평균 결과
        """
        # 가중평균 계산
        avg_cfm_df = cfm_df.copy()
        avg_cfm_df["WEIGHT"] = avg_cfm_df["ACCIDENT"] / TEST_LEN
        avg_cfm_df = avg_cfm_df.fillna(0)
        avg_cfm_df.loc["weighted"] = np.nan
        metrics_data = avg_cfm_df[METRIC_COLS].loc[labels].to_numpy()
        weights = avg_cfm_df["WEIGHT"].loc[labels].to_numpy()
        avg_cfm_df.loc["weighted"][METRIC_COLS] = np.average(metrics_data, weights=weights, axis=0)
        # 원래 dataframe에 추가
        cfm_df.loc["weighted"] = np.nan
        cfm_df.loc["weighted"] = avg_cfm_df.loc["weighted"][cfm_df.columns]
        return cfm_df
    
    def save_cfm_df(self, cfm_df, saved_dir, save_file):
        """
        Description:
            Confusion Matrix 와 Metrics 결과를 csv로 저장.
        Args:
            cfm_df: dataframe, Label 별로 CFM 및 Metrics 정리 + 가중평균 결과
            saved_dir: str, 저장할 디렉토리
            save_file: str, 저장할 파일명
        Returns:
            None
        """
        save_path = os.path.join(saved_dir, save_file)
        self.dfutil.save_csv(cfm_df, save_path)
    
    def draw_save_roc(self, labels, test_label, results, roc_cfg):
        """
        Description:
            ROC curve를 계산하고 plot을 저장.
        Args:
            labels: list, 데이터 label unique 리스트
            test_label: numpy array, 테스트용 입력 데이터의 label
            results: numpy array, 추론 결과 raw 데이터
            roc_cfg: namedtuple, ROC curve plot의 설정
        Returns:
            None
        """
        f, axes = plt.subplots(1, len(labels))
        # 격자 크기, 여백 설정
        f.set_size_inches((roc_cfg.IMG_W, roc_cfg.IMG_H))
        plt.subplots_adjust(wspace=roc_cfg.WSPACE, hspace=roc_cfg.HSPACE)

        for idx, label in enumerate(labels):
            label_test = results[:, label-1]
            label_true = np.where(test_label.to_numpy().astype(int) == label, 1, 0)
            fpr, tpr, thresholds = roc_curve(label_true, label_test)
            roc_auc = auc(fpr, tpr)
            
            axes[idx].plot(fpr, tpr, label="Risk {0}: {1:.3f}".format(label, roc_auc))
            axes[idx].axis("square")
            axes[idx].set_xlabel("False Positive Rate")
            axes[idx].set_ylabel("True Positive Rate")
            axes[idx].set_title("One-vs-Rest ROC curves:\n{0} vs the others".format(label))
            axes[idx].legend()

        # 이미지 파일로 저장
        save_path = os.path.join(self.plot_dir, self.plot_filename)
        try:
            plt.savefig(save_path, dpi=self.plot_dpi)
        except Exception as e:
            self.log.info("Save Plot image [ROC Curve] incompletely")
            self.err_log.error("Save Plot image [ROC Curve] incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save Plot image [ROC Curve] completely")
