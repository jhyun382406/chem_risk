"""
src_m2.extract_data 에서 apply로 사용하는 함수
"""
import re
import random
from datetime import datetime, time, date
from dateutil.relativedelta import relativedelta
from distutils.util import strtobool
import numpy as np
import pandas as pd


def _apply_acci_location(series, col):
    """
    Description:
        지역 범주화.
            - 서울특별시, 부산광역시, 인천광역시, 대구광역시,
              광주광역시, 대전광역시, 울산광역시, 세종특별시,
              경기도, 강원도, 제주도, 충청북도, 충청남도,
              전라북도, 전라남도, 경상북도, 경상남도
    """
    loc = series[col]
    if isinstance(loc, float):   # nan
        return loc
    elif len(loc) > 7:
        loc = loc.strip().split(" ")[0]
    else:
        loc = loc.strip()
    
    if loc.startswith("서울"):
        loc = "서울특별시"
    elif loc.startswith("부산"):
        loc = "부산광역시"
    elif loc.startswith("인천"):
        loc = "인천광역시"
    elif loc.startswith("대구"):
        loc = "대구광역시"
    elif loc.startswith("광주"):
        loc = "광주광역시"
    elif loc.startswith("대전"):
        loc = "대전광역시"
    elif loc.startswith("울산"):
        loc = "울산광역시"
    elif loc.startswith("세종"):
        loc = "세종특별시"
    elif loc.startswith("경기"):
        loc = "경기도"
    elif loc.startswith("강원"):
        loc = "강원도"
    elif loc.startswith("제주"):
        loc = "제주도"
    elif loc.startswith("충청북") or loc.startswith("충북"):
        loc = "충청북도"
    elif loc.startswith("충청남") or loc.startswith("충남"):
        loc = "충청남도"
    elif loc.startswith("전라북") or loc.startswith("전북"):
        loc = "전라북도"
    elif loc.startswith("전라남") or loc.startswith("전남"):
        loc = "전라남도"
    elif loc.startswith("경상북") or loc.startswith("경북"):
        loc = "경상북도"
    elif loc.startswith("경상남") or loc.startswith("경남"):
        loc = "경상남도"
    else:
        loc = None
    return loc


def _apply_acci_season(series, col):
    """
    Description:
        봄: 3~5 / 여름: 6~8 / 가을: 9~11 / 겨울: 12~2
    """
    if isinstance(series[col], str):
        month = int(series[col][4:6])
        if month in range(3, 6):
            return "봄"
        elif month in range(6, 9):
            return "여름"
        elif month in range(9, 12):
            return "가을"
        else:
            return "겨울"
    else:
        return series[col]   # nan


def _apply_acci_date(series, dt_col, ss_col):
    """
    Description:
        사고일자가 없는 경우, YYYYMM 랜덤 생성
            - 결측처리된 사고계절 이용
            - 봄: 3~5 / 여름: 6~8 / 가을: 9~11 / 겨울: 12~2
    """
    dt = series[dt_col]
    if isinstance(dt, str):
        return dt
    else:
        year = random.randint(2001, 2023)
        season = series[ss_col]
        if season == "봄":
            month = random.choice([3, 4, 5])
        elif season == "여름":
            month = random.choice([6, 7, 8])
        elif season == "가을":
            month = random.choice([9, 10, 11])
        else:
            month = random.choice([1, 2, 12])
        return "{0}{1:02d}".format(year, month)


def _apply_date_random(series, col):
    """
    Description:
        사고일자가 YYYYMM인 경우 YYYYMMDD로 변환
            - 해당 월의 일자를 무작위 선택
    """
    acci_time = series[col]
    if len(acci_time) == 8:
        return acci_time
    else:   # 길이 6인 경우
        yy, mm = int(acci_time[:4]), int(acci_time[4:])
        s_date = datetime(yy, mm, 1)
        e_date = s_date + relativedelta(months=1, days=-1)   # 월 말일
        random_days = random.randint(0, (e_date - s_date).days)
        random_date = s_date + relativedelta(days=random_days)
        return random_date.strftime("%Y%m%d")


def _apply_acci_timegroups(series, col, time_ranges):
    """
    Description:
        https://www.yna.co.kr/view/AKR20170630181500004 기상청 기준 적용
            - 한밤: 0~3 / 새벽: 3~6 / 아침: 6~9 / 늦은오전: 9~12
            - 이른오후: 12~15 / 늦은오후: 15~18 / 저녁: 18~21 / 늦은밤: 21~24
    Args:
        time_ranges: dict, {end_time시간대(time객체): 시간대그룹}
    """
    if isinstance(series[col], str):
        time_obj = datetime.strptime(series[col], "%H:%M")
        time_obj = time(time_obj.hour, time_obj.minute)
        result = tuple(time_ranges.keys())[0]
        for g, r in time_ranges.items():
            if time_obj < r:
                result = g
                break
            else:
                continue
        return result
    else:
        return series[col]   # nan


def _apply_acci_time(
    series, t_col, tg_col, time_ranges, dummy_date=date(2020, 1, 1)
    ):
    """
    Description:
        결측값이면 시간대를 참고하여 무작위로 채워넣음
        https://www.yna.co.kr/view/AKR20170630181500004 기상청 기준 적용
            - 한밤: 0~3 / 새벽: 3~6 / 아침: 6~9 / 늦은오전: 9~12
            - 이른오후: 12~15 / 늦은오후: 15~18 / 저녁: 18~21 / 늦은밤: 21~24
    Args:
        hr_min: str, 시간분 4자리 문자열
    """
    _t = series[t_col]
    _tg = series[tg_col]
    if isinstance(_t, str):
        _hr, _min = [int(elm) for elm in _t.split(":")]
    else:
        # 시간 계산하려면 datetime 객체여야만 함
        full_date = datetime.combine(dummy_date, time_ranges[_tg])
        s_time = full_date - relativedelta(hours=3)
        e_time = full_date - relativedelta(minutes=1)
        # 시작 날짜와 종료 날짜 사이에서 랜덤한 날짜 생성
        random_mins = random.randint(0, (e_time - s_time).seconds // 60)
        random_date = s_time + relativedelta(minutes=random_mins)
        _hr, _min = random_date.hour, random_date.minute
    return "{0:02d}{1:02d}".format(_hr, _min)


def _apply_swungdash_float(series, col):
    """
    Description:
        ~로 범위 있는 경우엔 평균값 취하여 모두 float로 변환
    """
    value = series[col]
    if isinstance(value, str) and value.find("~") == -1:
        return float(value)
    elif isinstance(value, str):
        lv, rv = value.split("~")
        return (float(lv) + float(rv)) / 2.
    else:
        return value


def _apply_avoid(series, col, ctgys):
    """
    Description:
        , 로 구분된 문자열을 분리.
        ctgys에 있는 범주로 신규 칼럼 생성.
        칼럼명은 col_ctgy 식으로 처리.
    """
    value = series[col]
    if isinstance(value, str):
        # string
        value_list = [v for v in value.split(",") if not v == ""]
        check_value = np.isin(value_list, ctgys)
        value_list = [v if c else "기타" for v, c in zip(value_list, check_value)]
        for cond in ctgys:
            if cond in value_list:
                series["{0}_{1}".format(col, cond)] = True
            else:
                series["{0}_{1}".format(col, cond)] = False
    else:
        # NaN
        for cond in ctgys:
            series["{0}_{1}".format(col, cond)] = value
    return series


def _apply_str_to_bool(series, col):
    """
    Description:
        문자열로 된 True, False를 bool로 변환.
        None, NaN은 원래 값이 나오도록 처리.
    """
    try:
        _bool = strtobool(series[col])
    except Exception as e:
        return series[col]
    else:
        if _bool:
            return True
        else:
            return False


def _apply_random_nan(series, input_cols, nannum_col):
    """
    Description:
        nannum_col 값만큼 nan으로 변환할 칼럼 산출(비복원추출).
        데이터마다 해당 칼럼값은 nan으로 처리.
    """
    nan_cols = np.random.choice(
        input_cols,
        size=series[nannum_col],
        replace=False   # 비복원추출
    )
    series[nan_cols] = np.nan
    return series


def _apply_lower_upper_bound(series, cols):
    """
    Description:
        하한값/상한값을 맞게 설정.
        칼럼 순서는 하한, 상한 순서.
    """
    _low_col = cols[0]
    _high_col = cols[1]
    series[_low_col], series[_high_col] = series[cols].min(), series[cols].max()
    return series


def _apply_split_license(series, col, split_cols):
    """
    Description:
        리스트 값을 아래 범주대로 개별 칼럼 분리.
            - 제조업, 판매업, 사용업, 보관저장업, 운반업,
            - 면제, 무허가, 비영업자, 기타
        split_cols의 조건이 값에 포함되면 True 처리
    """
    if series[col] is None:
        pass
    else:
        for elm in series[col]:
            for k, v in split_cols.items():
                for v_elm in v:
                    if re.search(v_elm, elm):
                        series["{0}_{1}".format(col, k)] = True
    return series


def _apply_handling_material(data):
    """
    Description:
        주요 취급물질 갯수 계산.
            - 맨 끝이 ~종, ~품목, ~개 등은 해당 숫자로 처리
            - 아닌 경우에는 ", "로 split 하여 개수 계산
    """
    if isinstance(data, str):
        regex_1 = re.compile(r"\d+\w*\s*\w+$")
        extract_1 = regex_1.findall(data)
        if len(extract_1) > 0:
            regex_2 = re.compile(r"\d+")
            return int(regex_2.findall(extract_1[-1])[-1])
        else:
            return len(data.split(", "))
    else:
        return None


def _apply_acci_matt_sep(series, col, acci_matt_splits):
    """
    Description:
        사고물질_구분 처리.
            - acci_matt_splits로 시작하는 칼럼으로 분리
        acci_matt_splitss의 조건이 값으로 시작되면 True 처리
    """
    if series[col] is None:
        pass
    else:
        for elm in series[col]:
            for split in acci_matt_splits:
                new_col = "{0}_{1}".format(col, split)
                if elm.startswith(split):
                    series[new_col] = True
    return series


def _apply_amount_unit_coeff(data):
    """
    Description:
        저장량, 누출량에 곱할 계수 처리.
            - 톤, 세제곱미터, 리터 등을 kg으로 통일하기 위한 계수
    """
    if isinstance(data, str):
        if "t" in data or "ton" in data or "톤" in data:
            return 1000
        elif "㎥" in data or "m3" in data or "m_3" in data:
            return 1
        elif "kg" in data or "킬로그램" in data or "키로그램" in data or "킬로그람" in data:
            return 1
        elif "g" in data or "그램" in data or "그람" in data:
            return 0.001
        elif "ml" in data or "밀리리터" in data:
            return 0.001
        elif "l" in data or "리터" in data:
            return 1
        elif "cc" in data:
            return 0.001
        else:
            return None


def _apply_amount_float(data):
    """
    Description:
        저장량, 누출량 처리.
            - , 있으면 Max 값만 추출
            - ~ 들어간 구간은 평균 처리
            - 문자열은 실수화
            - None과 NaN은 bypass
    """
    # Dataframe의 apply와 Series의 apply 구별
    if isinstance(data, pd.Series):
        v = data.iloc[0]
    else:
        v = data
    # , 또는 ~ 있는 경우 처리, None과 NaN은 통과, 실수로 변환
    if isinstance(v, str) and "," in v:
        v = max([float(elm) for elm in v.split(",")])
    elif isinstance(v, str) and "~" in v:
        values = [float(elm) for elm in v.split("~") if len(elm) > 0]
        return sum(values) / len(values)
    elif pd.isna(v):
        return v
    else:
        if isinstance(data, pd.Series):
            return np.prod(data.astype(float))
        else:
            return float(data)


def _apply_date_to_weekday(data, week_list):
    """
    Description:
        yyyy-mm-dd를 요일로 변환.
        week_list = ["월", "화", "수", "목", "금", "토", "일"]
    """
    if data in week_list or data is None:
        return data
    else:
        y, m, d = [int(elm) for elm in data.split("-")]
        return week_list[date(y, m, d).weekday()]


def _apply_latlon_random(series, district_latlon):
    """
    도/특별시/광역시에 해당하는 무작위 위/경도 산출.
    """
    loc = series["지역"]
    loc_latlon = district_latlon.loc[loc].sample(n=1, replace=True).values[0]
    series["위도"] = loc_latlon[0]
    series["경도"] = loc_latlon[1]
    return series


def _apply_weighted_sum(series, weights, kst_tm):
    """
    Description:
        기상 API에서 구간 데이터를 선형보간하여 값 반환.
        가중치를 이용해서 가중평균 계산
        풍향은 x, y 성분 분리 후 내적 이용해서 각도 계산.
            - 라디안으로 변경 후 x, y축 성분만 추출
            - 가중치 적용해서 내적 계산 (너무 작은 값은 0으로 처리)
            - arctan 이용해서 각도 산출 후 degree로 변환
            - 직각 방향은 분기로 강제 처리
        시정이 0 이하이면 nan 처리.
        기압이 900보다 낮은 경우는 nan 처리 후 nanmax 반환.
    """
    # series 개수와 weights 길이는 같아야 함
    assert len(series) == len(weights), "Must be same length, Data nums & Weights length"
    col = series.name
    # print("{0}: {1}".format(col, series))
    if col == "관측시각_KST":
        return kst_tm
    elif col == "강수유무":
        return np.max(series)
    elif col == "평균풍향_10분":
        # 각 좌표계 -> 데카르트 좌표계
        theta1 = np.deg2rad(90 - series.iloc[-1])
        theta2 = np.deg2rad(90 - series.iloc[0])
        v1 = (np.cos(theta1), np.sin(theta1))
        v2 = (np.cos(theta2), np.sin(theta2))
        # 가중치 적용한 내적 (너무 작으면 0으로 간주)
        v_net = np.dot(np.array([v1, v2]).T, weights)
        v_net = np.where(np.abs(v_net) < 1e-5, 0, v_net)
        # arctan 이용해서 각도 산출 (양수 값만 나오도록 분기 처리)
        if v_net[0] == 0 and v_net[1] >= 0:
            # print("S")
            theta = 180
        elif v_net[0] == 0 and v_net[1] < 0:
            # print("N")
            theta = 0
        elif v_net[1] == 0 and v_net[0] > 0:
            # print("E")
            theta = 90
        elif v_net[1] == 0 and v_net[0] < 0:
            # print("W")
            theta = 270
        else:
            # print("other")
            if v_net[0] >= 0:
                theta = 90 - np.rad2deg(np.arctan(v_net[1] / v_net[0]))
            else:
                theta = 270 - np.rad2deg(np.arctan(v_net[1] / v_net[0]))
        return theta
    elif col == "평균풍속_10분":
        wind_spd = np.nanmax(series)
        if wind_spd < 0:
            wind_spd = np.nan
        return wind_spd
    elif col == "시정":
        vis_dist = np.nanmax(series)
        if vis_dist <= 0:
            vis_dist = np.nan
        return vis_dist
    elif col == "현지기압":
        filterd = [v if v >= 900 else np.nan for v in series]
        if np.any(np.isnan(filterd)):
            value = np.nanmax(filterd)
        else:
            value = np.sum(np.multiply(series, weights))
        return value
    else:
        return np.sum(np.multiply(series, weights))


def _apply_rainfall_YN(series, yn_col, rain_60m, rain_1d):
    """
    Description:
        기상 데이터에서 강수유무 재처리.
        강수_60분, 강수_1일에 0보다 큰 값이 있으면 True 이외는 False.
    """
    if np.isnan(series[yn_col]):
        return series[yn_col]
    elif np.isnan(series[rain_60m]) and np.isnan(series[rain_1d]):
        return np.nan
    else:
        _rainfall = np.nanmax(series[[rain_60m, rain_1d]])
        if _rainfall > 0:
            return True
        else:
            return False


def _apply_wind_direction(series, col, split_pts, direc_16):
    """
    Description:
        기상 데이터에서 풍향 재처리.
        AWS에서 사용하는 16방위로 사용.
        split_pts은 11.25도부터 22.5도 간격.
        direc_16은 NNE부터 N까지 나열.
    """
    direction = series[col]
    if np.isnan(direction):
        return direction
    idx = np.argmin(np.greater_equal(direction, split_pts)) - 1
    return direc_16[idx]
