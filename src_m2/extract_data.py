"""
수집 데이터 이용.
숭실대, 안전원에서 수집/제공한 엑셀 파일을 csv로 만들어 사용.

1. 숭실대
  - (수정중) 0630(숭실대)화학사고 DB 구축.xlsx
2. 데이터 확인용 파일
  - 고용보험 업종코드
    https://www.data.go.kr/data/15049592/fileData.do
  - CAS 번호 확인용
    https://www.data.go.kr/data/15067068/fileData.do
3. 안전원
  - (2015-2022) 화학통계DB_안전원.xlsx
"""
import os
import time
from collections import defaultdict
from itertools import repeat, chain
import numpy as np
import pandas as pd

from utils.log_module import print_elapsed_time
from utils.code_utils import unpack
from utils.df_utils import DataframeUtils
from utils.wrap_func import exec_col_func
from src_m2.weather_api import WeatherAPI
from src_m2.apply_func import (
    _apply_acci_location, _apply_acci_season, _apply_acci_timegroups,
    _apply_swungdash_float, _apply_avoid,
    _apply_split_license, _apply_handling_material,
    _apply_acci_matt_sep, _apply_amount_unit_coeff,
    _apply_amount_float, _apply_date_to_weekday,
    _apply_rainfall_YN, _apply_wind_direction
    )

import warnings
warnings.filterwarnings(action="ignore")


def make_time_ranges(time_group):
    """
    Description:
        https://www.yna.co.kr/view/AKR20170630181500004 기상청 기준 적용
        시간 범위와 시간대를 매핑
            - 한밤: 0~3 / 새벽: 3~6 / 아침: 6~9 / 늦은오전: 9~12
            - 이른오후: 12~15 / 늦은오후: 15~18 / 저녁: 18~21 / 늦은밤: 21~24
    Args:
        time_group: list, 시간대(종료시간대 순으로 0시부터, 늦은밤~저녁)
    Returns:
        time_ranges: dict, 시간 범위와 시간대를 매핑한 결과.
    """
    from datetime import time as dtt
    ranges = [dtt(i) for i in range(0, 24, 3)]
    time_ranges = dict()
    for r, g in zip(ranges, time_group):
        time_ranges[g] = r
    return time_ranges


class ChemAcciSSU(DataframeUtils):
    """숭실대 - 화학사고."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            숭실대 화학사고 수집 파일.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(ChemAcciSSU, self).__init__(log, err_log)
        cfg_ssu = cfg.EXTRACT.SSU
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.data_path = cfg_ssu.DATA_PATH
        self.dtype_path = cfg_ssu.DTYPE_PATH
        self.encoding = cfg_ssu.ENCODING
        self.use_cols = {k: v for v, k in unpack(cfg_ssu.USE_COLS).items()}   # namedtuple로 인해 k, v 반대
        self.time_ranges = make_time_ranges(cfg.EXTRACT.TIME_GROUP)
        self.wind_direc16 = cfg.EXTRACT.WIND_DIREC16
        self.avoid_cond_ctgys = cfg_ssu.AVOID_COND_CTGYS
        self.avoid_mat_ctgys = cfg_ssu.AVOID_MAT_CTGYS
        self.yn_cols = cfg_ssu.YN_COLS
        self.y_values = cfg_ssu.Y_VALUES
        self.danger_hfr = cfg_ssu.DANGER_HFR
        self.danger_s = {k: v for k, v in unpack(cfg_ssu.DANGER_S).items()}
        self.celcius_cols = cfg_ssu.CELCIUS_COLS
        self.save_filepath = cfg_ssu.SAVE_FILEPATH
        self.save_dtypepath = cfg_ssu.SAVE_DTYPEPATH
        
        self.wth = WeatherAPI(cfg, log, err_log)
        
        # 체크용
        self.emp = cfg.CHECK.EMPCODE
        self.cas = cfg.CHECK.CAS
    
    def __call__(self):
        """
        Description:
            숭실대 화학사고 수집 파일 불러온 뒤 1차 처리 후 필요 데이터 저장.
        Args:
            None
        Returns:
            ssu_df: 필요 칼럼 추출하여 1차 처리 완료된 데이터.
        """
        self.log.info(">>> Start to extract Collected Chemical accident (SSU)")
        start_time = time.time()
        
        # 데이터 불러오기
        ssu_df = self.load_ssu_df(
            self.data_path,
            self.dtype_path,
            self.encoding,
            self.use_cols
        )
        pcs_cols = [col for col in ssu_df.columns if col.endswith("공정")]
        
        # 칼럼별 처리
        # bypass: 위도, 경도
        ssu_df["지역"] = self.acci_location(ssu_df, "지역")
        ssu_df["사고계절"] = self.acci_season(ssu_df, "사고일자")
        ssu_df["사고시간대"] = self.acci_timegroups(ssu_df, "사고시간")
        ssu_df["사고발생장소"] = self.acci_site(ssu_df, "사고발생장소")
        ssu_df["사고원인"] = self.acci_reason(ssu_df, "사고원인")
        ssu_df["사고종류"] = self.acci_kind(ssu_df, "사고종류")
        ssu_df["고용업종코드_중분류"] = self.industry_midcode(ssu_df, "고용업종코드_중분류")
        ssu_df["사망자"] = self.dead_injured(ssu_df, "사망자")
        ssu_df["부상자"] = self.dead_injured(ssu_df, "부상자")
        ssu_df["환경피해"] = self.environ_damage(ssu_df, "환경피해")
        ssu_df["CAS번호"] = self.cas_number(ssu_df, "CAS번호")
        ssu_df["성상"] = self.matter_state(ssu_df, "성상")
        ssu_df["색상"] = self.color_smell(ssu_df, "색상", "무색")
        ssu_df["냄새"] = self.color_smell(ssu_df, "냄새", "무향")
        ssu_df["녹는점_섭씨"] = self.swungdash_float(ssu_df, "녹는점_섭씨")
        ssu_df["초기끓는점_섭씨"] = self.swungdash_float(ssu_df, "초기끓는점_섭씨")
        ssu_df["Log_Kow"] = self.swungdash_float(ssu_df, "Log_Kow")
        ssu_df = self.avoid_condition(ssu_df, "피해야할_조건", self.avoid_cond_ctgys)
        ssu_df = self.avoid_matter(ssu_df, "피해야할_물질", self.avoid_mat_ctgys)
        ssu_df[self.yn_cols] = self.yn_to_tf(ssu_df, self.yn_cols, self.y_values)
        ssu_df["사고물질의양_kg"] = self.acci_matter_quantity(ssu_df, "사고물질의양_kg")
        ssu_df = self.dangerous_nfpa_HFR(ssu_df, ["건강위험성", "화재위험성", "반응위험성"])
        ssu_df["특수위험성"] = self.dangerous_nfpa_S(ssu_df, "특수위험성")
        # 제외 칼럼들
        # ssu_df["공정도_OX"] = self.yn_to_tf(ssu_df, ["공정도_OX"], self.y_values)
        # ssu_df[pcs_cols] = self.yn_to_tf(ssu_df, pcs_cols, self.y_values)
        
        # 온도 하한선 처리
        ssu_df = self.celcius_lowerlimit(ssu_df, self.celcius_cols)
        
        # 사고일자+사고시간으로 사고시각(연월일시분KST) 생성
        ssu_df = self.make_acci_ymdh24mi(ssu_df, "사고_연월일시분")
        
        # API로 기상데이터 수집(사고_연월일시분, 위도, 경도 모두 있는 경우)
        wth_df = self.get_weather_data(
            ssu_df, "사고_연월일시분", "위도", "경도"
        )
        ssu_df = self.merge_acci_wth(ssu_df, wth_df)
        ssu_df["강수유무"] = self.rainfall_YN(ssu_df, "강수유무", "강수_60분", "강수_1일")
        ssu_df["평균풍향_10분"] = self.wind_direction(ssu_df, "평균풍향_10분")
        
        # 특수위험성이 ","로 구분된 문자열이 존재하므로 1개씩 분리하여 별도 행 처리
        split_warn = self.split_onerow_onedata(ssu_df, "특수위험성", ",")
        ssu_df = self.arrange_df(ssu_df, "특수위험성", split_warn)
        ssu_df["특수위험성"] = self.dangerous_nfpa_S_filter(ssu_df, "특수위험성")
        
        # CSV 파일 저장
        ssu_cols_type = self.make_cols_type(ssu_df)
        self.save_csv(ssu_df, self.save_filepath)
        self.save_cols_type(ssu_cols_type, self.save_dtypepath)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to extract Collected Chemical accident (SSU): {0} hr {1} min {2:.2f} sec".format(h, m, s))
        
        return ssu_df
    
    def load_ssu_df(self, data_path, dtype_path, encoding, use_cols):
        """
        Description:
            CSV 파일 불러오고 필요 칼럼만 남기기.
        Args:
            data_path: str, CSV 파일 경로
            dtype_path: str, CSV 파일의 칼럼별 타입 JSON 파일 경로
            encoding: str, CSV 파일 인코딩
            use_cols: dict, 기존 칼럼명: 변경할 칼럼명
        Returns:
            ssu_df: dataframe, 전체 데이터
        """
        cols_type = self.load_cols_type(dtype_path)
        ssu_df = self.load_csv(data_path, cols_type, encoding)
        ssu_df = self._filter_cols(ssu_df, use_cols)
        ssu_df = self._str_data_strip(ssu_df)
        self.log.info("Filtering columns & strip str contents")
        return ssu_df
    
    def _filter_cols(self, ssu_df, use_cols):
        """
        Description:
            사용할 칼럼만 분리 및 칼럼명 변경.
        Args:
            ssu_df: dataframe, 전체 데이터
            use_cols: dict, 기존 칼럼명: 변경할 칼럼명
        Returns:
            ssu_df: dataframe, 사용할 칼럼만 남은 전체 데이터
        """
        ssu_df = ssu_df[use_cols.keys()]
        ssu_df = ssu_df.rename(columns=use_cols)
        return ssu_df
    
    def _str_data_strip(self, ssu_df):
        """
        Description:
            문자열 값의 양 끝 공백 제거.
        Args:
            ssu_df: dataframe, 전체 데이터
        Returns:
            ssu_df: dataframe, 처리된 전체 데이터
        """
        for name, dt in zip(ssu_df.dtypes.index, ssu_df.dtypes):
            if dt == "object":
                ssu_df[name] = ssu_df[name].str.strip()
        return ssu_df
    
    @exec_col_func
    def acci_location(self, ssu_df, col):
        """
        Description:
            지역을 도/특별시/광역시 단위까지만 추출.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        return ssu_df.apply(lambda row: _apply_acci_location(row, col), axis=1)
    
    @exec_col_func
    def acci_season(self, ssu_df, col):
        """
        Description:
            사고일자로 사고계절 변환.
                - 봄: 3~5 / 여름: 6~8 / 가을: 9~11 / 겨울: 12~2
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        return ssu_df.apply(lambda row: _apply_acci_season(row, col), axis=1)
    
    @exec_col_func
    def acci_timegroups(self, ssu_df, col):
        """
        Description:
            사고시간으로 사고시간대 변환.
                - https://www.yna.co.kr/view/AKR20170630181500004 기상청 기준 적용
                - 한밤: 0~3 / 새벽: 3~6 / 아침: 6~9 / 늦은오전: 9~12
                - 이른오후: 12~15 / 늦은오후: 15~18 / 저녁: 18~21 / 늦은밤: 21~24
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        return ssu_df.apply(lambda row: _apply_acci_timegroups(row, col, self.time_ranges), axis=1)
    
    @exec_col_func
    def acci_site(self, ssu_df, col):
        """
        Description:
            사고발생장소 데이터 정리.
                - 공백 모두 제거
                - 원래 값을 일정 값으로 통일
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        ssu_df[col] = ssu_df[col].str.replace("\s", "")
        ssu_df[col] = ssu_df[col].str.replace("(야적장|옥외저장소|창고)", "저장설비")
        ssu_df[col] = ssu_df[col].str.replace("(실험실|실럼실|연구소)", "연구실")
        ssu_df[col] = ssu_df[col].str.replace("(작업장|공사장)", "사업장")
        ssu_df[col] = ssu_df[col].str.replace("(폐수처리장|폐액처리장|페저장설비|폐저장설비|배출장|소각장|배출설비)", "폐처리")
        ssu_df[col] = ssu_df[col].str.replace("액위측정기", "측정기")
        ssu_df[col] = ssu_df[col].str.replace("이송밸브", "밸브")
        ssu_df[col] = ssu_df[col].str.replace("운송차량", "차량")
        ssu_df[col] = ssu_df[col].str.replace("(파이프|이송호스)", "배관")
        ssu_df[col] = ssu_df[col].str.replace("(혼합기|교반기|배합기)", "혼합/교반/배합기")
        ssu_df[col] = ssu_df[col].str.replace("(가게|가정집|계곡)", "기타장소")
        ssu_df[col] = ssu_df[col].str.replace("(열분해기|배선|덕트|용해조|측정기|증착설비|열교환기|추출기)", "기타설비")
        return ssu_df[col]
    
    @exec_col_func
    def acci_reason(self, ssu_df, col):
        """
        Description:
            사고원인 데이터 정리.
                - 원래 값을 일정 값으로 통일
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        ssu_df[col] = ssu_df[col].str.replace("운송차량사고", "운송차량 사고")
        return ssu_df[col]
    
    @exec_col_func
    def acci_kind(self, ssu_df, col):
        """
        Description:
            사고종류 데이터 정리.
                - 공백 모두 제거
                - 원래 값을 일정 값으로 통일
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        ssu_df[col] = ssu_df[col].str.replace("\s", "")
        ssu_df[col] = ssu_df[col].str.replace("파열", "파손")
        ssu_df[col] = ssu_df[col].str.replace("정전기착화", "화재")
        ssu_df[col] = ssu_df[col].str.replace("폭발음발생", "폭발")
        ssu_df[col] = ssu_df[col].str.replace("(이상화학반응|물질반응)", "화학반응")
        return ssu_df[col]
    
    @exec_col_func
    def industry_midcode(self, ssu_df, col):
        """
        Description:
            사업장 업종명으로 고용업종코드(중분류) 변환.
                - 고용노동부 고용업종코드 파일 이용
                - 공백 모두 제거
                - 고용업종명(세세분류), 고용업종명(소분류) 로 사업장 업종명과 비교
                - 사업장 업종명으로 찾을 수 없으면 NaN 처리
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            result: series, 처리된 해당 칼럼 데이터
        """
        code_df = self.load_csv(self.emp.FILEPATH, self.emp.DTYPE, self.emp.ENCODING)
        code_df = code_df[self.emp.USE_COLS]
        detail, subdiv, midcode = self.emp.USE_COLS
        
        col_df = ssu_df[[col]]
        col_df[col] = col_df[col].str.replace("\s", "")
        for code_col in code_df.columns:
            code_df[code_col] = code_df[code_col].str.replace("\s", "")
        
        merge_detail = pd.merge(
            col_df, code_df[[detail, midcode]],
            how="left", left_on=col, right_on=detail
        )
        merge_subdiv = pd.merge(
            col_df, code_df[[subdiv, midcode]].drop_duplicates(),
            how="left", left_on=col, right_on=subdiv
        )
        
        result = pd.merge(
            merge_detail[[midcode]], merge_subdiv[[midcode]],
            left_index=True, right_index=True
        )
        result = result["{0}_x".format(midcode)].fillna(result["{0}_y".format(midcode)])
        return result
    
    @exec_col_func
    def dead_injured(self, ssu_df, col):
        """
        Description:
            사망자, 부상자 결측은 0으로 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        return ssu_df[col].fillna(0)
    
    @exec_col_func
    def environ_damage(self, ssu_df, col):
        """
        Description:
            환경피해 값이 있으면 True, 없으면 False.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        return ssu_df[col].notna()
    
    @exec_col_func
    def cas_number(self, ssu_df, col):
        """
        Description:
            CAS 번호 확인 후 없는건 nan 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            ssu_cas: series, 처리된 해당 칼럼 데이터
        """
        cas_cond = unpack(self.cas.CONDITION)
        cas_df = self.load_csv(self.cas.FILEPATH, self.cas.DTYPE, self.cas.ENCODING)
        cas_series = cas_df[cas_df[cas_cond["column"]] == cas_cond["value"]][self.cas.USE_COL]
        
        ssu_cas = ssu_df[col]
        cas_include = ssu_cas[ssu_cas.notna()].isin(cas_series)
        ssu_cas.loc[cas_include[cas_include == False].index] = np.nan
        return ssu_cas
    
    @exec_col_func
    def matter_state(self, ssu_df, col):
        """
        Description:
            성상을 고체/액체/기체 로 재범주화.
                - 공백 모두 제거
                - 액화가스는 액체로 처리
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        ssu_df[col] = ssu_df[col].str.replace("\s", "")
        ssu_df[col] = ssu_df[col].str.replace("액화가스", "액체")
        
        solid = ssu_df[col].str.contains("고체")
        solid = pd.Series(np.where(solid == True, "고체", None))
        fluid = ssu_df[col].str.contains("액체")
        fluid = pd.Series(np.where(fluid == True, "액체", None))
        gas = ssu_df[col].str.contains("기체")
        gas = pd.Series(np.where(gas == True, "기체", None))
        
        ssu_df[col].loc[:] = np.nan
        ssu_df[col].fillna(solid, inplace=True)
        ssu_df[col].fillna(fluid, inplace=True)
        ssu_df[col].fillna(gas, inplace=True)
        return ssu_df[col]
    
    @exec_col_func
    def color_smell(self, ssu_df, col, false_str):
        """
        Description:
            색상에서 무색은 False, 유색은 True로 변경.
            냄새에서 무향은 False, 유향은 True로 변경.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
            false_str: str, False로 처리할 문자열
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        ssu_df[col] = ssu_df[col].str.fullmatch(false_str)
        lambda_func = lambda row: np.logical_not(row[col]) if not np.isnan(row[col]) else row[col]
        return ssu_df.apply(lambda_func, axis=1)
    
    @exec_col_func
    def swungdash_float(self, ssu_df, col):
        """
        Description:
            ~ 이 있는 수치형 데이터인 경우 평균값으로 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        return ssu_df.apply(lambda row: _apply_swungdash_float(row, col), axis=1)
    
    @exec_col_func
    def avoid_condition(self, ssu_df, col, ctgys):
        """
        Description:
            피해야할 조건에서 , 있는 데이터를 별개 칼럼 bool 값으로 분리.
            범주 통합시킨 후 데이터 개수가 적은 경우 '기타'로 처리.
            별개 칼럼으로 분리 후에 분리 전 칼럼은 제거.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
            ctgys: list, 별개 칼럼으로 만들 범주
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        ssu_df[col] = ssu_df[col].str.replace("\s", "")
        ssu_df[col] = ssu_df[col].str.replace("습한공기", "습기")
        ssu_df[col] = ssu_df[col].str.replace("습기열", "습기,열")
        ssu_df[col] = ssu_df[col].str.replace("고열", "열")
        ssu_df = ssu_df.apply(lambda row: _apply_avoid(row, col, ctgys), axis=1)
        ssu_df.drop(col, axis="columns", inplace=True)
        return ssu_df
    
    @exec_col_func
    def avoid_matter(self, ssu_df, col, ctgys):
        """
        Description:
            피해야할 물질에서 , 있는 데이터를 별개 칼럼 bool 값으로 분리.
            범주 통합시킨 후 데이터 개수가 적은 경우 '기타'로 처리.
            별개 칼럼으로 분리 후에 분리 전 칼럼은 제거.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
            ctgys: list, 별개 칼럼으로 만들 범주
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        ssu_df[col] = ssu_df[col].str.replace("\n", ",")
        ssu_df[col] = ssu_df[col].str.replace("\s", "")
        ssu_df[col] = ssu_df[col].str.replace("불활성기체하에서취급하고", "활성기체")
        ssu_df[col] = ssu_df[col].str.replace("(습기를방지필요|습기)", "물")
        ssu_df[col] = ssu_df[col].str.replace("환연성물질", "환원성물질")
        ssu_df[col] = ssu_df[col].str.replace("가연성물질자극성", "가연성물질,자극성")
        ssu_df[col] = ssu_df[col].str.replace("(금속물|중금속|파편화된금속)", "금속")
        ssu_df[col] = ssu_df[col].str.replace("다수의유기화합물", "유기화합물")
        ssu_df[col] = ssu_df[col].str.replace("할로겐물질", "할로겐화물")
        ssu_df[col] = ssu_df[col].str.replace("강산", "산")
        ssu_df[col] = ssu_df[col].str.replace(",{1,5}", ",")
        ssu_df = ssu_df.apply(lambda row: _apply_avoid(row, col, ctgys), axis=1)
        ssu_df.drop(col, axis="columns", inplace=True)
        return ssu_df
    
    @exec_col_func
    def yn_to_tf(self, ssu_df, cols, y_values):
        """
        Description:
            유, Y, O는 True / 무, N, X는 False 로 변환.
        Args:
            ssu_df: dataframe, 전체 데이터
            cols: list, 칼럼명 리스트
            y_values: list, True로 만들 값
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        return ssu_df[cols].isin(y_values)[ssu_df[cols].notna()]
    
    @exec_col_func
    def acci_matter_quantity(self, ssu_df, col):
        """
        Description:
            사고물질 양은 , 만 제거한 뒤 float로 변환.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        ssu_df[col] = ssu_df[col].str.replace(",", "")
        return ssu_df[col].astype("float")
    
    @exec_col_func
    def dangerous_nfpa_HFR(self, ssu_df, cols):
        """
        Description:
            건강, 화재, 반응 위험성은 0 ~ 4 문자열만 남도록 함.
        Args:
            ssu_df: dataframe, 전체 데이터
            cols: list, 칼럼명 리스트
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        for col in cols:
            ssu_df[col] = ssu_df[col].str.strip()
            ssu_df[col][~ssu_df[col].isin(self.danger_hfr)] = np.nan
        return ssu_df
    
    @exec_col_func
    def dangerous_nfpa_S(self, ssu_df, col):
        """
        Description:
            특수위험성은 문자열 통일시키기.
            문자열 사이의 띄어쓰기는 ,로 변환.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        series = ssu_df[col]
        series = series.str.strip()
        series = series.str.replace(" ", ",")
        series = series.str.replace(pat=r",{1,10}", repl=",", regex=True)
        for af_value, bf_values in self.danger_s.items():
            for bf_value in bf_values:
                series = series.str.replace(bf_value, af_value)
        return series
    
    @exec_col_func
    def dangerous_nfpa_S_filter(self, ssu_df, col):
        """
        Description:
            특수위험성 사용 가능한 문자만 포함하고 이외는 nan 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        series = ssu_df[col]
        series = series.str.strip()
        series[~series.isin(self.danger_s.keys())] = np.nan
        return series
    
    @exec_col_func
    def celcius_lowerlimit(self, ssu_df, cols):
        """
        Description:
            섭씨 온도 하한선 -273.15로 고정.
        Args:
            ssu_df: dataframe, 전체 데이터
            cols: list, 칼럼명 리스트
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        for col in cols:
            ssu_df[col] = ssu_df[col].clip(lower=-273.15)
        return ssu_df
    
    def make_acci_ymdh24mi(self, ssu_df, api_time_col):
        """
        Description:
            기상 데이터 수집에 이용할 사고 연월일시분 생성.
            사고일자, 사고시간이 정상적인 데이터인 경우만 계산.
        Args:
            ssu_df: dataframe, 전체 데이터
            api_time_col: str, API에서 사용할 사고시각 칼럼명
        Returns:
            ssu_df: dataframe, 사고_연월일시분 계산된 데이터
        """
        def _accitime(series):
            if isinstance(series, str):
                _hr, _min = [int(elm) for elm in series.split(":")]
                return "{0:02d}{1:02d}".format(_hr, _min)
            else:
                return series
        
        # 사고 시간에서 : 떼고 4자리로 만들기
        accitime = ssu_df["사고시간"].apply(lambda row: _accitime(row))
        # 연월일시분_KST: 사고일자+사고시간 (12자리)
        accitime = ssu_df["사고일자"].str.cat(accitime, na_rep="")
        accitime[accitime.str.len() != 12] = np.nan
        ssu_df[api_time_col] = accitime
        
        return ssu_df
    
    def get_weather_data(self, ssu_df, time_col, lat_col, lon_col):
        """
        Description:
            API를 이용하여 기상데이터 수집.
            사고_연월일시분, 위도, 경도 모두 있는 경우에만 수집.
            수집항목은 cfg.API.KMA.USE_COLS 에 활성화된 것.
        Args:
            ssu_df: dataframe, 전체 데이터
            time_col: str, 사고시각 칼럼명
            lat_col: str, 위도 칼럼명
            lon_col: str, 경도 칼럼명
        Returns:
            wth_df: dataframe, 기상 데이터
        """
        use_cols = [time_col, lat_col, lon_col]
        use_df = ssu_df[ssu_df[use_cols].notna().all(axis=1)]
        wth_df = self.wth(use_df, time_col, lat_col, lon_col)
        wth_df.index = use_df.index
        return wth_df
    
    def merge_acci_wth(self, ssu_df, wth_df):
        """
        Description:
            사고 데이터와 기상 데이터 병합.
            불필요 칼럼은 제외.
        Args:
            ssu_df: dataframe, 전체 데이터
            wth_df: dataframe, 기상 데이터
        Returns:
            ssu_df: dataframe, 병합 데이터
        """
        ssu_df = pd.merge(
            ssu_df.reset_index(drop=False),
            wth_df.reset_index(drop=False),
            left_on=["index", "사고_연월일시분"],
            right_on=["index", "관측시각_KST"],
            how="left"
        )
        # 불필요칼럼 삭제
        drop_cols = ["사고_연월일시분", "관측시각_KST", "index"]
        ssu_df = ssu_df.drop(columns=drop_cols)
        return ssu_df
    
    @exec_col_func
    def rainfall_YN(self, ssu_df, yn_col, rain_60m, rain_1d):
        """
        Description:
            강수60분, 강수1일이 있을 땐 True 아니면 False.
        Args:
            ssu_df: dataframe, 전체 데이터
            yn_col: str, 강수유무 칼럼명
            rain_60m: str, 강수_60분 칼럼명
            rain_1d: str, 강수_1일 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        series = ssu_df.apply(
            lambda row: _apply_rainfall_YN(row, yn_col, rain_60m, rain_1d),
            axis=1
        )
        return series
    
    @exec_col_func
    def wind_direction(self, ssu_df, col):
        """
        Description:
            평균풍향을 각도에서 16방위로 변경.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
        Returns:
            series: series, 처리된 해당 칼럼 데이터
        """
        split_pts = np.arange(start=11.25, stop=360+22.5, step=22.5)
        series = ssu_df.apply(
            lambda row: _apply_wind_direction(row, col, split_pts, self.wind_direc16),
        axis=1)
        return series
    
    def split_onerow_onedata(self, ssu_df, col, split_symbol):
        """
        Description:
            구분문자 기준 데이터를 1개씩 개별 행으로 분리.
            원래 dataframe 의 index를 유지(병합용).
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 개별 행으로 분리할 칼럼
            split_symbol: str, 분리 기준이 되는 문자
        Returns:
            split_data: dataframe, 값이 1개씩만 있는 전체 데이터
        """
        idx_col = "tmp_idx"
        split_data = pd.DataFrame(ssu_df[col].fillna("None").str.split(split_symbol), columns=[col])
        split_data[idx_col] = split_data.apply(lambda row: list(repeat(row.name, len(row[col]))), axis=1)
        split_data_idxs = list(chain(*split_data[idx_col].to_list()))
        split_data_values = list(chain(*split_data[col].to_list()))
        split_data = pd.DataFrame({col: split_data_values}, index=split_data_idxs)
        split_data[col] = split_data[col].replace({"None": None})
        return split_data
    
    def arrange_df(self, ssu_df, col, split_warn):
        """
        Description:
            2개의 dataframe을 병합 후 칼럼명 정리.
        Args:
            ssu_df: dataframe, 전체 데이터
            split_warn: dataframe, 특수위험성이 1개씩만 있는 전체 데이터
        Returns:
            ssu_df: dataframe, 개별 행 분리, 칼럼 및 데이터 정리된 전체 데이터
        """
        ssu_df = pd.merge(ssu_df[[c for c in ssu_df.columns if not c == col]],
                          split_warn,
                          how="right",
                          left_index=True,
                          right_index=True)
        return ssu_df.reset_index(drop=True)


if __name__ == "__main__":
    from setproctitle import setproctitle
    setproctitle("chem_test")
    
    # 로그 파일
    # from pathlib import Path
    from utils.log_module import LogConfig, get_log_view
    # upper_dir = Path.cwd()
    upper_dir = os.path.dirname(os.path.abspath(__file__))
    file_prefix = "test"
    lc = LogConfig(upper_dir, file_prefix)
    log = get_log_view(lc)   # default: INFO
    err_log = get_log_view(lc, log_level="WARNING", error_log=True)
    
    # 설정 파일
    from utils.code_utils import convert
    cfg = convert("./configs/collected.yaml")
    
    # 프로세스 실행
    ssu = ChemAcciSSU(cfg.EXTRACT, log, err_log)
    ssu()
    