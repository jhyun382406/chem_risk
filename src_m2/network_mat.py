"""
DNN + MAT 분류기의 MAT 부분.
deepchem 라이브러리의 MATModel, TorchModel 쪽을 오버라이드.
"""
from typing import Any, List, Tuple
import numpy as np
import torch
from deepchem.models.torch_models import MAT, MATModel
from deepchem.models.losses import L2Loss
from deepchem.feat import MATFeaturizer
from deepchem.utils.typing import RDKitAtom
from deepchem.utils.molecule_feature_utils import one_hot_encode

from MAT.src.transformer import make_model


class MATFeaturizerOrigin(MATFeaturizer):
    def __init__(self):
        pass
    
    # Override
    # MAT.src.featurization.data_utils의 get_atom_features 참고
    def atom_features(self, atom: RDKitAtom) -> np.ndarray:
        """Deepchem already contains an atom_features function, however we are defining a new one here due to the need to handle features specific to MAT.
        Since we need new features like Atom GetNeighbors and IsInRing, and the number of features required for MAT is a fraction of what the Deepchem atom_features function computes, we can speed up computation by defining a custom function.

        Parameters
        ----------
        atom: RDKitAtom
            RDKit Atom object.

        Returns
        ----------
        ndarray
            Numpy array containing atom features.

        """
        attrib = []
        attrib += one_hot_encode(
            atom.GetAtomicNum(),
            [5, 6, 7, 8, 9, 15, 16, 17, 35, 53, 999]
            )
        attrib += one_hot_encode(
            len(atom.GetNeighbors()),
            [0, 1, 2, 3, 4, 5]
            )
        attrib += one_hot_encode(
            atom.GetTotalNumHs(),
            [0, 1, 2, 3, 4]
            )

        attrib += one_hot_encode(
            atom.GetFormalCharge(),
            [-1, 0, 1]
            #  [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]
            )

        attrib.append(atom.IsInRing())
        attrib.append(atom.GetIsAromatic())

        return np.array(attrib, dtype=np.float32)



class ModifyMAT(MATModel):
    """
    DNN + MAT 모델에 사용할 MATModel 수정.
    """
    def __init__(
        self,
        dist_kernel: str = 'softmax',
        n_encoders=8,
        lambda_attention: float = 0.33,
        lambda_distance: float = 0.33,
        h: int = 16,
        sa_hsize: int = 1024,
        sa_dropout_p: float = 0.0,
        output_bias: bool = True,
        d_input: int = 1024,
        d_hidden: int = 1024,
        d_output: int = 1024,
        activation: str = 'leakyrelu',
        n_layers: int = 1,
        ff_dropout_p: float = 0.0,
        encoder_hsize: int = 1024,
        encoder_dropout_p: float = 0.0,
        embed_input_hsize: int = 36,
        embed_dropout_p: float = 0.0,
        gen_aggregation_type: str = 'mean',
        gen_dropout_p: float = 0.0,
        gen_n_layers: int = 1,
        gen_attn_hidden: int = 128,
        gen_attn_out: int = 4,
        gen_d_output: int = 1,
        **kwargs
        ):
        """deepchem 라이브러리의 MATModel과 동일."""
        self.model = MAT(
            dist_kernel=dist_kernel,
            n_encoders=n_encoders,
            lambda_attention=lambda_attention,
            lambda_distance=lambda_distance,
            h=h,
            sa_hsize=sa_hsize,
            sa_dropout_p=sa_dropout_p,
            output_bias=output_bias,
            d_input=d_input,
            d_hidden=d_hidden,
            d_output=d_output,
            activation=activation,
            n_layers=n_layers,
            ff_dropout_p=ff_dropout_p,
            encoder_hsize=encoder_hsize,
            encoder_dropout_p=encoder_dropout_p,
            embed_input_hsize=embed_input_hsize,
            embed_dropout_p=embed_dropout_p,
            gen_aggregation_type=gen_aggregation_type,
            gen_dropout_p=gen_dropout_p,
            gen_n_layers=gen_n_layers,
            gen_attn_hidden=gen_attn_hidden,
            gen_attn_out=gen_attn_out,
            gen_d_output=gen_d_output
        )
        self.loss = L2Loss()
        output_types = ['prediction']
        super(MATModel, self).__init__(self.model,
                                       loss=self.loss,
                                       output_types=output_types,
                                       **kwargs)
    
    def _prepare_batch(
        self, batch: Tuple[Any, Any, Any]
    ) -> Tuple[List[torch.Tensor], List[torch.Tensor], List[torch.Tensor]]:
        inputs, labels, weights = batch
        inputs = [
            x.astype(np.float32) if x.dtype == np.float64 else x for x in inputs
        ]
        input_tensors = [torch.as_tensor(x) for x in inputs]
        if labels is not None:
            labels = [
                x.astype(np.float32) if x.dtype == np.float64 else x
                for x in labels
            ]
            label_tensors = [
                torch.as_tensor(x) for x in labels
            ]
        else:
            label_tensors = []
        if weights is not None:
            weights = [
                x.astype(np.float32) if x.dtype == np.float64 else x
                for x in weights
            ]
            weight_tensors = [
                torch.as_tensor(x) for x in weights
            ]
        else:
            weight_tensors = []

        return (input_tensors, label_tensors, weight_tensors)
    
    def default_generator(
        self,
        mat_dataset,
        dnn_dataset,
        epochs=1,
        mode='fit',
        deterministic=True,
        pad_batches=True,
        **kwargs
        ):
        """
        deterministic: True면 non-shuffle / False면 random permutation
        pad_batches: True면 모든 batch 크기 동일 / False면 마지막은 batch 크기 다름
        """
        for epoch in range(epochs):
            for MAT_d, DNN_d in zip(
                mat_dataset.iterbatches(
                    batch_size=self.batch_size,
                    deterministic=deterministic,
                    pad_batches=pad_batches
                    ),
                dnn_dataset.iterbatches(
                    batch_size=self.batch_size,
                    deterministic=deterministic,
                    pad_batches=pad_batches
                    )
                ):
                X_b, y_b, w_b, ids_b = MAT_d
                X_b_2, y_b_2, w_b_2, ids_b_2 = DNN_d
                
                node_features = self.pad_sequence(
                    [torch.tensor(data.node_features).float() for data in X_b])
                adjacency_matrix = self.pad_sequence([
                    torch.tensor(data.adjacency_matrix).float() for data in X_b
                ])
                distance_matrix = self.pad_sequence([
                    torch.tensor(data.distance_matrix).float() for data in X_b
                ])

                inputs = [node_features, adjacency_matrix, distance_matrix]
                yield (inputs, [y_b], [w_b], X_b_2, y_b_2)


class ModifyMATV2(MATModel):
    """
    DNN + MAT 모델에 사용할 MATModel 수정.
    원본 논문의 Pretrained 모델 사용.
        - https://arxiv.org/abs/2002.08264
        - https://github.com/ardigen/MAT
    """
    def __init__(
        self,
        model_params,
        batch_size=32,
        pretrained_path=None,
        **kwargs
        ):
        self.pretrained_path = pretrained_path
        self.model = make_model(**model_params)
        if pretrained_path is not None:
            self._load_pretrained()
            self._freeze_pretrained()
        self.loss = L2Loss()
        
        output_types = ['prediction']
        super(MATModel, self).__init__(
            self.model,
            loss=self.loss,
            output_types=output_types,
            batch_size=batch_size,
            **kwargs
        )
    
    def _load_pretrained(self):
        """Pretrained model 불러오기."""
        pretrained_state_dict = torch.load(self.pretrained_path)
        mat_state_dict = self.model.state_dict()
        for name, param in pretrained_state_dict.items():
            if "generator" in name:
                continue
            if isinstance(param, torch.nn.Parameter):
                param = param.data
            mat_state_dict[name].copy_(param)
    
    def _freeze_pretrained(self):
        """Generator만 freeze하지 않음."""
        for name, param in self.model.named_parameters():
            if not name.startswith("generator"):
                param.requires_grad = False   # freeze 시킨다는 뜻
            # param.requires_grad = False   # freeze 시킨다는 뜻
    
    def _prepare_batch(
        self, batch: Tuple[Any, Any, Any]
    ) -> Tuple[List[torch.Tensor], List[torch.Tensor], List[torch.Tensor]]:
        inputs, labels, weights = batch
        input_tensors = self._cvt_float32tensor(inputs)
        label_tensors = self._cvt_float32tensor(labels)
        weight_tensors = self._cvt_float32tensor(weights)
        return (input_tensors, label_tensors, weight_tensors)
    
    def _cvt_float32tensor(self, arys):
        if arys is not None:
            arys = [
                x.astype(np.float32) if x.dtype == np.float64 else x
                for x in arys
            ]
            tensors = [torch.as_tensor(x) for x in arys]
        else:
            tensors = []
        return tensors
    
    def default_generator(
        self,
        mat_dataset,
        dnn_dataset,
        deterministic=True,
        pad_batches=True,
        **kwargs
        ):
        """
        deterministic: True면 non-shuffle / False면 random permutation
        pad_batches: True면 모든 batch 크기 동일 / False면 마지막은 batch 크기 다름
        """
        for MAT_d, DNN_d in zip(
            mat_dataset.iterbatches(
                batch_size=self.batch_size,
                deterministic=deterministic,
                pad_batches=pad_batches
                ),
            dnn_dataset.iterbatches(
                batch_size=self.batch_size,
                deterministic=deterministic,
                pad_batches=pad_batches
                )
            ):
            X_b, y_b, w_b, ids_b = MAT_d
            X_b_2, y_b_2, w_b_2, ids_b_2 = DNN_d
            
            node_features = self.pad_sequence(
                [torch.tensor(data.node_features).float() for data in X_b]
            )
            adjacency_matrix = self.pad_sequence(
                [torch.tensor(data.adjacency_matrix).float() for data in X_b]
            )
            distance_matrix = self.pad_sequence(
                [torch.tensor(data.distance_matrix).float() for data in X_b]
            )
            inputs = [node_features, adjacency_matrix, distance_matrix]
            
            yield (inputs, [y_b], [w_b], X_b_2, y_b_2)
