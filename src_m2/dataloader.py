"""
DNN+MAT 모델에 넣을 Dataloader.
"""
import time
import numpy as np
import pandas as pd
import deepchem as dc
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader

from utils.log_module import print_elapsed_time

import warnings
warnings.filterwarnings(action="ignore")


class DnnMATDataset(Dataset):
    def __init__(
        self,
        mat,
        featurizer,
        df,
        dnn_input_cols,
        tgt_col,
        cas_col,
        BATCH_SIZE
        ):
        super(DnnMATDataset, self).__init__()
        self.TRAIN_NUMS = len(df)
        self.BATCH_SIZE = BATCH_SIZE
        
        # Make generator (Using deepchem format)
        mat_ipt, dnn_ipt, tgt, tgt_onehot = self._split_df(df, dnn_input_cols, tgt_col, cas_col)
        self.target_nums = len(tgt_onehot.columns)
        mat_ds = self._make_mat_ds(featurizer, mat_ipt, tgt)
        dnn_ds = self._make_dnn_ds(dnn_ipt, tgt_onehot)
        self.generator = self._make_gen(mat, mat_ds, dnn_ds)
    
    def __len__(self):
        return self.TRAIN_NUMS // self.BATCH_SIZE + 1

    def __getitem__(self, index):
        return self.generator[index]
    
    def _split_df(self, df, dnn_input_cols, tgt_col, cas_col):
        mat_ipt = df[cas_col]
        dnn_ipt = df[dnn_input_cols]
        tgt = df[tgt_col]
        tgt_onehot = pd.get_dummies(tgt)   # one-hot encoding
        return mat_ipt, dnn_ipt, tgt, tgt_onehot
    
    def _make_mat_ds(self, featurizer, mat_ipt, tgt, shard_size=2):
        imloader = dc.data.InMemoryLoader(tasks=["task1"], featurizer=featurizer)
        mat_ds = imloader.create_dataset(
            zip(mat_ipt, tgt.astype(np.float32)),
            shard_size=shard_size
        )
        return mat_ds
    
    def _make_dnn_ds(self, dnn_ipt, tgt_onehot):
        return dc.data.NumpyDataset(X=dnn_ipt.values, y=tgt_onehot.values)
    
    def _make_gen(self, mat, mat_ds, dnn_ds):
        """
        deterministic: True면 non-shuffle / False면 random permutation
        pad_batches: True면 모든 batch 크기 동일 / False면 마지막은 batch 크기 다름
        """
        generator = mat.default_generator(
            mat_ds,
            dnn_ds,
            deterministic=True,
            pad_batches=False
        )
        return [elm for elm in generator]


class CustomCollateFn(object):
    def __init__(self, *params):
        """
        Pytorch-lightning Dataloader에 사용할 collate_fn 커스텀.
        일반 collate_fn 함수는 batch 만 넣을 수 있으나
        추가적인 인자를 넣고 싶다면 Class로 처리.
        """
        self.params = params
    
    def __call__(self, batch):
        """
        batch_size는 dataset에서 이미 설정하여 dataloader 꼴로만 변경.
        batch_size = 1로 강제하여 첫 번째 원소만 추출.
        batch_size >=2 이면 batch[1], ... 존재.
        """
        return batch[0]


class DnnMATDataloader(object):
    def __init__(self,
                 dnn_input_cols,
                 tgt_col,
                 cas_col,
                 hparams,
                 log,
                 err_log):
        super(DnnMATDataloader, self).__init__()
        self.dnn_input_cols = dnn_input_cols
        self.tgt_col = tgt_col
        self.cas_col = cas_col
        self.log, self.err_log = log, err_log
        
        self.batch_size = hparams.batch_size
        self.tv_ratio = hparams.train_valid_ratio
        self.random_seed = hparams.random_seed
        self.dl_workers = hparams.dl_workers
        
    def __call__(self,
                 df,
                 mat,
                 featurizer,
                 is_test=False):
        """
        Description:
            학습 / 검증 용으로 데이터 분리 후 dataloader 생성.
            데이터 분리할 때 shuffle도 진행.
        Args:
            df: dataframe, 학습/검증 데이터 또는 테스트 데이터
            mat: torch model, MAT 인스턴스
            featurizer: featurizer, MAT featurizer
            is_pred: bool, predict 모드인지 여부
        Returns:
            학습
                - train_dl: dataloader, 학습 데이터 dataloader
                - valid_dl: dataloader, 검증 데이터 dataloader
            테스트
                - test_dnn_df: dataframe, test에서만 사용, 테스트용 DNN 네트워크 입력 데이터
                - test_mat_df: dataframe, test에서만 사용, 테스트용 MAT 네트워크 입력 데이터
                - test_label: numpy array, test에서만 사용, 테스트용 입력 데이터의 label
                - test_dl: dataloader, 테스트 데이터 dataloader
        """
        start_time = time.time()
        self.log.info(">>> Start to Make dataloader")
        
        if not is_test:
            # Make train/valid dataset
            train_df, valid_df = self.split_train_valid_test(df, self.tv_ratio)
            train_ds = DnnMATDataset(
                mat, featurizer,
                train_df,
                self.dnn_input_cols, self.tgt_col, self.cas_col,
                self.batch_size
            )
            valid_ds = DnnMATDataset(
                mat, featurizer,
                valid_df,
                self.dnn_input_cols, self.tgt_col, self.cas_col,
                self.batch_size
            )
            
            # Make train/valid dataloader
            collate_fn = CustomCollateFn()
            train_dl = self.make_dl(train_ds, collate_fn)
            valid_dl = self.make_dl(valid_ds, collate_fn)
            
            h, m, s = print_elapsed_time(start_time)
            self.log.info(">>> End to Make dataloader: {0} hr {1} min {2:.2f} sec".format(h, m, s))
            
            return train_dl, valid_dl
            
        else:
            # Make test dataset
            test_ds = DnnMATDataset(
                mat, featurizer,
                df,
                self.dnn_input_cols, self.tgt_col, self.cas_col,
                self.batch_size
            )
            test_dnn_df = df[self.dnn_input_cols]
            test_cas_df = df[self.cas_col] 
            test_label = df[self.tgt_col]
            
            # Make test dataloader
            collate_fn = CustomCollateFn()
            test_dl = self.make_dl(test_ds, collate_fn)
            
            h, m, s = print_elapsed_time(start_time)
            self.log.info(">>> End to Make dataloader: {0} hr {1} min {2:.2f} sec".format(h, m, s))
            
            return test_dnn_df, test_cas_df, test_label, test_dl
    
    def split_train_valid_test(self, train_df, ratio):
        """
        Description:
            Dataframe을 학습 / 검증 데이터 셋으로 분리.
        Args:
            df: dataframe, 분리할 dataframe
            ratio: float, 분리할 데이터 비율
            log_txt: str, log에 덧붙일 텍스트
        Returns:
            train_df: dataframe, 학습 데이터
            valid_df: dataframe, 검증 데이터
        """
        train_ratio, valid_ratio = ratio
        t_v = train_ratio / (train_ratio + valid_ratio)
        train_df, valid_df = self.split_data_using_ratio(train_df, t_v)
        self.log.info("Ratio: train {0} / valid {1}".format(train_ratio, valid_ratio))
        self.log.info("Nums: train {0} / valid {1}".format(len(train_df), len(valid_df)))
        return train_df, valid_df
    
    def split_data_using_ratio(self, df, ratio):
        """
        Description:
            Dataframe을 ratio에 따라 분리.
        Args:
            df: dataframe, 분리할 dataframe
            ratio: float, 분리할 데이터 비율, [0, 1] 구간
        Returns:
            ratio_df: dataframe, ratio 만큼의 데이터
            residual_df: dataframe, 1 - ratio 만큼의 데이터
        """
        if ratio == 0:
            ratio_df = df.iloc[0:0]
            residual_df = df
        elif ratio == 1:
            ratio_df = df
            residual_df = df.iloc[0:0]
        else:
            ratio_df, residual_df = train_test_split(df,
                                                     train_size=ratio,
                                                     shuffle=True,
                                                     stratify=df[self.tgt_col],
                                                     random_state=self.random_seed)
        return ratio_df, residual_df
    
    def make_dl(self, ds, collate_fn):
        """
        Description:
            Dataframe을 ratio에 따라 분리.
        Args:
            ds: dataset, 배치 처리까지 된 데이터셋
            collate_fn: class or func, Dataloader에 커스텀 반영할 함수
        Returns:
            dl: dataloader, 데이터셋을 데이터로더 형태로 변경한 것
        """
        dl = DataLoader(
            ds,
            batch_size=1,   # 이미 배치 처리했기 때문에 1로 고정
            shuffle=False,
            drop_last=False,
            num_workers=self.dl_workers,
            collate_fn=collate_fn
        )
        return dl
