"""
DNN+MAT 분류기에 사용할 손실 함수.
"""
import torch
from torch import nn
import torch.nn.functional as F


class FocalLoss(nn.Module):
    """
    Easy Example의 weight를 줄이고
    Hard Negative Example에 대한 학습에 초점을 맞추는
    Cross Entropy Loss 함수의 확장판.
    
    최초 제시 논문: Tsung-Yi Lin et al, 2017, Focal Loss for Dense Object Detection
                    (arXiv:1708.02002)
    최초 논문에서 alpha=0.25, gamma=2.0 사용했으나 조정 가능.
    """
    def __init__(self, alpha=0.25, gamma=2.0, logits=False, reduce=True):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.logits = logits
        self.reduce = reduce

    def forward(self, inputs, targets):
        if self.logits:
            BCE_loss = F.binary_cross_entropy_with_logits(inputs, targets, reduce=False)
        else:
            BCE_loss = F.binary_cross_entropy(inputs, targets, reduce=False)
        pt = torch.exp(-BCE_loss)
        F_loss = self.alpha * (1-pt)**self.gamma * BCE_loss

        if self.reduce:
            return torch.mean(F_loss)
        else:
            return F_loss
