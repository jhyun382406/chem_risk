"""
pytorch-lightning을 이용한 DNN+MAT 학습.
"""
import torch
import pytorch_lightning as pl
import torch.nn as nn 

from src_m2.loss_function import FocalLoss

import warnings
warnings.filterwarnings(action="ignore")


class DnnMat(pl.LightningModule):
    """
    Description:
        Pytorch-lightning에 적용할 모델.
        각각 step 설정.
    """
    def __init__(self, hparams, model, f_log):
        """
        Description:
            DNN+MAT를 Pytorch-lightning 적용하기 위한 init.
            hyperparameters 저장은 train에서 별도로 진행 (재사용 가능하도록)
        Args:
            hparams: namedtuple, 하이퍼 파라미터
            model: Model, DNN+MAT 모델
            f_log: logger, 파일 및 스트림으로 찍을 로그 인스턴스
        Returns:
            None
        """
        super(DnnMat, self).__init__()
        # self.save_hyperparameters()   # Save all hyperparameters automatically
        # self.save_hyperparameters("hparams")   # Save selected arguments' hyperparameters automatically
        self.hyp = hparams
        self.model = model
        self.criterion = self.set_loss_func(
            hparams.loss_kind, *hparams.loss_args
        )
        self.loss_scale = hparams.loss_scale
        self.f_log = f_log

    def forward(self, x):
        """
        Description:
            DNN+MAT 결과.
        Args:
            x: tensor, Input data, (mat_inputs, mat_labels, mat_weights, dnn_inputs, dnn_labels)
        Returns:
            result: tensor, 모델 전체 결과, (batch_size, classes number)
            label: tensor, 입력 label 그대로 반환, (batch_size, classes number)
        """
        return self.model(x)

    def training_step(self, batch, batch_idx):
        """
        Description:
            * Override
            Train 중에 반영할 로직 (trainer.fit).
            Focal loss를 산출.
            각각 loss 및 learning rate scheduler에 의한 learning rate를 저장.
                - 각각 on_step, on_epoch, progress_bar 표출 처리
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            loss_dict: loss 결과 딕셔너리
        """
        loss = self._shared_step(batch, batch_idx, "train")

        # Add log some usefull stuff
        # on_step: save logger on step / on_epoch: save logger on epoch
        self.log(
            "loss",
            loss.item(),
            on_step=False,
            on_epoch=True,
            prog_bar=False,
            batch_size=self.hyp.batch_size
        )
        self.log(
            "lr_onecycle",
            self.lr_schedulers().get_last_lr()[0],
            on_step=False,
            on_epoch=True,
            prog_bar=False,
            batch_size=self.hyp.batch_size
        )
        return {"loss": loss}
    
    def on_train_end(self):
        """
        Description:
            * Override
            Train 종료 시 로그.
        Args:
            None
        Returns:
            None
        """
        super().on_train_end()
        self.f_log.info(">>> Train End")
    
    def validation_step(self, batch, batch_idx):
        """
        Description:
            * Override
            Train 중에 Validation에 반영할 로직 (trainer.fit).
            전체 val_data에 대해 Focal loss를 산출.
            각각 loss를 저장.
                - 각각 on_step, on_epoch, progress_bar 표출 처리
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            loss_dict: loss 결과 딕셔너리
        """
        loss = self._shared_step(batch, batch_idx, "train")
        
        # Add log some usefull stuff
        # on_step: save logger on step / on_epoch: save logger on epoch
        self.log(
            "val_loss",
            loss.item(),
            on_step=False,
            on_epoch=True,   # Recognized from EarlyStopping
            prog_bar=True,
            batch_size=self.hyp.batch_size
        )
        
        return {"val_loss": loss}
    
    def test_step(self, batch, batch_idx):
        """
        Description:
            * Override
            Test 중 반영할 로직 (trainer.test).
            전체 test_data에 대해 Focal loss를 산출.
            코사인 유사도도 산출.
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            loss: tensor, 최종 loss 값
        """
        loss = self._shared_step(batch, batch_idx, "eval")
        return loss
    
    def test_epoch_end(self, test_outputs):
        """
        Description:
            * Override
            Test 중 epoch 종료 시 로그.
            최종적으로 Loss 값을 로그로 출력.
        Args:
            test_outputs: list, test_step의 결과 값 (batch_size 별로 1개 원소)
        Returns:
            None
        """
        # # x_hat은 shape이 다르므로 별도 처리
        # x_hat_list = []
        # for i, test_output in enumerate(test_outputs):
        #     x_hat_list.append(test_output[-1])
        #     test_outputs[i] = test_output[:4]
        test_losses = torch.as_tensor(test_outputs)
        loss = torch.mean(test_losses)
        self.f_log.info(">>> Total Loss Mean: {0:.5f}".format(loss))
    
    def on_test_end(self):
        """
        Description:
            * Override
            Test 종료 시 로그.
        Args:
            None
        Returns:
            None
        """
        super().on_test_end()
        self.f_log.info(">>> Test End")
    
    def predict_step(self, batch, batch_idx):
        """
        Description:
            * Override
            Predict 중 반영할 로직 (trainer.predict).
            입력한 데이터에 대해 모델 결과값 산출.
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            x_hat: numpy array, 모델 결과 값, (batch_size, input_features)
        """
        x = batch
        # x = x.to(torch.float32)   # default dataloader dtype: torch.float64
        x_hat, _ = self.model(x)
        return x_hat.cpu().detach().numpy()   # convert to numpy array
    
    def _shared_step(self, batch, batch_idx, mode):
        """
        Description:
            * Override
            Train, Validation, Test 중 공통 로직.
            입력된 데이터에 대해 Focal loss를 산출.
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
            mode: str, 학습/검증은 train, 테스트는 eval
        Returns:
            loss: tensor, 최종 loss 값
        """
        x = batch
        # x = x.to(torch.float32)   # default dataloader dtype: torch.float64
        x_hat, label = self.model(x)
        loss = self.criterion(x_hat, label) * self.loss_scale
        
        if mode == "train":
            pass
        elif mode == "eval":
            pass
        else:
            pass
        
        return loss

    def configure_optimizers(self):
        """
        Description:
            * Override
            학습에서 Optimizer 반영.
            Learning rate Scheduler 이용.
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            opt_list: list, 옵티마이저
            lr_scheduler_list: list, Learning rate Scheduler가 개별 딕셔너리로 구성
        """
        # default: manage learning rate by epochs
        # setting "interval": "step": manage learning rate by steps
        opt = torch.optim.AdamW(self.parameters(), lr=self.hyp.lr)
        # Fast.AI's best practices :)
        scheduler = torch.optim.lr_scheduler.OneCycleLR(
            opt,
            max_lr=self.hyp.lr,
            epochs=self.hyp.epochs,
            steps_per_epoch=self.hyp.steps_per_epoch,
            div_factor=self.hyp.div_factor   # initial_lr = max_lr/div_factor, default=25.0
        )
        return [opt], [{"scheduler": scheduler, "name": "lr_onecycle", "interval": "step"}]
    
    def set_loss_func(self, kind, *args):
        """
        Description:
            학습에서 Loss function 반영.
        Args:
            kind: str, 사용할 손실함수 이름
            args: str or float, 손실함수에 적용할 인자
                - cce: reduction (str), label_smoothing (float)
                - focal: alpha (float), gamma (float), logits (bool), reduce (bool)
        Returns:
            loss: Loss, 손실함수
        """
        if kind == "cce":
            return nn.CrossEntropyLoss(reduction=args[0], label_smoothing=args[1])
        elif kind == "focal":
            return FocalLoss(alpha=args[0], gamma=args[1], logits=args[2], reduce=args[3])
        else:
            self.f_log.info(">>> Wrong loss function kinds: {0}. Just using 'cce' or 'focal'.".format(kind))
