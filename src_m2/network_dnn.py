"""
DNN + MAT 분류기의 DNN 부분.
"""
import torch
import torch.nn as nn


class DNNCls(nn.Module):
    def __init__(self, input_cols, hparams):
        super(DNNCls, self).__init__()
        
        # 설정 변수
        input_features = len(input_cols)
        layer_features = hparams.dnn_layer_features
        drp_rate = hparams.dnn_dropout_rate
        
        features = self._make_features(input_features, layer_features)
        
        # Construct DNN layer
        self.linears = nn.ModuleList()
        for _in, _out in zip(features[:-1], features[1:]):
            cls_layer = self._make_cls_layer(_in, _out)
            self.linears.append(cls_layer)
        terminal = self._make_terminal(features[-1], 1, drp_rate)
        self.linears.append(terminal)
    
    def _make_features(self, input_features, layer_features):
        return [input_features, *layer_features]
    
    def _make_cls_layer(self, in_features, out_features):
        layer = torch.nn.Sequential(
            nn.Linear(in_features=in_features, out_features=out_features, bias=True),
            nn.LayerNorm(out_features),
            nn.SELU()
        )
        return layer
    
    def _make_terminal(self, in_features, out_features, drp_rate=0.2):
        terminal = torch.nn.Sequential(
            nn.Dropout(drp_rate, inplace=True),
            nn.Linear(in_features=in_features, out_features=out_features, bias=True)
        )
        return terminal
    
    def forward(self, x):
        for layer in self.linears:
            x = layer(x)
        return x


class DNNClsV2(nn.Module):
    """
    DNN 모델 쪽에 Skip connection 을 추가.
    각각의 residual_block 마다 작은 오토인코더 형태로 만들기.
    TODO: https://velog.io/@gibonki77/ResNetwithPyTorch 을
          CNN에서 DNN으로 바꿔보기.
    """
    def __init__(self, input_cols, hparams):
        super(DNNClsV2, self).__init__()
        
        # 설정 변수
        input_features = len(input_cols)
        layer_features = hparams.dnn_layer_features
        drp_rate = hparams.dnn_dropout_rate
        terminal = hparams.dnn_terminal
        
        features = self._make_features(input_features, layer_features)
        
        # Construct DNN layer (Using skip-connection)
        self.res_blocks = nn.ModuleList()
        self.identities = nn.ModuleList()
        for _in, _out in zip(features[:-1], features[1:]):
            res_block, iden = self._make_res_block(_in, _out)
            self.res_blocks.append(res_block)
            self.identities.append(iden)
        self.terminal = self._make_terminal(features[-1], terminal, drp_rate)
    
    def _make_features(self, input_features, layer_features):
        return [input_features, *layer_features]
    
    def _make_cls_layer(self, in_features, out_features):
        layer = torch.nn.Sequential(
            nn.Linear(
                in_features=in_features,
                out_features=out_features,
                bias=True
            ),
            nn.LayerNorm(out_features),
            nn.SELU()
        )
        return layer
    
    def _make_res_block(self, in_features, out_features):
        mid_features = in_features // 4
        block = torch.nn.Sequential(
            self._make_cls_layer(in_features, mid_features),
            self._make_cls_layer(mid_features, mid_features),
            self._make_cls_layer(mid_features, out_features)
        )
        if in_features == out_features:
            iden = nn.Identity()
        else:
            iden = nn.Linear(in_features, out_features, bias=False)
        return block, iden
    
    def _make_terminal(self, in_features, out_features, drp_rate=0.2):
        terminal = torch.nn.Sequential(
            nn.Dropout(drp_rate, inplace=True),
            nn.Linear(
                in_features=in_features,
                out_features=out_features,
                bias=True
            )
        )
        return terminal
    
    def forward(self, x):
        for res, iden in zip(self.res_blocks, self.identities):
            x = res(x) + iden(x)
        x = self.terminal(x)
        return x
