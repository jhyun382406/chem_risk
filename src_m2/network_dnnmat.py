"""
DNN 네트워크와 MAT 네트워크를 합치고
마지막 단에 softmax 처리.
"""
import torch
import torch.nn as nn


class DnnMatCls(nn.Module):
    def __init__(self, mat, dnn_net, device, hparams):
        super(DnnMatCls, self).__init__()
        self.mat = mat
        self.mat_net = mat.model.to(device)
        self.dnn_net = dnn_net.to(device)
        self.last_net = self._make_last_net(
            hparams.output_categories,
            hparams.loss_kind,
            device
        )
        self.device = device
        self.hparams = hparams
    
    def forward(self, x):
        mat_inputs, mat_labels, weights = self.mat._prepare_batch(x[:3])
        mat_inputs = [elm.to(self.device) for elm in mat_inputs]
        dnn_inputs = torch.from_numpy(x[3]).float().to(self.device)
        dnn_labels = torch.from_numpy(x[4]).float().to(self.device)
        x1 = self.mat_net(mat_inputs)
        x2 = self.dnn_net(dnn_inputs)
        x = self._merge_net_rst(x1, x2, self.hparams.last_merge_method)
        x = self.last_net(x)
        return x, dnn_labels
    
    def _make_last_net(self, output_categories, loss_kind, device):
        """
        Pytorch에서 Categorical CrossEntropy 에는 Softmax가 이미 있음.
        """
        last_lin = nn.Linear(
            in_features=2,
            out_features=output_categories,
            bias=True
        )
        if loss_kind == "cce":
            last_net = torch.nn.Sequential(last_lin)
        else:
            last_net = torch.nn.Sequential(
                last_lin,
                nn.Softmax()
            )
        return last_net.to(device)
    
    def _merge_net_rst(self, mat_rst, dnn_rst, method):
        """
        Concat 또는 Add 방식으로 네트워크 결과 합치기.
        """
        if method == "cat":
            rst = torch.cat([mat_rst, dnn_rst], dim=-1)
        elif method == "add":
            rst = torch.add(mat_rst, dnn_rst)
        else:
            rst = torch.cat([mat_rst, dnn_rst], dim=-1)
        return rst


class DnnMatClsV2(DnnMatCls):
    def __init__(self, mat, dnn_net, device, hparams):
        super(DnnMatClsV2, self).__init__(
            mat, dnn_net, device, hparams
        )
    
    def forward(self, x):
        mat_inputs, mat_labels, weights = self.mat._prepare_batch(x[:3])
        node_features = mat_inputs[0].to(self.device)
        adjacency_matrix = mat_inputs[1].to(self.device)
        distance_matrix = mat_inputs[2].to(self.device)
        dnn_inputs = torch.from_numpy(x[3]).float().to(self.device)
        dnn_labels = torch.from_numpy(x[4]).float().to(self.device)
        
        batch_mask = (torch.sum(torch.abs(node_features), dim=-1) != 0).to(self.device)
        x1 = self.mat_net(
            node_features, batch_mask, adjacency_matrix, distance_matrix, None
        )
        x2 = self.dnn_net(dnn_inputs)
        x = self._merge_net_rst(x1, x2, self.hparams.last_merge_method)
        x = self.last_net(x)
        return x, dnn_labels
