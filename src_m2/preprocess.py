import os
import time
import joblib
from itertools import zip_longest
from collections import defaultdict
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from statsmodels import robust
from imblearn.over_sampling import SMOTENC
from category_encoders.ordinal import OrdinalEncoder
from category_encoders.cat_boost import CatBoostEncoder
from category_encoders.binary import BinaryEncoder
from sklearn.preprocessing import (
    MinMaxScaler, RobustScaler, StandardScaler,
    QuantileTransformer, PowerTransformer
)

from utils.log_module import print_elapsed_time
from utils.code_utils import unpack, flatten_iter
from utils.df_utils import DataframeUtils
from utils.wrap_func import exec_col_func
from src_m2.extract_data import make_time_ranges
from src_m2.weather_api import WeatherAPI
from src_m2.apply_func import (
    _apply_str_to_bool, _apply_random_nan, _apply_lower_upper_bound,
    _apply_acci_location, _apply_latlon_random,
    _apply_acci_date, _apply_date_random, _apply_acci_time,
    _apply_rainfall_YN
    )

import warnings
warnings.filterwarnings(action="ignore")


class PrepDatas(DataframeUtils):
    """결측 처리, 인코딩 및 스케일링 진행."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            결측 처리, 인코딩 및 스케일링 진행.
            학습 전처리는 결과 파일 저장, 추론 전처리는 전처리 데이터 전달.
                1. bool 값 복원
                2. 학습/테스트 셋 분리
                3. 결측값 처리
                4. Target 생성: Risk score 계산 (자체 rule 우선 적용), 학습 전처리에만 반영
                5. API 이용하여 기상데이터 수집(결측처리 무조건 해야 가능)
                    - 기상데이터 결측값 처리도 별도로 추가 진행
                6. 데이터 증식 및 추가 결측값 처리
                7. 범주형 features 인코딩
                8. 수치형, 범주형 features 스케일링
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(PrepDatas, self).__init__(log, err_log)
        cfg_ssu = cfg.PREPROCESS.SSU
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.data_path = cfg_ssu.DATA_PATH
        self.dtype_path = cfg_ssu.DTYPE_PATH
        self.data_encoding = cfg_ssu.DATA_ENCODING
        self.tgt_col = cfg_ssu.TARGET_COLNAME
        self.target_cols = unpack(cfg_ssu.TARGET_COLS)
        self.target_bins = cfg_ssu.TARGET_BINS
        self.cas_col = cfg_ssu.SPLIT.CAS_COL
        self.time_ranges = make_time_ranges(cfg.EXTRACT.TIME_GROUP)
        self.main_matters = cfg_ssu.SPLIT.MAIN_MATTERS
        self.total_test_ratio = cfg_ssu.SPLIT.TOTAL_TEST_RATIO
        self.main_test_ratio = cfg_ssu.SPLIT.MAIN_TEST_RATIO
        
        self.AUGMENT_USE = cfg_ssu.AUGMENT.USE
        self.ADD_RANDOM = cfg_ssu.AUGMENT.ADD_RANDOM
        self.REPEAT_NUM = cfg_ssu.AUGMENT.REPEAT_NUM
        self.POISSON_LAMBDA = cfg_ssu.AUGMENT.POISSON_LAMBDA
        self.MAX_NANNUM_RATIO = cfg_ssu.AUGMENT.MAX_NANNUM_RATIO
        
        self.fillna_ctgy_cols = unpack(cfg_ssu.FILLNA.CTGY_COLS)
        self.fillna_bool_cols = cfg_ssu.FILLNA.BOOL_COLS
        self.fillna_int_cols = unpack(cfg_ssu.FILLNA.INT_COLS)
        self.fillna_float_cols = unpack(cfg_ssu.FILLNA.FLOAT_COLS)
        self.fillna_latlon_cols = cfg_ssu.FILLNA.LATLON_COLS
        self.fillna_compare_cols = cfg_ssu.FILLNA.COMPARE_COLS
        
        self.encode_bool_cols = cfg_ssu.FILLNA.BOOL_COLS
        self.encode_ctgy_cols = unpack(cfg_ssu.ENCODING.CTGY_COLS)
        self.encode_none = cfg_ssu.ENCODING.ENCODE_NONE
        self.encode_nan = cfg_ssu.ENCODING.ENCODE_NAN
        self.encoder_save = cfg_ssu.ENCODING.SAVE_WHETHER
        self.encoder_path = cfg_ssu.ENCODING.SAVE_PATH
        
        self.scale_total_cols = unpack(cfg_ssu.SCALING.COLS)
        self.scaler_save = cfg_ssu.SCALING.SAVE_WHETHER
        self.scaler_path = cfg_ssu.SCALING.SAVE_PATH
        
        self.save_train_filepath = cfg_ssu.SAVE_FILEPATH.format("TRAIN")
        self.save_test_filepath = cfg_ssu.SAVE_FILEPATH.format("TEST")
        self.save_dtypepath = cfg_ssu.SAVE_JSONPATH
        
        self.wth = WeatherAPI(cfg, log, err_log)
        self.c2s = Cas2Smiles(cfg, log, err_log)
        
        # 체크용
        self.emp = cfg.CHECK.EMPCODE
        self.cas = cfg.CHECK.CAS
        self.district = cfg.CHECK.DISTRICT
        self.random_seed = cfg.TRAIN.HPARAMS.RANDOM_SEED

    def __call__(self):
        """
        Description:
            1차 처리된 숭실대 화학사고 수집 파일 불러온 뒤 전처리 후 저장.
            전처리 과정
                - 학습/테스트 데이터 인덱스 분리
                - 1차 결측 처리
                - target 생성
                - 데이터 증식 및 2차 결측 처리
                - 인코딩
                - 스케일링
        Args:
            None
        Returns:
            None
        """
        self.log.info(">>> Start to preprocess Collected Chemical accident (SSU)")
        start_time = time.time()
        
        # 데이터 불러오기
        ssu_df = self.load_csv_df(
            self.data_path,
            self.dtype_path,
            self.data_encoding,
            self.fillna_bool_cols
        )
        train_indices, test_indices = self.make_train_test_indices(
            ssu_df,
            self.cas_col,
            self.main_matters,
            self.total_test_ratio,
            self.main_test_ratio
        )
        
        # 1차 결측 처리
        ssu_df = self.fill_cols_ctgy(ssu_df, self.fillna_ctgy_cols)
        ssu_df = self.fill_cols_bool(ssu_df, self.fillna_bool_cols)
        ssu_df = self.fill_cols_int(ssu_df, self.fillna_int_cols)
        ssu_df = self.fill_cols_float(ssu_df, self.fillna_float_cols)
        ssu_df = self.fill_cols_latlon(ssu_df, self.fillna_latlon_cols)
        
        # 값 맞게 추가 조정(1차)
        ssu_df = self.lower_upper_bound(ssu_df, self.fillna_compare_cols)
        
        # target 생성
        ssu_df, target_dict = self.make_target(ssu_df, self.target_cols, self.target_bins)
        
        # 사고일자+사고시간으로 사고시각(연월일시분KST) 생성
        ssu_df = self.make_acci_ymdh24mi(ssu_df, "사고_연월일시분")
        
        # API로 기상데이터 수집
        wth_df = self.get_weather_data(
            ssu_df, "사고_연월일시분", "위도", "경도"
        )
        ####################################################################
        # 임시
        self.save_csv(wth_df, "./processed_data/kma_wth_df.csv")
        ####################################################################
        
        # 사고데이터와 기상데이터 병합, 불필요칼럼 삭제
        drop_cols = ["위도", "경도", "사고일자", "사고시간", "사고_연월일시분", "관측시각_KST"]
        ssu_df = self.merge_acci_wth(ssu_df, wth_df, drop_cols)
        
        ####################################################################
        
        # # 인코더, 스케일러 생성 (copy로 미리 인코더, 스케일러 만들고 증식 후 적용)
        # copy_df = ssu_df.copy()
        # self.industry_midcode_list()   # 파일에서 고용업종코드_중분류 불러오기
        # encoder_dict = self.make_encoder(copy_df, self.encode_ctgy_cols)
        # copy_df = self.encode_cols_bool(copy_df, self.encode_bool_cols)
        # copy_df = self.encode_cols_ctgy(copy_df, self.encode_ctgy_cols, encoder_dict)
        # scaler_dict = self.make_scaler(copy_df, self.scale_total_cols)
        # saved_encoder_dict = self.save_encoder_dict(encoder_dict, target_dict, self.encoder_path)
        # self.save_scaler_dict(scaler_dict, self.scaler_path)
        
        # # 데이터 증식 (train 데이터만 증식)
        # train_df, test_df = self.make_train_test_df(ssu_df, train_indices, test_indices)
        # TEST_SIZE = len(test_df)
        # if self.AUGMENT_USE:
        #     train_df = self.augment_data(
        #         train_df,
        #         self.ADD_RANDOM,
        #         self.REPEAT_NUM,
        #         self.POISSON_LAMBDA,
        #         self.MAX_NANNUM_RATIO
        #     )
            
        #     # 2차 결측 처리
        #     train_df = self.fill_cols_ctgy(train_df, self.fillna_ctgy_cols)
        #     train_df = self.fill_cols_bool(train_df, self.fillna_bool_cols)
        #     fillna_int_cols_2nd = {
        #         col: args
        #         for col, args in self.fillna_int_cols.items()
        #         if not col in self.target_cols
        #     }
        #     train_df = self.fill_cols_int(train_df, fillna_int_cols_2nd)
        #     train_df = self.fill_cols_float(train_df, self.fillna_float_cols)
        # ssu_df = pd.concat([train_df, test_df]).reset_index(drop=True)
        
        # # 값 맞게 추가 조정(2차)
        # ssu_df = self.lower_upper_bound(ssu_df, self.fillna_compare_cols)
        
        # # 인코딩
        # ssu_df = self.encode_cols_bool(ssu_df, self.encode_bool_cols)
        # ssu_df = self.encode_cols_ctgy(ssu_df, self.encode_ctgy_cols, encoder_dict)
        # ssu_df = self.scale_cols(ssu_df, scaler_dict)
        
        # # 스케일링 후 train/test 데이터 재분리
        # train_df, test_df = ssu_df[:-TEST_SIZE], ssu_df[-TEST_SIZE:]
        # self.log.info(">>> Train nums: {0} / Test nums: {1}".format(len(train_df), TEST_SIZE))
        
        # # CSV 파일 저장
        # ssu_cols_type = self.make_cols_type(ssu_df)
        # self.save_csv(train_df, self.save_train_filepath)
        # self.save_csv(test_df, self.save_test_filepath)
        # self.save_cols_type(ssu_cols_type, self.save_dtypepath)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to preprocess Collected Chemical accident (SSU): {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def load_csv_df(self, data_path, dtype_path, encoding, bool_cols):
        """
        Description:
            CSV 파일 불러오기.
            결측으로 인해 object로 지정된 bool 타입 데이터도 복원.
        Args:
            data_path: str, CSV 파일 경로
            dtype_path: str, CSV 파일의 칼럼별 타입 JSON 파일 경로
            encoding: str, CSV 파일 인코딩
            bool_cols: list, bool 타입 칼럼
        Returns:
            ssu_df: dataframe, 전체 데이터
        """
        cols_type = self.load_cols_type(dtype_path)
        ssu_df = self.load_csv(data_path, cols_type, encoding)
        for col in bool_cols:
            ssu_df[col] = ssu_df.apply(lambda row: _apply_str_to_bool(row, col), axis=1)
        self.log.info("Restore bool type data")
        return ssu_df
    
    @exec_col_func
    def make_train_test_indices(self,
                                ssu_df,
                                cas_col,
                                main_matters,
                                total_test_ratio,
                                main_test_ratio):
        """
        Description:
            학습/테스트 index 분리.
            주요 물질은 main_test_ratio 비율 정도 테스트 셋에 배정.
        Args:
            ssu_df: dataframe, 전체 데이터
            cas_col: str, CAS번호 칼럼명
            main_matters: list, 주요 물질 CAS번호
            total_test_ratio: float, 전체 테스트 데이터 비율 [0, 1]
            main_test_ratio: float, 주요 물질 테스트 데이터 비율 [0, 1]
        Returns:
            train_indices: numpy array, 학습에 사용할 index
            test_indices: numpy array, 테스트에 사용할 index
        """
        main_matters_indices = ssu_df[ssu_df[cas_col].isin(main_matters)].index
        sub_matters_indices = ssu_df[~ssu_df[cas_col].isin(main_matters)].index
        
        TEST_NUMS = int(np.ceil(len(ssu_df) * total_test_ratio))
        split_main = train_test_split(
            main_matters_indices,
            test_size=main_test_ratio,
            shuffle=False,
            random_state=self.random_seed
        )
        TEST_SUB_RATIO = (TEST_NUMS - len(split_main[1])) / len(sub_matters_indices)
        split_sub = train_test_split(
            sub_matters_indices,
            test_size=TEST_SUB_RATIO,
            shuffle=False,
            random_state=self.random_seed
        )
        
        train_indices = np.concatenate((split_main[0], split_sub[0]))
        test_indices = np.concatenate((split_main[1], split_sub[1]))
        np.random.shuffle(train_indices)
        np.random.shuffle(test_indices)
        self.log.info("Split train/test indices: {0} / {1}".format(len(train_indices), len(test_indices)))
        
        return train_indices, test_indices
    
    def fill_cols_ctgy(self, ssu_df, ctgy_cols):
        """
        Description:
            범주형 데이터 결측 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            ctgy_cols: dict, {범주형 칼럼: 범주 데이터}
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        # 칼럼, arguments 분리
        fill_methods_ctgy = self._make_methods(ctgy_cols)
        choice_dict = self._extract_methods(fill_methods_ctgy, "choice")
        # 칼럼 별 적용
        for col, args in choice_dict.items():
            ssu_df = self._fill_random_choice_ctgy(ssu_df, col, **args)
        self.log.info("Fill category columns' missing value")
        return ssu_df
    
    def fill_cols_bool(self, ssu_df, bool_cols):
        """
        Description:
            bool형 데이터 결측 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            bool_cols: list, bool형 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        ssu_df = self._fill_random_choice_bool(ssu_df, bool_cols)
        self.log.info("Fill boolean columns' missing value")
        return ssu_df
    
    def fill_cols_int(self, ssu_df, int_cols):
        """
        Description:
            int형 데이터 결측 처리.
            적용한 결측 처리 방법
                - random_pareto
        Args:
            ssu_df: dataframe, 전체 데이터
            int_cols: dict, {int형 칼럼: 결측 처리 방법}
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        # 칼럼, arguments 분리
        fill_methods_int = self._make_methods(int_cols)
        pareto_dict = self._extract_methods(fill_methods_int, "pareto")
        # 칼럼 별 적용
        for col, args in pareto_dict.items():
            ssu_df = self._fill_random_pareto_int(ssu_df, col, **args)
        self.log.info("Fill integer columns' missing value")
        return ssu_df
    
    def fill_cols_float(self, ssu_df, float_cols):
        """
        Description:
            float형 데이터 결측 처리.
            적용한 결측 처리 방법 (1개 칼럼 기준 groupby도 지원)
                - random_logistic
                - random_gaussian
                - random_triangular
                - random_pareto
                - random_choice
                - random_chisquare
        Args:
            ssu_df: dataframe, 전체 데이터
            float_cols: dict, {float형 칼럼: 결측 처리 방법}
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        # 칼럼, arguments 분리
        fill_methods_float = self._make_methods(float_cols)
        logistic_dict = self._extract_methods(fill_methods_float, "logistic")
        gaussian_dict = self._extract_methods(fill_methods_float, "gaussian")
        triangular_dict = self._extract_methods(fill_methods_float, "triangular")
        pareto_dict = self._extract_methods(fill_methods_float, "pareto")
        choice_dict = self._extract_methods(fill_methods_float, "choice")
        chisquare_dict = self._extract_methods(fill_methods_float, "chisquare")
        
        grp_logistic_dict = self._extract_methods(fill_methods_float, "group_logistic")
        grp_gaussian_dict = self._extract_methods(fill_methods_float, "group_gaussian")
        grp_triangular_dict = self._extract_methods(fill_methods_float, "group_triangular")
        grp_pareto_dict = self._extract_methods(fill_methods_float, "group_pareto")
        grp_choice_dict = self._extract_methods(fill_methods_float, "group_choice")
        grp_chisquare_dict = self._extract_methods(fill_methods_float, "group_chisquare")
        
        # 칼럼 별 적용
        for col, args in logistic_dict.items():
            ssu_df = self._fill_random_logistic_float(ssu_df, col, **args)
        for col, args in gaussian_dict.items():
            ssu_df = self._fill_random_gaussian_float(ssu_df, col, **args)
        for col, args in triangular_dict.items():
            ssu_df = self._fill_random_triangular_float(ssu_df, col, **args)
        for col, args in pareto_dict.items():
            ssu_df = self._fill_random_pareto_float(ssu_df, col, **args)
        for col, args in choice_dict.items():
            ssu_df = self._fill_random_choice_float(ssu_df, col)
        for col, args in chisquare_dict.items():
            ssu_df = self._fill_random_chi_float(ssu_df, col, **args)
        
        for col, args in grp_logistic_dict.items():
            ssu_df = self._fill_random_logistic_float_group(ssu_df, col, **args)
        for col, args in grp_gaussian_dict.items():
            ssu_df = self._fill_random_gaussian_float_group(ssu_df, col, **args)
        for col, args in grp_triangular_dict.items():
            ssu_df = self._fill_random_triangular_float_group(ssu_df, col, **args)
        for col, args in grp_pareto_dict.items():
            ssu_df = self._fill_random_pareto_float_group(ssu_df, col, **args)
        for col, args in grp_choice_dict.items():
            ssu_df = self._fill_random_choice_float_group(ssu_df, col, **args)
        for col, args in grp_chisquare_dict.items():
            ssu_df = self._fill_random_chi_float_group(ssu_df, col, **args)
        
        self.log.info("Fill float columns' missing value")
        
        return ssu_df
    
    def _make_methods(self, cols_dict):
        """
        Description:
            칼럼, arguments 분리.
        Args:
            cols_dict: dict, {칼럼명: 데이터 처리 방법}
        Returns:
            methods: dict, {처리방법: {칼럼명: arguments}}
        """
        methods = defaultdict(dict)
        for col, details in cols_dict.items():
            if "args" in details.keys():
                methods[details["method"]][col] = {k: v for k, v in details["args"].items()}
            else:
                methods[details["method"]][col] = None
        return methods
    
    def _extract_methods(self, methods, key):
        """
        Description:
            칼럼, arguments 분리.
        Args:
            methods: dict, {처리방법: {칼럼명: arguments}}
            key: str, 처리방법 키 값
        Returns:
            one_method: dict, {칼럼명: arguments}
        """
        if key in methods.keys():
            return methods[key]
        else:
            return dict()
    
    @exec_col_func
    def _fill_random_choice_ctgy(self, ssu_df, col, KEEP_RATIO, CTGY_LIST=None):
        """
        Description:
            무작위값으로 범주형 데이터 결측 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 범주형 칼럼
            KEEP_RATIO: bool, 결측 처리시 범주값 비율 유지 여부
            CTGY_LIST: list, 범주값 리스트 (default: None)
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        nan_indices = ssu_df[ssu_df[col].isna()].index
        if KEEP_RATIO:
            col_data = ssu_df[col].value_counts() / ssu_df[col].notna().sum()
            random_value = np.random.choice(
                col_data.index.tolist(),
                size=len(nan_indices),
                p=col_data.tolist()
            )
        else:
            random_value = np.random.choice(CTGY_LIST, size=len(nan_indices))
        ssu_df[col].loc[nan_indices] = random_value
        return ssu_df
    
    @exec_col_func
    def _fill_random_choice_bool(self, ssu_df, bool_cols):
        """
        Description:
            무작위값으로 bool형 데이터 결측 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            bool_cols: list, bool형 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        for col in bool_cols:
            nan_indices = ssu_df[ssu_df[col].isna()].index
            random_value = np.random.choice([True, False], size=len(nan_indices))
            ssu_df[col].loc[nan_indices] = random_value
        return ssu_df
    
    @exec_col_func
    def _fill_random_pareto_int(self, ssu_df, col, PARETO_PARAM, FILL_MIN=0):
        """
        Description:
            파레토 분포의 무작위값으로 int형 데이터 결측 처리.
            파레토 지표는 2로 고정.
            파레토 분포의 무작위값에 IQR을 곱하여 최종 결측값 생성.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, int값 칼럼
            PARETO_PARAM: int, 파레토 지표 (Pareto distribution shape parameter)
            FILL_MIN: int, 최소하한선
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        nan_indices = ssu_df[ssu_df[col].isna()].index
        IQR = np.subtract(*np.nanquantile(ssu_df[col], q=[0.75, 0.25]))
        if IQR < 0.5:
            IQR = 0.5
        random_value = np.random.pareto(PARETO_PARAM, size=len(nan_indices)) * IQR
        ssu_df[col].loc[nan_indices] = np.round(random_value, 0) + FILL_MIN
        return ssu_df
    
    def __fill_random_logistic_float(self, ssu_df, col, CELCIUS):
        """
        Description:
            로지스틱 분포의 무작위값으로 float형 데이터 결측 처리.
            Robust 값 영향 줄이기 위해 [0.05, 0.95) 데이터만 이용.
            로지스틱 분포의 loc은 중간값, scale은 MAD (Median Absolute Deviation) 이용.
            표준편차 값이 너무 작으면 전체 데이터의 MAD 이용.
            섭씨온도인 경우, 최솟값을 -273.15로 clipping 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            CELCIUS: bool, 섭씨 데이터이면 True
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        nan_indices = ssu_df[ssu_df[col].isna()].index
        quant_5, quant_95 = np.nanquantile(ssu_df[col], q=[0.05, 0.95])
        use_data = ssu_df[(ssu_df[col] >= quant_5) & (ssu_df[col] < quant_95)][col]
        mu, sigma = use_data.median(), robust.mad(use_data)
        if sigma < 1e-3:
            sigma = robust.mad(ssu_df[ssu_df[col].notna()][col])
        random_value = np.random.logistic(mu, sigma, size=len(nan_indices))
        if CELCIUS:
            random_value = np.clip(random_value, -273.15, np.inf)
        ssu_df[col].loc[nan_indices] = random_value
        return ssu_df
    
    @exec_col_func
    def _fill_random_logistic_float(self, ssu_df, col, CELCIUS):
        """
        Description:
            로지스틱 분포의 무작위값으로 float형 데이터 결측 처리.
            칼럼명 로그만 추가로 찍는 bypass 함수.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            CELCIUS: bool, 섭씨 데이터이면 True
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        return self.__fill_random_logistic_float(ssu_df, col, CELCIUS)
    
    @exec_col_func
    def _fill_random_logistic_float_group(self, ssu_df, col, CELCIUS, GROUPBY):
        """
        Description:
            로지스틱 분포의 무작위값으로 float형 데이터 결측 처리.
            GROUP별로 별도로 묶어 계산하여 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            CELCIUS: bool, 섭씨 데이터이면 True
            GROUPBY: str, group할 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        ssu_dfs = [
            ssu_df[ssu_df[GROUPBY] == elm]
            for elm in ssu_df[GROUPBY].unique()
        ]
        ssu_dfs = [
            self.__fill_random_logistic_float(elm_df, col, CELCIUS)
            for elm_df in ssu_dfs
        ]
        ssu_df = pd.concat(ssu_dfs).sort_index()
        return ssu_df
    
    def __fill_random_gaussian_float(self, ssu_df, col, CELCIUS):
        """
        Description:
            정규 분포의 무작위값으로 float형 데이터 결측 처리.
            Robust 값 영향 줄이기 위해 [0.05, 0.95) 데이터만 이용.
            정규 분포의 loc은 중간값, scale은 MAD (Median Absolute Deviation) 이용.
            표준편차 값이 너무 작으면 전체 데이터의 MAD 이용.
            섭씨온도인 경우, 최솟값을 -273.15로 clipping 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            CELCIUS: bool, 섭씨 데이터이면 True
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        nan_indices = ssu_df[ssu_df[col].isna()].index
        quant_5, quant_95 = np.nanquantile(ssu_df[col], q=[0.05, 0.95])
        use_data = ssu_df[(ssu_df[col] >= quant_5) & (ssu_df[col] < quant_95)][col]
        mu, sigma = use_data.median(), robust.mad(use_data)
        if sigma < 1e-3:
            sigma = robust.mad(ssu_df[ssu_df[col].notna()][col])
        random_value = np.random.normal(mu, sigma, size=len(nan_indices))
        ssu_df[col].loc[nan_indices] = random_value
        if CELCIUS:
            ssu_df[col] = np.clip(ssu_df[col], -273.15, np.inf)
        return ssu_df
    
    @exec_col_func
    def _fill_random_gaussian_float(self, ssu_df, col, CELCIUS):
        """
        Description:
            정규 분포의 무작위값으로 float형 데이터 결측 처리.
            칼럼명 로그만 추가로 찍는 bypass 함수.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            CELCIUS: bool, 섭씨 데이터이면 True
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        return self.__fill_random_gaussian_float(ssu_df, col, CELCIUS)
    
    @exec_col_func
    def _fill_random_gaussian_float_group(self, ssu_df, col, CELCIUS, GROUPBY):
        """
        Description:
            정규 분포의 무작위값으로 float형 데이터 결측 처리.
            GROUP별로 별도로 묶어 계산하여 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            CELCIUS: bool, 섭씨 데이터이면 True
            GROUPBY: str, group할 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        ssu_dfs = [
            ssu_df[ssu_df[GROUPBY] == elm]
            for elm in ssu_df[GROUPBY].unique()
        ]
        ssu_dfs = [
            self.__fill_random_gaussian_float(elm_df, col, CELCIUS)
            for elm_df in ssu_dfs
        ]
        ssu_df = pd.concat(ssu_dfs).sort_index()
        return ssu_df
    
    def __fill_random_triangular_float(self, ssu_df, col, USE_ALL):
        """
        Description:
            삼각 분포의 무작위값으로 float형 데이터 결측 처리.
            min ~ median ~ max 로 삼각 분포 설정.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            USE_ALL: bool, 전체 데이터로 통계량 계산여부
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        nan_indices = ssu_df[ssu_df[col].isna()].index
        if USE_ALL:
            _min, _median, _max = np.nanquantile(ssu_df[col], q=[0, 0.5, 1])
        else:
            _min, _median, _max = np.nanquantile(ssu_df[col], q=[0.05, 0.5, 0.95])
        random_value = np.random.triangular(_min, _median, _max, size=len(nan_indices))
        ssu_df[col].loc[nan_indices] = random_value
        return ssu_df
    
    @exec_col_func
    def _fill_random_triangular_float(self, ssu_df, col, USE_ALL):
        """
        Description:
            삼각 분포의 무작위값으로 float형 데이터 결측 처리.
            칼럼명 로그만 추가로 찍는 bypass 함수.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            USE_ALL: bool, 전체 데이터로 통계량 계산여부
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        return self.__fill_random_triangular_float(ssu_df, col, USE_ALL)
    
    @exec_col_func
    def _fill_random_triangular_float_group(self, ssu_df, col, USE_ALL, GROUPBY):
        """
        Description:
            삼각 분포의 무작위값으로 float형 데이터 결측 처리.
            GROUP별로 별도로 묶어 계산하여 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            USE_ALL: bool, 전체 데이터로 통계량 계산여부
        Returns:
            ssu_df: dataframe, 처리된 데이터
            GROUPBY: str, group할 칼럼
        """
        ssu_dfs = [
            ssu_df[ssu_df[GROUPBY] == elm]
            for elm in ssu_df[GROUPBY].unique()
        ]
        ssu_dfs = [
            self.__fill_random_triangular_float(elm_df, col, USE_ALL)
            for elm_df in ssu_dfs
        ]
        ssu_df = pd.concat(ssu_dfs).sort_index()
        return ssu_df
    
    def __fill_random_pareto_float(self, ssu_df, col, PARETO_PARAM, FILL_MIN=0.0):
        """
        Description:
            파레토 분포의 무작위값으로 int형 데이터 결측 처리.
            파레토 지표는 2로 고정.
            파레토 분포의 무작위값에 IQR을 곱하여 최종 결측값 생성.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            PARETO_PARAM: int, 파레토 지표 (Pareto distribution shape parameter)
            FILL_MIN: float, 최소하한선
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        nan_indices = ssu_df[ssu_df[col].isna()].index
        IQR = np.subtract(*np.nanquantile(ssu_df[col], q=[0.75, 0.25]))
        if IQR < 0.5:
            IQR = 0.5
        random_value = np.random.pareto(PARETO_PARAM, size=len(nan_indices)) * IQR
        ssu_df[col].loc[nan_indices] = random_value + FILL_MIN
        return ssu_df
    
    @exec_col_func
    def _fill_random_pareto_float(self, ssu_df, col, PARETO_PARAM, FILL_MIN):
        """
        Description:
            파레토 분포의 무작위값으로 int형 데이터 결측 처리.
            칼럼명 로그만 추가로 찍는 bypass 함수.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            PARETO_PARAM: int, 파레토 지표 (Pareto distribution shape parameter)
            FILL_MIN: float, 최소하한선
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        return self.__fill_random_pareto_float(ssu_df, col, PARETO_PARAM, FILL_MIN)
    
    @exec_col_func
    def _fill_random_pareto_float_group(self, ssu_df, col, PARETO_PARAM, FILL_MIN, GROUPBY):
        """
        Description:
            파레토 분포의 무작위값으로 int형 데이터 결측 처리.
            칼럼명 로그만 추가로 찍는 bypass 함수.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            PARETO_PARAM: int, 파레토 지표 (Pareto distribution shape parameter)
            FILL_MIN: float, 최소하한선
            GROUPBY: str, group할 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        ssu_dfs = [
            ssu_df[ssu_df[GROUPBY] == elm]
            for elm in ssu_df[GROUPBY].unique()
        ]
        ssu_dfs = [
            self.__fill_random_pareto_float(elm_df, col, PARETO_PARAM, FILL_MIN)
            for elm_df in ssu_dfs
        ]
        ssu_df = pd.concat(ssu_dfs).sort_index()
        return ssu_df
    
    def __fill_random_choice_float(self, ssu_df, col):
        """
        Description:
            무작위값으로 float형 데이터 결측 처리.
            원래 데이터 비율 유지하며 추출.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        nan_indices = ssu_df[ssu_df[col].isna()].index
        col_data = ssu_df[col].value_counts() / ssu_df[col].notna().sum()
        random_value = np.random.choice(
            col_data.index.tolist(),
            size=len(nan_indices),
            p=col_data.tolist()
        )
        ssu_df[col].loc[nan_indices] = random_value
        return ssu_df
    
    @exec_col_func
    def _fill_random_choice_float(self, ssu_df, col):
        """
        Description:
            무작위값으로 float형 데이터 결측 처리.
            칼럼명 로그만 추가로 찍는 bypass 함수.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        return self.__fill_random_choice_float(ssu_df, col)
    
    @exec_col_func
    def _fill_random_choice_float_group(self, ssu_df, col, GROUPBY):
        """
        Description:
            무작위값으로 float형 데이터 결측 처리.
            GROUP별로 별도로 묶어 계산하여 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            GROUPBY: str, group할 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        ssu_dfs = [
            ssu_df[ssu_df[GROUPBY] == elm]
            for elm in ssu_df[GROUPBY].unique()
        ]
        ssu_dfs = [
            self.__fill_random_choice_float(elm_df, col)
            for elm_df in ssu_dfs
        ]
        ssu_df = pd.concat(ssu_dfs).sort_index()
        return ssu_df
    
    def __fill_random_chi_float(self, ssu_df, col, CHI_DF):
        """
        Description:
            카이제곱 분포의 무작위값으로 float형 데이터 결측 처리.
            자유도는 2로 고정.
            음수는 모두 0으로 clipping.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            CHI_DF: int, 자유도 (Degree of freedom)
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        nan_indices = ssu_df[ssu_df[col].isna()].index
        random_value = np.random.chisquare(df=CHI_DF, size=len(nan_indices))
        ssu_df[col].loc[nan_indices] = random_value
        ssu_df[col] = np.clip(ssu_df[col], 0, np.inf)
        return ssu_df
    
    @exec_col_func
    def _fill_random_chi_float(self, ssu_df, col, CHI_DF):
        """
        Description:
            카이제곱 분포의 무작위값으로 float형 데이터 결측 처리.
            칼럼명 로그만 추가로 찍는 bypass 함수.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            CHI_DF: int, 자유도 (Degree of freedom)
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        return self.__fill_random_chi_float(ssu_df, col, CHI_DF)
    
    @exec_col_func
    def _fill_random_chi_float_group(self, ssu_df, col, CHI_DF, GROUPBY):
        """
        Description:
            카이제곱 분포의 무작위값으로 float형 데이터 결측 처리.
            GROUP별로 별도로 묶어 계산하여 처리.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, float값 칼럼
            CHI_DF: int, 자유도 (Degree of freedom)
            GROUPBY: str, group할 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        ssu_dfs = [
            ssu_df[ssu_df[GROUPBY] == elm]
            for elm in ssu_df[GROUPBY].unique()
        ]
        ssu_dfs = [
            self.__fill_random_chi_float(elm_df, col, CHI_DF)
            for elm_df in ssu_dfs
        ]
        ssu_df = pd.concat(ssu_dfs).sort_index()
        return ssu_df
    
    @exec_col_func
    def fill_cols_latlon(self, ssu_df, latlon_cols):
        """
        Description:
            위도, 경도 결측 처리.
            도/특별시/광역시 기준의 값 중 무작위 선택.
        Args:
            ssu_df: dataframe, 전체 데이터
            latlon_cols: list, [지역, 위도, 경도]
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        district_latlon = self.load_district_latlon()
        nan_df = ssu_df[
            (ssu_df["위도"].isna()) |
            (ssu_df["경도"].isna())
        ][latlon_cols]
        _nan_func = lambda row: _apply_latlon_random(row, district_latlon)
        nan_df = nan_df.apply(_nan_func, axis=1)
        ssu_df = ssu_df.combine_first(nan_df)   # nan만 대체
        self.log.info("Fill lat/lon columns' missing value")
        return ssu_df
    
    def load_district_latlon(self):
        """
        Description:
            행정구역 위치 파일 열기.
                - 출처: 기상청_생활기상지수 조회서비스
                        https://www.data.go.kr/data/15085288/openapi.do
                - 행정구역 위치 정보(동네예보 단위) 확인 가능
        Args:
            None
        Returns:
            district_latlon: dataframe, 행정구역 위치 데이터
        """
        cols_type = self.load_cols_type(self.district.COL_TYPE)
        district_latlon = self.load_csv(
            self.district.FILEPATH,
            cols_type,
            self.district.ENCODING,
        )
        district_latlon = district_latlon[self.district.USE_COLS]
        _loc_col = self.district.USE_COLS[0]
        district_latlon[_loc_col] = district_latlon.apply(
            lambda row: _apply_acci_location(row, _loc_col), axis=1
        )
        district_latlon = district_latlon[district_latlon[_loc_col].notna()]
        return district_latlon.set_index(_loc_col)
    
    def lower_upper_bound(self, ssu_df, compare_cols):
        """
        Description:
            하한값/상한값을 맞게 설정.
            칼럼 순서는 하한, 상한 순서.
        Args:
            ssu_df: dataframe, 전체 데이터
            compare_cols: list, 하한, 상한 순서대로 정렬된 칼럼 리스트
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        for compare in compare_cols:
            ssu_df = self._compare_lower_upper_bound(ssu_df, compare)
        self.log.info("Adjust Lower/Upper bound values")
        return ssu_df
    
    @exec_col_func
    def _compare_lower_upper_bound(self, ssu_df, compare):
        """
        Description:
            하한값/상한값을 맞게 설정.
            칼럼 순서는 하한, 상한 순서.
        Args:
            ssu_df: dataframe, 전체 데이터
            compare: list, [하한 칼럼, 상한 칼럼]
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        return ssu_df.apply(lambda row: _apply_lower_upper_bound(row, compare), axis=1)
    
    @exec_col_func
    def make_target(self, ssu_df, target_cols, target_bins):
        """
        Description:
            target 생성.
            임의 규칙에 의거하여 위험도 산출 후 범주로 분리.
                - 사망자, 부상자 칼럼 이용
                - 가중 평균값에 random_poisson 값을 더함
                - random_poisson 의 lambda는 각각 칼럼의 nonzero 평균의 가중 평균
            산출한 위험도를 "1" ~ "5" 로 범주화 (이상~미만 범위).
            target 생성에 사용한 칼럼은 제외.
        Args:
            ssu_df: dataframe, 전체 데이터
            target_cols: dict, {target칼럼: arguments}
            target_bins: list, 위험도 범주화 범위 bins
        Returns:
            ssu_df: dataframe, 처리된 데이터
            target_dict: dict, target에 사용한 칼럼, lambda 값
        """
        tgt_cols = list(target_cols.keys())
        weights = self._make_target_weights(target_cols)
        target_ary = ssu_df[tgt_cols].to_numpy()
        
        # target_1: 가중 평균
        target_1 = np.dot(target_ary, weights)
        # target_2: nonzero_mean을 lambda로 사용한 random poisson 값
        nonzero_mean = [
            target_ary[:, i][np.nonzero(target_ary[:, i])].mean()
            for i in range(len(target_cols))
        ]
        nonzero_mean = np.array(nonzero_mean)
        poisson_lambda = np.dot(nonzero_mean, weights)
        target_2 = np.random.poisson(poisson_lambda, size=len(ssu_df))
        
        # 위험도 산출 후 "1" ~ "5"로 범주화
        ssu_df[self.tgt_col] = target_1 + target_2
        ssu_df[self.tgt_col] = np.digitize(ssu_df[self.tgt_col], bins=target_bins) + 1
        ssu_df[self.tgt_col] = ssu_df[self.tgt_col].astype(str)
        ssu_df = ssu_df.drop(columns=target_cols)
        
        # 저장할 target에 이용 columns, lambda 값
        target_dict = {
            self.tgt_col: {
                "cols": tgt_cols,
                "lambda": poisson_lambda
            }
        }
        self.log.info("Make target col & Drop target's material")
        
        return ssu_df, target_dict
    
    def _make_target_weights(self, target_cols):
        """
        Description:
            target 가중치 생성.
            임의 규칙에 의거하여 위험도 산출에 사용할 weights 처리.
        Args:
            target_cols: dict, {target칼럼: arguments}
        Returns:
            target_weights: numpy array, target 가중치
        """
        weights = []
        for _, args in target_cols.items():
            weights.append(args["args"]["WEIGHT"])
        return np.array(weights)
    
    @exec_col_func
    def make_acci_ymdh24mi(self, ssu_df, api_time_col):
        """
        Description:
            기상 데이터 수집에 이용할 사고 연월일시분 생성.
            결측값은 사고계절, 사고시간대 결측처리 결과를 이용하여 생성.
        Args:
            ssu_df: dataframe, 전체 데이터
            api_time_col: str, API에서 사용할 사고시각 칼럼명
        Returns:
            ssu_df: dataframe, 사고_연월일시분 계산된 데이터
        """
        # 사고일자: 사고계절로 결측처리
        ssu_df["사고일자"] = ssu_df.apply(
            lambda row: _apply_acci_date(row, "사고일자", "사고계절"),
            axis=1
        )
        # 사고일자 중 6자리는 8자리로 변환 (일자 추가)
        ssu_df["사고일자"] = ssu_df.apply(
            lambda row: _apply_date_random(row, "사고일자"),
            axis=1
        )
        # 사고 시간: 사고시간대로 결측처리
        ssu_df["사고시간"] = ssu_df.apply(
            lambda row: _apply_acci_time(row, "사고시간", "사고시간대", self.time_ranges),
            axis=1
        )
        # 연월일시분_KST: 사고일자+사고시간
        ssu_df[api_time_col] = ssu_df.apply(
            lambda row: row["사고일자"]+row["사고시간"],
            axis=1
        )
        return ssu_df
    
    def get_weather_data(self, ssu_df, time_col, lat_col, lon_col):
        """
        Description:
            API를 이용하여 기상데이터 수집.
            수집항목은 cfg.API.KMA.USE_COLS 에 활성화된 것.
        Args:
            ssu_df: dataframe, 전체 데이터
            time_col: str, 사고시각 칼럼명
            lat_col: str, 위도 칼럼명
            lon_col: str, 경도 칼럼명
        Returns:
            wth_df: dataframe, 기상 데이터
        """
        wth_df = self.wth(ssu_df, time_col, lat_col, lon_col)
        return wth_df
    
    def merge_acci_wth(self, ssu_df, wth_df, drop_cols):
        """
        Description:
            사고 데이터와 기상 데이터 병합.
            학습에 불필요 칼럼은 제외.
        Args:
            ssu_df: dataframe, 전체 데이터
            wth_df: dataframe, 기상 데이터
            drop_cols: list, 삭제할 칼럼
        Returns:
            ssu_df: dataframe, 병합 데이터
        """
        ssu_df = pd.merge(
            ssu_df.reset_index(drop=False),
            wth_df.reset_index(drop=False),
            left_on=["index", "사고_연월일시분"],
            right_on=["index", "관측시각_KST"],
            how="inner"
        )
        # 불필요칼럼 삭제
        drop_cols.append("index")
        ssu_df = ssu_df.drop(columns=drop_cols)
        return ssu_df
    
    def make_train_test_df(self,
                           ssu_df,
                           train_indices,
                           test_indices):
        """
        Description:
            학습/테스트 데이터 분리.
        Args:
            ssu_df: dataframe, 전체 데이터
            train_indices: numpy array, 학습에 사용할 index
            test_indices: numpy array, 테스트에 사용할 index
        Returns:
            train_df: dataframe, 학습 데이터
            test_df: dataframe, 테스트 데이터
        """
        train_df = ssu_df.loc[train_indices]
        test_df = ssu_df.loc[test_indices].reset_index(drop=True)
        self.log.info("Original Train / Test data: {0} / {1}".format(len(train_df), len(test_df)))
        return train_df, test_df
    
    def augment_data(self,
                     train_df,
                     ADD_RANDOM,
                     REPEAT_NUM,
                     POISSON_LAMBDA=2,
                     MAX_NANNUM_RATIO=0.1):
        """
        Description:
            데이터 증식.
            임의 규칙에 의거하여 학습 데이터만 증식.
                - SMOTE를 이용하여 불균형 label 데이터 증식
                - train_df에서 복원추출로 SMOTE 데이터의 REPEAT_NUM 배수만큼 인덱스 추출
                - 각각 복제한 데이터에서 일부 칼럼 데이터를 nan으로 변경
                    - input feature 개수의 MAX_NANNUM_RATIO 정도 값으로 random poisson
                    - 데이터별 nan 개수는 input feature 개수의 MAX_NANNUM_RATIO 정도 값으로 clipping
        Args:
            train_df: dataframe, 학습 데이터
            ADD_RANDOM: bool, SMOTE 후 추가 증식시킬지 여부
            REPEAT_NUM: int, SMOTE 후 추가 증식시킬 배수
            POISSON_LAMBDA: int, random poisson에 적용할 값 (default: 2)
            MAX_NANNUM_RATIO: float, row에서 칼럼에 nan으로 바꿀 최대 개수 비율
        Returns:
            train_df: dataframe, 처리된 학습 데이터
        """
        # SMOTE
        input_cols = [col for col in train_df.columns if not col == self.tgt_col]
        smotenc = SMOTENC(
            categorical_features=list(self.encode_ctgy_cols.keys()),
            sampling_strategy="all",
            random_state=self.random_seed,
            n_jobs=-1
        )
        X_smote, Y_smote = smotenc.fit_resample(train_df[input_cols], train_df[[self.tgt_col]])
        
        # Additional Random Augmentation
        if ADD_RANDOM:
            AUGMT_SIZE = len(X_smote) * REPEAT_NUM
            MAX_NANNUM = int(np.floor(len(input_cols) * MAX_NANNUM_RATIO))
            base_indices = np.random.choice(X_smote.index, size=AUGMT_SIZE)
            nannums_indices = np.random.poisson(POISSON_LAMBDA, size=AUGMT_SIZE) + 1
            nannums_indices = np.clip(nannums_indices, 0, MAX_NANNUM)
            
            nannum_col = "nannums"
            X_augmt = X_smote.loc[base_indices]
            Y_augmt = Y_smote.loc[base_indices]
            X_augmt[nannum_col] = nannums_indices
            X_augmt = X_augmt.apply(lambda row: _apply_random_nan(row, input_cols, nannum_col), axis=1)
            
            # Concatenate SMOTE & RandomChoice
            X_augmt = X_augmt.drop(columns=nannum_col)
            X_augmt = pd.concat([X_smote, X_augmt])
            Y_augmt = pd.concat([Y_smote, Y_augmt])
            self.log.info("Data augmentation: Using SMOTE + Additional Random Augmentation")
        else:
            X_augmt, Y_augmt = X_smote, Y_smote
            self.log.info("Data augmentation: Using SMOTE")
        X_augmt = X_augmt.reset_index(drop=True)
        Y_augmt = Y_augmt.reset_index(drop=True)
        train_df = pd.concat([X_augmt, Y_augmt], axis=1)
        self.log.info("Augmented Train data: {0}".format(len(train_df)))
        
        return train_df
    
    def industry_midcode_list(self):
        """
        Description:
            고용업종코드_중분류 인코딩에 사용할 전체 코드 리스트 생성.
            고용업종코드 파일 불러와서 처리.
        Args:
            target_cols: dict, {target칼럼: arguments}
        Returns:
            target_weights: numpy array, target 가중치
        """
        code_df = self.load_csv(self.emp.FILEPATH, self.emp.DTYPE, self.emp.ENCODING)
        codes = code_df[self.emp.CODE_COL]
        codes = codes[codes.notna()].unique()
        self.encode_ctgy_cols["고용업종코드_중분류"]["args"]["CTGY_LIST"] = list(codes)
        self.log.info("Load total industry midcode")
    
    @exec_col_func
    def encode_cols_bool(self, ssu_df, bool_cols):
        """
        Description:
            bool형 데이터 인코딩.
        Args:
            ssu_df: dataframe, 전체 데이터
            bool_cols: list, bool형 칼럼
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        ssu_df[bool_cols] = ssu_df[bool_cols].astype(np.float32)
        self.log.info("Encode boolean columns")
        return ssu_df
    
    def encode_cols_ctgy(self, ssu_df, ctgy_cols, encoder_dict):
        """
        Description:
            범주형 데이터 인코딩.
        Args:
            ssu_df: dataframe, 전체 데이터
            ctgy_cols: dict, {범주형 칼럼: 범주 데이터}
            encoder_dict: dict, {method: encoder}
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        # 칼럼, arguments 분리
        encode_methods_ctgy = self._make_methods(ctgy_cols)
        ord_cols = list(self._extract_methods(encode_methods_ctgy, "ordinal").keys())
        cb_cols = list(self._extract_methods(encode_methods_ctgy, "catboost").keys())
        bn_cols = list(self._extract_methods(encode_methods_ctgy, "binary").keys())
        smiles_col = list(self._extract_methods(encode_methods_ctgy, "smiles").keys())[0]
        # 인코딩
        ssu_df = self._encoding_ord(ssu_df, ord_cols, encoder_dict["ordinal"])
        ssu_df = self._encoding_cb(ssu_df, cb_cols, encoder_dict["catboost"])
        ssu_df = self._encode_bn(ssu_df, bn_cols, encoder_dict["binary"])
        ssu_df = self._encode_smiles(ssu_df, smiles_col, encoder_dict["smiles"])
        self.log.info("Encode category columns")
        return ssu_df
    
    def _encoding_ord(self, ssu_df, ord_cols, ord_encoder):
        """
        Description:
            범주형 데이터 Ordinal 인코딩.
        Args:
            ssu_df: dataframe, 전체 데이터
            ord_cols: list, 칼럼명
            ord_encoder: OrdinalEncoder, Ordinal 인코더
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        if len(ord_cols) > 0:
            ssu_df[ord_cols] = ord_encoder.transform(ssu_df[ord_cols])
        return ssu_df
    
    def _encoding_cb(self, ssu_df, cb_cols, cb_encoder):
        """
        Description:
            범주형 데이터 CatBoost 인코딩.
        Args:
            ssu_df: dataframe, 전체 데이터
            ord_cols: list, 칼럼명
            ord_encoder: CatBoostEncoder, CatBoost 인코더
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        if len(cb_cols) > 0:
            ssu_df[cb_cols] = cb_encoder.transform(ssu_df[cb_cols])
        return ssu_df
    
    def _encode_bn(self, ssu_df, bn_cols, bn_encoder):
        """
        Description:
            범주형 데이터 Binary 인코딩.
            인코딩 전 칼럼 제거, 인코딩 후 칼럼 병합.
        Args:
            ssu_df: dataframe, 전체 데이터
            bn_cols: list, 칼럼명
            bn_encoder: BinaryEncoder, Binary 인코더
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        if len(bn_cols) > 0:
            bn_encode_df = bn_encoder.transform(ssu_df[bn_cols])
            ssu_df = ssu_df.drop(columns=bn_cols)
            ssu_df = pd.merge(ssu_df, bn_encode_df, left_index=True, right_index=True)
        return ssu_df
    
    def _encode_smiles(self, ssu_df, col, featurizer):
        """
        Description:
            범주형 데이터 SMILES 인코딩.
            deepchem 라이브러리에 있는 featurizer 이용.
        Args:
            ssu_df: dataframe, 전체 데이터
            col: str, 칼럼명
            featurizer: featurizers, SMILES 또는 mol 데이터 이용한 모델 input 변환기
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        total_smiles_dict = self.c2s(list(ssu_df[col].unique()))
        # 임시 인코딩 df 만든 뒤, 원래 df와 병합
        tmp_col = "smiles_feat"
        total_smiles_df = pd.DataFrame({
            col: total_smiles_dict.keys(),
            # tmp_col: featurizer.featurize(total_smiles_dict.values())   # featurize
            tmp_col: total_smiles_dict.values()   # smiles
        })
        ssu_df = pd.merge(ssu_df, total_smiles_df, on=col, how="left")
        ssu_df = ssu_df.drop(columns=col)
        ssu_df = ssu_df.rename(columns={tmp_col: col})
        return ssu_df
    
    def make_encoder(self, ssu_df, ctgy_cols):
        """
        Description:
            범주형 데이터 인코더 생성.
        Args:
            ssu_df: dataframe, 전체 데이터
            ctgy_cols: dict, {범주형 칼럼: 범주 데이터}
        Returns:
            encoder_dict: dict, {method: encoder}
        """
        # 칼럼, arguments 분리
        encode_methods_ctgy = self._make_methods(ctgy_cols)
        ordinal_dict = self._extract_methods(encode_methods_ctgy, "ordinal")
        catboost_dict = self._extract_methods(encode_methods_ctgy, "catboost")
        binary_dict = self._extract_methods(encode_methods_ctgy, "binary")
        smiles_dict = self._extract_methods(encode_methods_ctgy, "smiles")
        # Method 별 적용
        ord_encoder = self._encode_ordinal_ctgy(ssu_df, ordinal_dict)
        cb_encoder = self._encode_catboost_ctgy(ssu_df, catboost_dict)
        bn_encoder = self._encode_binary_ctgy(None, binary_dict)
        featurizer = self._encode_smiles_ctgy(None, smiles_dict)
        
        # Encoder 반환
        encoder_dict = {
            "ordinal": ord_encoder,
            "catboost": cb_encoder,
            "binary": bn_encoder,
            "smiles": featurizer
        }
        self.log.info("Make category columns encoder")
        return encoder_dict
    
    @exec_col_func
    def _encode_ordinal_ctgy(self, ssu_df, ordinal_dict):
        """
        Description:
            범주형 데이터 Ordinal 인코더 생성.
        Args:
            ssu_df: dataframe, 전체 데이터
            ordinal_dict: dict, {칼럼명: arguments}
        Returns:
            ord_encoder: OrdinalEncoder, Ordinal 인코더
        """
        ord_cols = list(ordinal_dict.keys())
        if len(ord_cols) > 0:
            # mapping 생성
            mappings = []
            for col, args in ordinal_dict.items():
                ord_mapping = self._make_ordinal_mapping(col, **args)
                mappings.append(ord_mapping)
            # 인코더 생성
            ord_encoder = OrdinalEncoder(mapping=mappings)
            ord_encoder.fit(ssu_df[ord_cols])
            return ord_encoder
        else:
            return None
    
    def _make_ordinal_mapping(self, col, CTGY_LIST=None):
        """
        Description:
            범주형 데이터 Ordinal 인코딩에 사용되는 mapping 생성.
            설정 파일에 있는 순서대로 적용.
            None은 0, NaN은 -2 로 추가 설정.
        Args:
            col: str, 범주형 칼럼
            CTGY_LIST: list, 범주값 리스트 (default: None)
        Returns:
            ord_mapping: dict, {"col": 칼럼명, "mapping": 매핑 데이터}
        """
        ord_mapping = dict()
        mapping = {ctgy: i+1 for i, ctgy in enumerate(CTGY_LIST)}
        mapping[None] = self.encode_none
        mapping[np.nan] = self.encode_nan
        ord_mapping["col"] = col
        ord_mapping["mapping"] = mapping
        return ord_mapping
    
    @exec_col_func
    def _encode_catboost_ctgy(self, ssu_df, catboost_dict):
        """
        Description:
            범주형 데이터 CatBoost 인코더 생성.
            인코딩 방식 설명
                - (TargetSum + prior) / (FeatureCount + 1)
                - TargetSum: 해당 범주에 해당하는 target 값의 합
                - prior: 상수값, (AllTargetSum / AlldataNum)
                - FeatureCount: 해당 범주에 해당하는 데이터 수
        Args:
            ssu_df: dataframe, 전체 데이터
            catboost_dict: dict, {칼럼명: arguments}
        Returns:
            cb_encoder: CatBoostEncoder, CatBoost 인코더
        """
        cb_cols = list(catboost_dict.keys())
        if len(cb_cols) > 0:
            cb_encoder = CatBoostEncoder(
                cols=cb_cols,
                random_state=self.random_seed
            )
            cb_encoder.fit(ssu_df[cb_cols], ssu_df[self.tgt_col].astype(int))
            return cb_encoder
        else:
            return None
    
    @exec_col_func
    def _encode_binary_ctgy(self, EMPTY_ARG, binary_dict):
        """
        Description:
            범주형 데이터 Binary 인코더 생성.
            전체 범주 리스트 중 실데이터에 없는 것도 처리.
            길이 다른 리스트 인코딩하기 위해 임시 dataframe 이용.
            해당 칼럼명은 _0, _1 등이 붙게 되고, 개수도 변경됨.
        Args:
            EMPTY_ARG: 데코레이터 쓰기 위해 비우는 값 (미사용)
            binary_dict: dict, {칼럼명: arguments}
        Returns:
            bn_encoder: BinaryEncoder, Binary 인코더
        """
        bn_cols = list(binary_dict.keys())
        if len(bn_cols) > 0:
            # 전체 범주 포함하는 임시 dataframe 생성
            bn_ctgys = [self._make_bn_ctgys(**args) for args in binary_dict.values()]
            bn_ctgys_zip = list(zip_longest(*bn_ctgys))
            bn_df = pd.DataFrame(bn_ctgys_zip, columns=bn_cols).fillna(method="ffill")
            # 인코딩
            bn_encoder = BinaryEncoder(cols=bn_cols)
            bn_encoder.fit(bn_df[bn_cols])
            return bn_encoder
        else:
            return None
    
    def _make_bn_ctgys(self, CTGY_LIST):
        """
        Description:
            범주형 데이터 Binary 인코딩의 범주 리스트 반환.
            pass through 로직.
        Args:
            CTGY_LIST: list, 범주값 리스트
        Returns:
            CTGY_LIST: list, 범주값 리스트
        """
        return CTGY_LIST
    
    @exec_col_func
    def _encode_smiles_ctgy(self, EMPTY_ARG, smiles_dict):
        """
        Description:
            범주형 데이터 SMILES 인코딩.
            deepchem 라이브러리에 있는 featurizer 이용.
                - MolGraphConvFeaturizer
                    - 다양한 네트워크에서 쓰지만 데이터 변환 못하는 경우 많음
                    - GAT, GCN, MPNN, InfoGraph 등 적용 가능
                - ConvMolFeaturizer
                    - GraphConv, DAG 등 적용 가능
                - MATFeaturizer
                    - MAT에 적용 가능
        Args:
            EMPTY_ARG: 데코레이터 쓰기 위해 비우는 값 (미사용)
            smiles_dict: dict, {칼럼명: arguments}
        Returns:
            featurizer: featurizers, SMILES 또는 mol 데이터 이용한 모델 input 변환기
        """
        # Just one column
        col = list(smiles_dict.keys())[0]
        args = smiles_dict[col]
        featurizer = self._make_smiles_featurizer(**args)
        return featurizer
    
    def _make_smiles_featurizer(self, KIND):
        """
        Description:
            SMILES 데이터 인코딩에 사용되는 mapping 생성.
            설정 파일에 있는 순서대로 적용.
        Args:
            KIND: str, 적용할 featurizers 종류
                       MolGraphConv / ConvMol / MAT 만 가능
        Returns:
            featurizer: featurizers, SMILES 또는 mol 데이터 이용한 모델 input 변환기
        """
        from deepchem.feat import MolGraphConvFeaturizer, ConvMolFeaturizer
        from src_m2.network_mat import MATFeaturizerOrigin
        
        if KIND == "MolGraphConv":
            featurizer = MolGraphConvFeaturizer(
                use_edges=True,
                use_chirality=True,
                use_partial_charge=True
            )
        elif KIND == "ConvMol":
            featurizer = ConvMolFeaturizer(per_atom_fragmentation=True)
        elif KIND == "MAT":
            featurizer = MATFeaturizerOrigin()
        else:
            featurizer = None
            self.log.info("Write correct featurizer kind: MolGraphConv / ConvMol / MAT, not using {0}".format(KIND))
            self.error.error("Write correct featurizer kind: MolGraphConv / ConvMol / MAT, not using {0}".format(KIND))
        
        return featurizer
    
    def make_scaler(self, ssu_df, cols_dict):
        """
        Description:
            데이터 스케일러 생성.
        Args:
            ssu_df: dataframe, 전체 데이터
            cols_dict: dict, {칼럼명: 데이터 처리 방법}
        Returns:
            ssu_df: dataframe, 처리된 데이터
            scaler_dict: dict, {method: scaler}
        """
        # 칼럼, arguments 분리
        scale_methods = self._make_methods(cols_dict)
        minmax_dict = self._extract_methods(scale_methods, "minmax")
        robust_dict = self._extract_methods(scale_methods, "robust")
        standard_dict = self._extract_methods(scale_methods, "standard")
        quantile_dict = self._extract_methods(scale_methods, "quantile")
        power_dict = self._extract_methods(scale_methods, "power")
        minusone_dict = self._extract_methods(scale_methods, "minusone")
        # Method 별 적용
        minmax_scaler = self._scale_minmax(ssu_df, minmax_dict)
        robust_scaler = self._scale_robust(ssu_df, robust_dict)
        standard_scaler = self._scale_standard(ssu_df, standard_dict)
        quantile_scaler = self._scale_quantile(ssu_df, quantile_dict)
        power_scaler = self._scale_power(ssu_df, power_dict)
        minusone_scaler = self._scale_minusone(ssu_df, minusone_dict)
        
        # Scaler 반환
        scaler_dict = {
            **minmax_scaler,
            **robust_scaler,
            **standard_scaler,
            **quantile_scaler,
            **power_scaler,
            **minusone_scaler,
        }
        self.log.info("Make scaler")
        return scaler_dict
    
    @exec_col_func
    def _scale_minmax(self, ssu_df, minmax_dict):
        """
        Description:
            MinMax Scaling.
        Args:
            ssu_df: dataframe, 전체 데이터
            minmax_dict: dict, {칼럼명: arguments}
        Returns:
            minmax: dict, {칼럼명: MinMax 스케일러}
        """
        minmax = dict()
        for col in minmax_dict.keys():
            scaler = MinMaxScaler()
            scaler.fit(ssu_df[[col]])
            minmax[col] = scaler
        return minmax
    
    @exec_col_func
    def _scale_robust(self, ssu_df, robust_dict):
        """
        Description:
            Robust Scaling.
            args인 quantile_range 는 config에서 설정.
        Args:
            ssu_df: dataframe, 전체 데이터
            robust_dict: dict, {칼럼명: arguments}
        Returns:
            robust: dict, {칼럼명: Robust 스케일러}
        """
        def _return_scaler(quantile_range):
            return RobustScaler(quantile_range=tuple(quantile_range))
        
        robust = dict()
        for col, args in robust_dict.items():
            scaler = _return_scaler(**args)
            scaler.fit(ssu_df[[col]])
            robust[col] = scaler
        return robust
    
    @exec_col_func
    def _scale_standard(self, ssu_df, standard_dict):
        """
        Description:
            Standard Scaling.
        Args:
            ssu_df: dataframe, 전체 데이터
            standard_dict: dict, {칼럼명: arguments}
        Returns:
            standard: dict, {칼럼명: Standard 스케일러}
        """
        standard = dict()
        for col in standard_dict.keys():
            scaler = StandardScaler()
            scaler.fit(ssu_df[[col]])
            standard[col] = scaler
        return standard
    
    @exec_col_func
    def _scale_quantile(self, ssu_df, quantile_dict):
        """
        Description:
            QuantileTransformer 이용한 Scaling.
            args인 n_quantiles, output_distribution는 config에서 설정.
                - output_distribution: uniform, normal
        Args:
            ssu_df: dataframe, 전체 데이터
            quantile_dict: dict, {칼럼명: arguments}
        Returns:
            quantile: dict, {칼럼명: Quantile 트랜스포머}
        """
        def _return_scaler(n_quantiles, output_distribution):
            return QuantileTransformer(
                n_quantiles=n_quantiles,
                output_distribution=output_distribution
            )
        
        quantile = dict()
        for col, args in quantile_dict.items():
            scaler = _return_scaler(**args)
            scaler.fit(ssu_df[[col]])
            quantile[col] = scaler
        return quantile

    
    @exec_col_func
    def _scale_power(self, ssu_df, power_dict):
        """
        Description:
            PowerTransformer 이용한 Scaling.
            args인 method는 config에서 설정.
                - method: yeo-johnson (음양수), box-cox (양수만)
        Args:
            ssu_df: dataframe, 전체 데이터
            power_dict: dict, {칼럼명: arguments}
        Returns:
            power: dict, {칼럼명: Power 트랜스포머}
        """
        def _return_scaler(method):
            return PowerTransformer(method=method)
        
        power = dict()
        for col, args in power_dict.items():
            scaler = _return_scaler(**args)
            scaler.fit(ssu_df[[col]])
            power[col] = scaler
        return power
    
    @exec_col_func
    def _scale_minusone(self, ssu_df, minusone_dict):
        """
        Description:
            (0, 1) 범위의 데이터를 (-1, 1)로 변환.
            MinMax Scaling 이용.
        Args:
            ssu_df: dataframe, 전체 데이터
            minusone_dict: dict, {칼럼명: arguments}
        Returns:
            minusone: dict, {칼럼명: MinMax 스케일러}
        """
        minusone = dict()
        use_cols = list()
        for col in minusone_dict.keys():
            for df_col in ssu_df.columns:
                if df_col.startswith(col):
                    use_cols.append(df_col)
        for col in use_cols:
            scaler = MinMaxScaler(feature_range=(-1, 1))
            scaler.fit(ssu_df[[col]])
            minusone[col] = scaler
        return minusone
    
    def scale_cols(self, ssu_df, scaler_dict):
        """
        Description:
            데이터 스케일링.
        Args:
            ssu_df: dataframe, 전체 데이터
            scaler_dict: dict, {method: scaler}
        Returns:
            ssu_df: dataframe, 처리된 데이터
        """
        for scaled_col, scaler in scaler_dict.items():
            ssu_df[scaled_col] = scaler.transform(ssu_df[[scaled_col]])
        return ssu_df
    
    def save_encoder_dict(self, encoder_dict, target_dict, save_path):
        """
        Description:
            인코더, target 정보 pickle 파일로 저장.
        Args:
            encoder_dict: dict, {인코딩방법: 인코더}
            target_dict: dict, target에 사용한 칼럼, lambda 값
        Returns:
            saved_encoder_dict: dict, 인코더 및 target 정보
        """
        saved_encoder_dict = {**encoder_dict, **target_dict}
        try:
            if self.encoder_save:
                joblib.dump(saved_encoder_dict, save_path)
        except Exception as e:
            self.log.info("Save Encoder dictionary incompletely")
            self.err_log.error("Save Encoder dictionary incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save Encoder dictionary completely")
        finally:
            return saved_encoder_dict
    
    def save_scaler_dict(self, scaler_dict, save_path):
        """
        Description:
            스케일러 정보 pickle 파일로 저장.
        Args:
            scaler_dict: dict, {스케일링방법: 스케일러}
        Returns:
            None
        """
        try:
            if self.scaler_save:
                joblib.dump(scaler_dict, save_path)
        except Exception as e:
            self.log.info("Save Scaler dictionary incompletely")
            self.err_log.error("Save Scaler dictionary incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save Scaler dictionary completely")


class Cas2Smiles(DataframeUtils):
    """CAS번호로부터 SMILES 생성."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            CAS번호로부터 SMILES 생성.
            API로 받아올 수 없는 경우는 수동으로 추가 필요.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(Cas2Smiles, self).__init__(log, err_log)
        cfg = cfg.PREPROCESS.CAS2SMILES
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.api_rul = cfg.API_URL
        self.manual_data_path = cfg.MANUAL_DATA_PATH
        self.save_filepath = cfg.SAVE_FILEPATH
        self.dtype = cfg.DTYPE
        self.data_encoding = cfg.DATA_ENCODING
        self.CAS_COL = "cas"
        self.SMILES_COL = "smiles"
    
    def __call__(self, cas_list=[]):
        """
        Description:
            CAS -> SMILES 결과 딕셔너리 생성.
        Args:
            cas_list: list, Unique한 CAS번호 리스트 (default: [])
        Returns:
            total_smiles_dict: dict, {CAS번호: SMILES}
        """
        if os.path.exists(self.save_filepath):
            total_smiles_dict = self.load_total()
        else:
            total_smiles_dict = dict()
        
        manual = self.load_manual()
        cas_list = self.filter_cas_list(cas_list, total_smiles_dict, manual)
        used_api, no_smiles_cas = self.webapi_cas2smiles(cas_list)
        
        total_smiles_dict, no_cano_cas = self.make_total(used_api, manual, total_smiles_dict)
        no_smiles_cas.extend(no_cano_cas)
        self.save_total(total_smiles_dict)
        
        if self.check_cas(cas_list, list(total_smiles_dict.keys())):
            return total_smiles_dict
        else:
            self.log.info("Make sure nothing is missing on manual file: {0}".format(no_smiles_cas))
            self.err_log.error("Make sure nothing is missing on manual file: {0}".format(no_smiles_cas))
    
    def load_manual(self):
        """
        Description:
            API로 받아올 수 없는 수동으로 수집한 CAS -> SMILES.
        Args:
            None
        Returns:
            manual: dict, {CAS번호: SMILES}
        """
        import yaml
        with open(self.manual_data_path, "r") as f:
            manual = yaml.full_load(f)
        return list(manual.values())[0]
    
    def filter_cas_list(self, cas_list, total_smiles, manual):
        """
        Description:
            API로 받아올 CAS -> SMILES 의 CAS 리스트 반환.
        Args:
            cas_list: list, Unique한 CAS번호 리스트
            total_smiles: dict, {CAS번호: SMILES} 기존 저장됐던 데이터
            manual: dict, {CAS번호: SMILES} 수동 입력 데이터
        Returns:
            cas_list: list, 정제된 Unique한 CAS번호 리스트
        """
        _cas_list = np.array(cas_list)
        _total = np.array(list(total_smiles.keys()))
        _manual = np.array(list(manual.keys()))
        _cas_list = np.setdiff1d(_cas_list, _total)
        _cas_list = np.setdiff1d(_cas_list, _manual)
        return list(_cas_list)
    
    def webapi_cas2smiles(self, cas_list):
        """
        Description:
            API로 수집한 CAS -> SMILES.
        Args:
            cas_list:  list, CAS번호 리스트
        Returns:
            used_api: dict, {CAS번호: SMILES}
            no_smiles_cas: list, API로 확인되지 않은 CAS번호 리스트
        """
        from urllib.request import urlopen
        used_api = dict()
        no_smiles_cas = list()
        for cas in cas_list:
            try:
                smiles = urlopen(self.api_rul.format(cas)).read().decode("utf-8")
                used_api[cas] = smiles
            except Exception as e:
                print("{0} ==> {1}".format(e, cas))
                no_smiles_cas.append(cas)
        self.log.info("Data collection on Web API / Cannot collected nums: {0}".format(len(no_smiles_cas)))
        return used_api, no_smiles_cas
    
    def make_total(self, used_api, manual, total_smiles_dict):
        """
        Description:
            전체 CAS -> SMILES 결과 통합.
            Canonical SMILES로 변환.
        Args:
            used_api: dict, {CAS번호: SMILES}
            manual: dict, {CAS번호: SMILES}
            total_smiles_dict: {CAS번호: SMILES} 이전 total
        Returns:
            total_canosmiles_dict: dict, {CAS번호: SMILES} 처리 후 total
            no_cano_cas: list, Canonical SMILES 안 되는 CAS번호
        """
        from rdkit import Chem
        total_smiles_dict = {**used_api, **manual, **total_smiles_dict}
        total_canosmiles_dict = dict()
        no_cano_cas = list()
        for cas, smiles in total_smiles_dict.items():
            try:
                total_canosmiles_dict[cas] = Chem.MolToSmiles(Chem.MolFromSmiles(smiles), canonical=True)
            except Exception as e:
                self.log.info("Error {0}: {1}".format(cas, e))
                no_cano_cas.append(cas)
        return total_canosmiles_dict, no_cano_cas
    
    def save_total(self, total_smiles_dict):
        """
        Description:
            전체 CAS -> SMILES 결과 저장.
        Args:
            total_smiles_dict: dict, {CAS번호: SMILES}
        Returns:
            None
        """
        total_df = pd.DataFrame(
            total_smiles_dict.items(),
            columns=[self.CAS_COL, self.SMILES_COL]
        )
        self.save_csv(
            total_df,
            self.save_filepath,
            encoding=self.data_encoding
        )
    
    def load_total(self):
        """
        Description:
            전체 CAS -> SMILES 결과 불러오기.
        Args:
            None
        Returns:
            total_smiles_dict: dict, {CAS번호: SMILES}
        """
        total = self.load_csv(self.save_filepath, self.dtype, encoding=self.data_encoding)
        return total.set_index(self.CAS_COL).to_dict()[self.SMILES_COL]
    
    def check_cas(self, datas_cas, smiles_cas):
        """
        Description:
            SMILES 변환 딕셔너리 CAS에 파일 CAS가 모두 포함되는지 확인.
        Args:
            datas_cas: list, 파일 CAS
            smiles_cas: list, SMILES 변환 딕셔너리 CAS
        Returns:
            CHECK_CAS: bool, 모두 포함되면 True, 아니면 False
        """
        _data_cas = np.array(datas_cas)
        _smiles_cas = np.array(smiles_cas)
        return np.all(np.isin(_data_cas, _smiles_cas) == True)
