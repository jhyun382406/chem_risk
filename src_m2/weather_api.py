"""
기상청 API 사용.
    - 융합기상 - 고해상도 격자자료 API.
        - (1997. 1. 1. ~ 2022. 12. 31.) 한시간 간격 (매시 00분)
        - (2023. 1. 1. ~) 매시 00분부터 5분 간격
API key를 RSA 암호화하여 이용.
"""
import time
from datetime import datetime, timedelta
import requests
import numpy as np
import pandas as pd

from utils.log_module import print_elapsed_time
from utils.code_utils import unpack
from utils.df_utils import DataframeUtils
from src.apply_func import _apply_weighted_sum
from kma_api.rsa_apikey import RSA


class WeatherAPI(DataframeUtils):
    """기상 데이터 API."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            화학사고 input에 추가할 기상 data를
            기상청 API를 이용하여 가져오기.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(WeatherAPI, self).__init__(log, err_log)
        cfg_kma = cfg.API.KMA
        self.CVT_TIME_FORMAT = "%Y%m%d%H%M"
        self.PRINT_INTERVAL = 10
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.rsa = RSA(cfg, log)
        self.file_tail = cfg_kma.FILE_TAIL
        self.API_URL = cfg_kma.API_URL
        self.use_cols = unpack(cfg_kma.USE_COLS)
        self.RETRY_MAX = cfg_kma.RETRY_MAX
        
        # 체크용
        self.emp = cfg.CHECK.EMPCODE
        self.cas = cfg.CHECK.CAS
    
    def __call__(self, df, time_col, lat_col, lon_col):
        """
        Description:
            원래 사고 데이터에서 기상 data를 추가.
                - 위/경도
                - 시간대
        Args:
            df: dataframe, 기상 data를 추가할 dataframe
            apikey: str, API에 사용할 key
            time_col: str, 사고시각 칼럼명
            lat_col: str, 위도 칼럼명
            lon_col: str, 경도 칼럼명
        Returns:
            wth_df: dataframe, 기상데이터 dataframe.
        """
        self.log.info(">>> Start to search Weather datas")
        start_time = time.time()
        
        # API key 불러오기
        apikey = self.decrypt_key()
        
        # API로부터 기상 데이터 수집
        grid_weathers = []
        for row in df[[time_col, lat_col, lon_col]].to_records():
            i, kst_tm, latlon_pt = row[0], row[1], (row[2], row[3])
            if i % self.PRINT_INTERVAL == 0 or i == len(df)-1:
                self.log.info("   Index of colleted API data: {0}".format(i))
            (kst_tm_1, kst_tm_2), grid_weights = self.get_grid_api_tm(kst_tm)
            # API 호출
            grid_weather_df = self.get_grid_weather(
                i, kst_tm_1, kst_tm_2, latlon_pt, apikey
            )
            # 결과는 pd.Series
            if grid_weights is None:
                grid_weather = grid_weather_df.iloc[0]
            else:
                grid_weather = grid_weather_df.apply(
                    lambda _row: _apply_weighted_sum(_row, grid_weights, kst_tm), axis=0
                )
            grid_weathers.append(grid_weather)
        
        # 1개 Dataframe으로 병합
        wth_df = pd.concat(grid_weathers, axis=1).T.reset_index(drop=True)
        
        # 맨 앞의 시간대 빼고 float 변환
        float_cols = list(self.use_cols.values())[1:]
        wth_df = wth_df.astype({col: float for col in float_cols})
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to search Weather datas: {0} hr {1} min {2:.2f} sec".format(h, m, s))
        
        return wth_df
    
    def make_rsa_key(self):
        """
        Description:
            RSA 공개키, 암호키 생성.
        Args:
            None
        Returns:
            None
        """
        self.rsa(mode="newkey")
    
    def encrypt_key(self):
        """
        Description:
            RSA 방식으로 암호파일 생성.
                - input을 넣었기 때문에 실행 후 별도 입력해줘야 함
        Args:
            None
        Returns:
            None
        """
        self.rsa(mode="encrypt")
    
    def decrypt_key(self):
        """
        Description:
            RSA 방식으로 암호파일 읽기.
        Args:
            file_path: str, 암호파일 경로
        Returns:
            key: str, 암호
        """
        key = self.rsa(mode="decrypt", file_tail=self.file_tail)
        return key
    
    def get_grid_api_tm(self, kst_tm):
        """
        Description:
            고해상도 그리드 데이터 기간 분기점.
                - (1997. 1. 1. ~ 2022. 12. 31.) 한시간 간격 (매시 00분)
                - (2023. 1. 1. ~) 매시 00분부터 5분 간격
            API의 사이 시간대이면 구간을 반환.
            이후에 비례식으로 선형 보간 처리할 수 있게 가중치 반환.
        Args:
            kst_tm: str, 연월일시분 (yyyymmddh24mi)
        Returns:
            interval: tuple, API에 사용할 (tm1, tm2) 구간 데이터
            WEIGHTS: tuple, 선형 보간에 사용할 가중치
        """
        TM_1HR = datetime.strptime("199701010000", self.CVT_TIME_FORMAT)
        TM_5MIN = datetime.strptime("202301010000", self.CVT_TIME_FORMAT)
        
        kst_dt = datetime.strptime(kst_tm, self.CVT_TIME_FORMAT)
        if kst_dt > TM_5MIN:
            self.log.debug("API interval: 5 min")
            INTERVAL_VALUE = 5
        elif kst_dt >= TM_1HR:
            self.log.debug("API interval: 1 hour")
            INTERVAL_VALUE = 60
        else:
            raise ValueError("Error time".format(kst_dt))
        
        bf_len = kst_dt.minute % INTERVAL_VALUE
        if bf_len == 0:
            WEIGHTS = None
            interval_s = kst_tm
            interval_e = kst_tm
        else:
            weight = bf_len / INTERVAL_VALUE
            WEIGHTS = (weight, 1-weight)
            interval_s = kst_dt - timedelta(minutes=bf_len)
            interval_e = kst_dt + timedelta(minutes=INTERVAL_VALUE-bf_len)
            interval_s = datetime.strftime(interval_s, self.CVT_TIME_FORMAT)
            interval_e = datetime.strftime(interval_e, self.CVT_TIME_FORMAT)
    
        return (interval_s, interval_e), WEIGHTS
    
    def get_grid_weather(
        self, idx, kst_tm_1, kst_tm_2, latlon_pt, apikey
        ):
        """
        Description:
            융합기상 - 고해상도 격자자료 API.
                - (1997. 1. 1. ~ 2022. 12. 31.) 한시간 간격 (매시 00분)
                - (2023. 1. 1. ~) 매시 00분부터 5분 간격
            obs에 항목1,항목2,... 식으로 문자열로 만들어 넣어야 함
                - ta: 기온, td: 이슬점온도, ta_chi: 체감온도, hm: 상대습도, 
                  wd_10m: 평균풍향_10분, ws_10m: 평균풍속_10분,
                  uu: 바람_u, vv: 바람_v,
                  pa: 현지기압, ps: 해면기압, vs: 시정,
                  rn_ox: 강수유무, rn_15m: 강수_15분,
                  rn_60m: 강수_60분, rn_day: 강수_1일
        Args:
            idx: int, dataframe의 index
            kst_tm_1: str, 시작 연월일시분 (yyyymmddh24mi)
            kst_tm_2: str, 종료 연월일시분 (yyyymmddh24mi, 필수)
            latlon_pt: tuple, 위/경도 데이터 (위도, 경도)
            apikey: str, API 키 값
        Returns:
            grid_weather_df: dataframe, API 결과 dataframe
        """
        lat, lon = latlon_pt
        grid_weather_cols = list(self.use_cols.values())
        grid_weather_str = ",".join(list(self.use_cols.keys())[1:])
        data_dict = {
            "tm1": kst_tm_1,
            "tm2": kst_tm_2,   # 없으면 제대로 안 나옴
            "lat": lat,
            "lon": lon,
            "obs": grid_weather_str,
            "itv": 5,   # 결과 시간간격 (tm1과 tm2보다 간격 이하여야 제대로 나옴)
            "help": 0,   # 도움말 표시: 1 / 미표시: 0
            "authKey": apikey,
        }
        
        _RE_NUM, _RETRY_MAX = 1, self.RETRY_MAX
        while _RE_NUM < _RETRY_MAX:
            response = requests.get(self.API_URL, params=data_dict)
            status_code = response.status_code
            if not status_code == 200:
                self.err_log.error("   Code: {0} / DF index: {1} / Retry: {2}".format(status_code, idx, _RE_NUM))
                _RE_NUM += 1
            else:
                _RE_NUM = _RETRY_MAX
        if not status_code == 200:
            raise ValueError("   Code: {0}".format(status_code))
        
        # 연월일시분이 12자리 / #으로 시작은 help 설명 또는 시작, 끝 부분
        grid_weather = [
            elm.split(", ")
            for elm in response.text.split("\n")
            if not elm.startswith("#") and len(elm) > 12
        ]
        grid_weather_df = pd.DataFrame(grid_weather, columns=grid_weather_cols)
        grid_weather_dtypes = {col: np.float32 for col in grid_weather_cols[1:]}
        grid_weather_dtypes[grid_weather_cols[0]] = str
        grid_weather_df = grid_weather_df.astype(grid_weather_dtypes)
        
        return grid_weather_df