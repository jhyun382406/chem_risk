"""
화학사고 예측 및 리스크 평가.

방법
- 공공데이터만 사용하여 진행
"""

import os
import datetime
import argparse
from setproctitle import setproctitle

from utils.log_module import LogConfig, get_log_view
from utils.code_utils import convert

import warnings
warnings.filterwarnings(action="ignore")


def train(cfg, log, err_log):
    from src.dataloader import RSRAeDataloader
    import src.network as src_net
    from src.model import RSRAE
    from src.train import TrainModel
    from utils.torch_settings import set_gpu_torch, set_random_seed, set_float32_matmul
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # GPU 설정, Random Seed 고정
    ctg_trn = cfg.TRAIN
    device = set_gpu_torch(ctg_trn.GPU.IDX, ctg_trn.GPU.MEMORY)
    set_random_seed(ctg_trn.HPARAMS.RANDOM_SEED, if_cudnn=True)
    set_float32_matmul("high")

    # Dataloader 생성
    rsr_dl = RSRAeDataloader(cfg, log, err_log)
    train_dl, valid_dl, add_hparams = rsr_dl()
    
    # Model 생성, 학습
    NET = getattr(src_net, cfg.TRAIN.NETWORK_VER)
    ae = NET(cfg.TRAIN.HPARAMS, add_hparams)
    model = RSRAE(ae.hparams, ae, log)
    trn = TrainModel(cfg, log, err_log)
    trn(model, add_hparams, train_dl, valid_dl, device, ctg_trn.LOAD_CKPT_PATH)


def test(cfg, log, err_log):
    from src.dataloader import RSRAeDataloader
    import src.network as src_net
    from src.model import RSRAE
    from src.test import TestModel
    from utils.torch_settings import set_gpu_torch, set_random_seed, set_float32_matmul
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")

    # GPU 설정, Random Seed 고정
    device = set_gpu_torch(cfg.TEST.GPU.IDX, cfg.TEST.GPU.MEMORY)
    set_random_seed(cfg.TRAIN.HPARAMS.RANDOM_SEED, if_cudnn=True)
    set_float32_matmul("high")

    # Dataloader 생성
    rsr_dl = RSRAeDataloader(cfg, log, err_log)
    test_input_df, test_label, test_dl = rsr_dl(is_test=True)
    
    # Model 생성 및 불러오기, 테스트
    hparams = convert(cfg.TEST.LOAD_HPARAMS_PATH)
    NET = getattr(src_net, cfg.TEST.NETWORK_VER)
    ae = NET(hparams)
    model = RSRAE(ae.hparams, ae, log)
    test = TestModel(cfg, log, err_log)
    test(
        model,
        test_input_df,
        test_label,
        test_dl,
        device
    )


def predict(input_data, cfg, log, err_log):
    from src.preprocess import PrepDatas
    from src.dataloader import RSRAePredDataloader
    import src.network as src_net
    from src.model import RSRAE
    from src.predict import PredictModel
    from utils.torch_settings import set_gpu_torch, set_random_seed, set_float32_matmul
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")

    # GPU 설정, Random Seed 고정
    device = set_gpu_torch(cfg.PREDICT.GPU.IDX, cfg.PREDICT.GPU.MEMORY)
    set_random_seed(cfg.TRAIN.HPARAMS.RANDOM_SEED, if_cudnn=True)
    set_float32_matmul("high")
    
    # 전처리, 추론 객체 생성
    prep = PrepDatas(cfg.PREPROCESS, log, err_log)
    pred = PredictModel(cfg, log, err_log)
    
    # 전처리 후 Dataloader 생성
    
    rsr_dl = RSRAePredDataloader(cfg, log, err_log)
    pred_dl = rsr_dl(input_df)
    
    # Model 생성 및 불러오기 + Trainer 생성
    hparams = convert(cfg.PREDICT.LOAD_HPARAMS_PATH)
    NET = getattr(src_net, cfg.PREDICT.NETWORK_VER)
    ae = NET(hparams)
    model = RSRAE(ae.hparams, ae, log)
    pred.load_model_state(model)
    trainer = pred.make_trainer(device)


def validate(input_data, cfg, log, err_log):
    # log.info("--------------------------------------------------")
    # err_log.error("--------------------------------------------------")
    pass


def data_extract(cfg, log, err_log):
    from src.extract_data import Comwel, GyeonggiCompany, GyeonggiFactory, GyeonggiChemicalHandling, GyeonggiChemicalAccident
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    cfg = cfg.EXTRACT
    
    # 고용 산재보험 가입 현황 (경기도)
    comwel = Comwel(cfg, log, err_log)
    comwel()
    
    # 경기도 업종별 사업체 현황
    gg_compy = GyeonggiCompany(cfg, log, err_log)
    gg_compy()
    
    # 경기도 공장등록 현황
    gg_fac = GyeonggiFactory(cfg, log, err_log)
    gg_fac()
    
    # 경기도 유해화학물질 취급사업장 현황
    gg_chem_handle = GyeonggiChemicalHandling(cfg, log, err_log)
    gg_chem_handle()
    
    # 경기도 유해화학물질 사고현황
    gg_chem_acci = GyeonggiChemicalAccident(cfg, log, err_log)
    gg_chem_acci()


def preprocess(cfg, log, err_log):
    from src.preprocess import MergeDatas, PrepDatas
    
    log.info("--------------------------------------------------")
    err_log.error("--------------------------------------------------")
    
    # 추출한 데이터 병합 후 저장
    merge = MergeDatas(cfg, log, err_log)
    merge()
    
    # 결측 처리, 인코딩, 스케일링 후 저장 (병합 선결돼야 함)
    prep = PrepDatas(cfg, log, err_log)
    prep()


def setting_log(cfg,
                upper_dir,
                log_level="INFO",
                err_log_level="WARNING"):
    """
    Description:
        로그 객체 생성.
    Args:
        cfg: Namedtuple, YAML 파일에서 읽어온 설정
        upper_dir: str, 해당 파일의 상위 디렉토리 경로
        log_level: str, 로그 레벨 (default="INFO")
            - DEBUG -> INFO -> WARNING -> ERROR -> CRITICAL
        err_log_level: str, 에러 로그 레벨 (default="WARNING")
            - DEBUG -> INFO -> WARNING -> ERROR -> CRITICAL
    Returns:
        log: logging.logger, 로그 객체
        err_log: logging.logger, 에러 로그 객체
    """
    file_prefix = cfg.LOG_PREFIX
    lc = LogConfig(upper_dir, file_prefix)
    log = get_log_view(lc, log_level=log_level)   # default: INFO
    err_log = get_log_view(lc, log_level=err_log_level, error_log=True)
    return log, err_log


def main(args):
    """
    Description:
        메인 모듈 실행함수.
        data 때는 tensorflow 또는 torch를 import 하지 않도록 분기 설정.

        사용 예
          - 사용 데이터 추출: python main.py -c /path/of/config/yaml -m extract
    Args:
        args: 파이썬 실행할 때, 입력한 값
              "-c", "--config": 설정 yaml 파일 경로
              "-m", "--mode": train, test, predict, extract 모드 변경
    """
    upper_dir = os.path.dirname(os.path.abspath(__file__))
    cfg_file = args.config
    cfg = convert(cfg_file)

    if args.mode == "train":
        setproctitle("chem_risk_train")
        log, err_log = setting_log(cfg.TRAIN, upper_dir)
        train(cfg, log, err_log)
        
    elif args.mode == "test":
        setproctitle("chem_risk_test")
        log, err_log = setting_log(cfg.TEST, upper_dir)
        test(cfg, log, err_log)
        
    elif args.mode == "predict":
        setproctitle("chem_risk_pred")
        log, err_log = setting_log(cfg.PREDICT, upper_dir)
        input_data = None
        predict(input_data, cfg, log, err_log)
    
    elif args.mode == "valid":
        setproctitle("chem_risk_valid")
        log, err_log = setting_log(cfg.VALID, upper_dir)
        input_data = None
        validate(input_data, cfg, log, err_log)
    
    elif args.mode == "extract":
        setproctitle("chem_risk_extract")
        log, err_log = setting_log(cfg.EXTRACT, upper_dir)
        data_extract(cfg, log, err_log)
    
    elif args.mode == "preprocess":
        setproctitle("chem_risk_prep")
        log, err_log = setting_log(cfg.PREPROCESS, upper_dir)
        preprocess(cfg, log, err_log)
    
    else:
        pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-c", "--config", type=str,
        help="a config file in yaml format to control experiment"
        )
    parser.add_argument(
        "-m", "--mode", type=str,
        help="executition mode",
        choices=["train", "test", "predict", "valid", "extract", "preprocess"]
        )

    args = parser.parse_args()

    main(args)
