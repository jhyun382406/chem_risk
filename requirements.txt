absl-py==1.4.0
aiohttp==3.8.4
aiosignal==1.3.1
alabaster==0.7.13
annotated-types==0.5.0
asttokens==2.0.5
astunparse==1.6.3
async-timeout==4.0.2
attrs==22.1.0
autopep8==2.0.4
Babel==2.12.1
backcall==0.2.0
beautifulsoup4==4.11.1
boltons==23.0.0
brotlipy==0.7.0
cachetools==5.3.1
category-encoders==2.6.0
certifi==2023.5.7
cffi==1.15.1
chardet==4.0.0
charset-normalizer==2.0.4
chex==0.1.5
click==8.1.3
cloudpickle==2.2.1
conda==23.3.1
conda-build==3.23.3
conda-package-handling==1.9.0
contourpy==1.0.7
cryptography==38.0.1
cycler==0.11.0
dask==2022.10.2
debugpy==1.5.1
decorator==5.1.1
deepchem==2.7.2.dev20230730200710
dgl==1.1.2+cu116
dglgo==0.0.2
dgllife==0.3.2
dm-haiku==0.0.10
dm-tree==0.1.8
dnspython==2.2.1
docutils==0.20.1
easydict==1.10
entrypoints==0.4
exceptiongroup==1.0.4
executing==0.8.3
expecttest==0.1.4
filelock==3.6.0
flatbuffers==23.5.26
flit_core==3.6.0
fonttools==4.39.4
frozenlist==1.3.3
fsspec==2023.5.0
future==0.18.2
gast==0.4.0
glob2==0.7
google-auth==2.22.0
google-auth-oauthlib==1.0.0
google-pasta==0.2.0
grpcio==1.57.0
h5py==3.7.0
hyperopt==0.2.7
hypothesis==6.61.0
idna==3.4
imagesize==1.4.1
imbalanced-learn==0.11.0
install==1.3.5
ipykernel==6.15.0
ipython==8.7.0
isort==5.12.0
jax==0.4.6
jaxlib==0.4.6
jedi==0.18.1
Jinja2==3.1.2
jmp==0.0.4
joblib==1.2.0
jsonpatch==1.32
jsonpointer==2.0
jupyter-client==7.3.4
jupyter_core==4.12.0
keras==2.13.1
Keras-Preprocessing==1.1.2
kiwisolver==1.4.4
libarchive-c==2.9
libclang==16.0.6
lightning-utilities==0.8.0
littleutils==0.2.2
locket==1.0.0
Markdown==3.4.4
MarkupSafe==2.1.3
matplotlib==3.7.1
matplotlib-inline==0.1.6
mkl-fft==1.3.1
mkl-random==1.2.2
mkl-service==2.4.0
ml-dtypes==0.2.0
mpmath==1.2.1
multidict==6.0.4
nest-asyncio==1.5.6
networkx==3.1
numpy==1.23.4
numpydoc==1.5.0
nvidia-cublas-cu11==11.11.3.6
nvidia-cuda-cupti-cu11==11.8.87
nvidia-cuda-nvcc-cu11==11.8.89
nvidia-cuda-nvrtc-cu11==11.8.89
nvidia-cuda-runtime-cu11==11.8.89
nvidia-cudnn-cu11==8.9.4.25
nvidia-cufft-cu11==10.9.0.58
nvidia-cusolver-cu11==11.4.1.48
nvidia-cusparse-cu11==11.7.5.86
oauthlib==3.2.2
ogb==1.3.6
opencv-python==4.5.5.64
opt-einsum==3.3.0
optax==0.1.3
outdated==0.2.2
packaging==23.1
pandas==1.5.2
parso==0.8.3
partd==1.4.0
patsy==0.5.3
pexpect==4.8.0
pickleshare==0.7.5
Pillow==9.3.0
pip==23.2.1
pkginfo==1.8.3
pluggy==1.0.0
prompt-toolkit==3.0.20
protobuf==4.24.2
psutil==5.9.0
ptyprocess==0.7.0
pure-eval==0.2.2
py4j==0.10.9.7
pyasn1==0.5.0
pyasn1-modules==0.3.0
pycodestyle==2.11.0
pycosat==0.6.4
pycparser==2.21
pydantic==2.3.0
pydantic_core==2.6.3
Pygments==2.16.1
pyOpenSSL==22.0.0
pyparsing==3.0.9
PySocks==1.7.1
python-dateutil==2.8.2
python-etcd==0.4.5
pytorch-lightning==1.9.5
pytz==2022.1
PyYAML==6.0
pyzmq==25.0.2
rdkit==2023.3.3
rdkit-pypi==2022.9.5
requests==2.28.1
requests-oauthlib==1.3.1
rsa==4.9
ruamel.yaml==0.17.21
ruamel.yaml.clib==0.2.6
scikit-learn==1.2.2
scipy==1.10.1
setproctitle==1.3.2
setuptools==65.5.0
six==1.16.0
snowballstemmer==2.2.0
sortedcontainers==2.4.0
soupsieve==2.3.2.post1
Sphinx==7.2.5
sphinxcontrib-applehelp==1.0.7
sphinxcontrib-devhelp==1.0.5
sphinxcontrib-htmlhelp==2.0.4
sphinxcontrib-jsmath==1.0.1
sphinxcontrib-qthelp==1.0.6
sphinxcontrib-serializinghtml==1.1.9
stack-data==0.2.0
statsmodels==0.13.5
sympy==1.11.1
tabulate==0.9.0
tensorboard==2.13.0
tensorboard-data-server==0.7.1
tensorboard-plugin-wit==1.8.1
tensorflow==2.13.0
tensorflow-estimator==2.13.0
tensorflow-io-gcs-filesystem==0.33.0
termcolor==2.3.0
threadpoolctl==3.1.0
toml==0.10.2
tomli==2.0.1
toolz==0.12.0
torch==1.13.1
torch-geometric==2.3.1
torchelastic==0.2.2
torchinfo==1.7.2
torchmetrics==0.11.4
torchtext==0.14.1
torchvision==0.14.1
tornado==6.1
tqdm==4.64.1
traitlets==5.7.1
typer==0.9.0
types-dataclasses==0.6.6
typing_extensions==4.4.0
urllib3==1.26.13
wcwidth==0.2.5
Werkzeug==2.3.7
wheel==0.37.1
wrapt==1.15.0
yarl==1.9.2
