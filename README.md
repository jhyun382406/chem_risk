# Predict Chemical Risk & Evaluate Risk

[[_TOC_]]

## Name
- 화학사고 예측 및 리스크 평가
- 화학사고 예측 모델
  - [공공 데이터 포털](https://www.data.go.kr/) 및 [경기데이터드림](https://data.gg.go.kr)의 공개 데이터 사용
  - Pytorch 이용하여 [RSR (Robust Subspace Recovery) AutoEncoder](https://github.com/marrrcin/rsrlayer-pytorch) 반영
- 화학사고 위험성 평가 모델
  - 화학물질안전원, 안전보건공단 등 공개자료에서 수집한 데이터 사용(비정형 데이터 어느정도 수기 처리)
  - CAS번호는 SMILES로 변환하여 Deepchem 적용
  - 기상 데이터 API로 받아와서 적용 진행 중

## Description
- 화학사고 예측 모델
  - 이상탐지 알고리즘(AutoEncoder) 우선 반영  

  |항목|상세|
  |:---:|:---|
  |사용 데이터|① 고용 산재보험 가입 현황<br>② 경기도 업종별 사업체 현황<br>③ 경기도 공장등록 현황<br>④ 경기도 유해화학물질 취급사업장 현황<br>⑤ 경기도 유해화학물질 사고현황|
  |데이터 병합|- 1차: 도로명주소<br>- 2차: 고용보험업종코드<br>- 3차: 조사년도<br>- ③ ← (①, ②) ← ④ ← ⑤<br>&emsp;* (A, B): Inner join<br>&emsp;* A ← B: Left join|
  |Encoding & Scaling|- 순서 없는 범주형 항목<br>&emsp;: Binary Encoding 적용<br>- 순서 있는 범주형 항목<br>&emsp;: Ordinal Encoding 적용 → MinMax Scaling<br>- 수치형 항목<br>&emsp;: MinMax Scaling<br>&emsp;: Quantile transformation (Unifor PDF)|
  |결측 처리|- 데이터 병합 후 진행<br>- 산재/고용보험상시근로자수의 경우 Random 값 사용하는 부분 있음<br>- 이외 칼럼들은 특정 값으로 처리|
  |정상/비정상 구분|- Target 칼럼들에서 값 존재 여부로 판단<br>- 1개 칼럼에만 값이 있어도 사고(비정상) 데이터로 판단|  
  
  - 경기도 데이터만 썼기 때문에 다른 지역 추론 확장성을 위해 지역 관련 feature 제외  
  
  |Model 버전<br>(network.py)|상세|
  |:---:|:---|
  |V1|기본 RSR AutoEncoder|
  |V2|Layer 추가 및 units 조정|
  |V3|Skip connection 추가 (Concatenate)|
  |V4|Skip connection 추가 (Add)|  

- 화학사고 위험성 평가 모델
  - DNN+MAT 우선 반영  

  |항목|상세|
  |:---:|:---|
  |사용 데이터|① 화학사고 수집 데이터<br>② 고용노동부 고용업종코드<br>③ 한국생명공학연구원 국가연구안전정보시스템 운영현황|
  |결측 처리|- 증강 전에 1차 처리 후, 증강 후에도 2차 처리<br>- 범주형, bool, int, float 각각 feature 별 특징에 따라 결측 처리<br>이외 칼럼들은 특정 값으로 처리|
  |Target 생성<br>(임의 규칙 반영)|① 사망자에 가중치 5, 부상자에 가중치 1을 두어 가중평균 계산 (λ_𝑚𝑒𝑎𝑛=5×𝐷𝑒𝑎𝑑𝑠+1×𝐼𝑛𝑗𝑢𝑟𝑒𝑑)<br>② λ_𝑚𝑒𝑎𝑛을 적용한 랜덤 포아송 분포로 무작위 값 λ_𝑟𝑎𝑛𝑑𝑜𝑚 산출<br>③ λ_𝑚𝑒𝑎𝑛+λ_𝑟𝑎𝑛𝑑𝑜𝑚 으로 위험지수 산출<br>④ 구간 분리점을 수동 지정하여 위험성 단계를 1\~5단계로 범주화 (이상\~미만 범위)|
  |데이터 증강|- 학습 데이터만 증강<br>&emsp;: SMOTE를 이용하여 불균형 label 데이터 증강<br>&emsp;: 복원추출로 SMOTE 데이터의 REPEAT_NUM 배수만큼 증강|
  |Encoding & Scaling|- 범주형 항목<br>&emsp;: Ordinal, CatBoost, Binary 적용<br>- CAS번호는 SMILES로 변환|
  
  - pretrained 설정에는 원래 MAT 네트워크의 pretrained 모델의 weights 불러와서 적용

## Environment
- Base 도커 이미지: pytorch/pytorch:1.13.1-cuda11.6-cudnn8-runtime
- 우분투 버전: Ubuntu 18.04.6

|사용 라이브러리|버전|
|:---:|---:|
|Python|3.10.8|
|torch|1.13.1|
|torchinfo|1.7.2|
|pytorch-lightning|1.9.5|
|numpy|1.23.4|
|pandas|1.5.2|
|dask|2022.10.2|
|opencv-python|4.5.5.64|
|matplotlib|3.7.1|
|scikit-learn|1.2.2|
|category-encoders|2.6.0|
|deepchem|2.7.2.dev20230730200710|
|imbalanced-learn|0.11.0|
|tensorflow|2.13.0|

- 이외 파이썬 라이브러리: requirements.txt
- dockerfile 참고: ./configs/dna_chem_risk_torch.dockerfile

## Execution
- 화학사고 예측 모델  
  main.py 에서 config/ 에 있는 yaml 파일을 반영하여 실행
  - 데이터 선별  
  ```python
  python main.py -c /path/of/config/file -m extract
  ```
  - 학습 전처리  
  ```python
  python main.py -c /path/of/config/file -m preprocess
  ```
  - 학습  
  ```python
  python main.py -c /path/of/config/file -m train
  ```
  - 테스트  
  ```python
  python main.py -c /path/of/config/file -m test
  ```
- 화학사고 위험성 평가 모델  
  main_m2.py 에서 config/ 에 있는 yaml 파일을 반영하여 실행(화학사고 수집 데이터)  
  - 데이터 선별 + 1차 전처리  
  ```python
  python main_m2.py -c /path/of/config/file -m extract
  ```
  - 학습  
  ```python
  python main_m2.py -c /path/of/config/file -m train
  ```
  - 테스트  
  ```python
  python main_m2.py -c /path/of/config/file -m test
  ```

## File Structure
```bash
├── checkpoints/   # 체크포인트, 모델, 학습로그, 사용 hyp, 테스트 결과 저장
├── collected_data/   # 화학사고 위험성 평가 모델에 사용할 수집 데이터
├── configs/   # 설정 yaml 파일
├── log/   # 로그 파일
├── err_log/   # 에러 로그 파일
├── pkl_files/   # Encoder, Scaler 등의 pickle 파일
├── processed_data/   # 원본 파일을 정제한 파일
├── public_data/   # 화학사고 예측 모델에 사용할 공공 데이터 원본 파일
│   ├── comwel/   # 근로복지공단 고용산재보험 가입현황
│   └── gyeonggi_datadream/   # 경기데이터드림
├── utils/   # 범용 설정
│   ├── code_utils.py   # 범용 유틸리티 함수
│   ├── df_utils.py   # Dataframe I/O 관련 유틸리티 함수
│   ├── log_module.py   # 로그 용
│   ├── torch_settings.py   # pytorch 환경 설정
│   └── wrap_func.py   # 데코레이터 함수 설정
├── src/   # 화학사고 예측 모델
│   ├── dataloader.py   # Dataloader 생성
│   ├── extract_data.py   # 원본 데이터에서 사용할 칼럼만 추출하여 저장
│   ├── loss_function.py   # Loss 함수
│   ├── model.py   # pytorch-lightning 학습/테스트 step 함수 override
│   ├── network.py   # 모델 네트워크 구성
│   ├── predict.py   # 추론 및 추론 후처리 (미작성)
│   ├── preprocess.py   # 추출된 데이터 병합 및 전처리
│   ├── test.py   # 테스트 데이터셋으로 모델 전후 데이터 값 비교, 이미지 저장
│   └── train.py   # 모델 학습 및 저장
├── src_m2/   # 화학사고 위험성 평가 모델
│   ├── apply_func.py   # Dataframe apply에 사용하는 함수
│   ├── dataloader.py   # Dataloader 생성
│   ├── extract_data.py   # 원본 데이터에서 사용할 칼럼만 추출하여 1차 처리 후 저장
│   ├── loss_function.py   # Loss 함수
│   ├── model_hparams.py   # Hyperparameter 및 기타 학습 관련 설정
│   ├── model.py   # pytorch-lightning 학습/테스트 step 함수 override
│   ├── network_dnn.py   # DNN 네트워크 구성
│   ├── network_dnnmat.py   # DNN+MAT 네트워크 구성
│   ├── network_mat.py   # MAT 네트워크 구성, deepchem 클래스 override 하여 generator 변경
│   ├── preprocess.py   # 추출된 데이터 전처리
│   ├── test.py   # Confusion Matrix 및 관련 수치 산출, ROC curve 이미지 저장
│   ├── train.py   # 모델 학습 및 저장
│   └── weather_api.py   # 기상청 API를 이용한 기상 데이터 수집
├── kma_api/   # 기상 데이터 수집 관련 API 정보
│   ├── apk_key/   # RSA 암호화한 key 저장
│   └── rsa_apikey.py   # API key를 RSA로 암호화하기 위한 모듈
├── main.py   # 화학사고 예측 모델 메인 모듈 (데이터 선별, 학습 전처리, 학습, 테스트)
└── main_m2.py   # 화학사고 위험성 평가 모델(수집 데이터) 메인 모듈 (데이터 선별, 학습 전처리, 학습, 테스트)
```

## Additional Research
- 코드 수정 및 리팩토링
  - 추가 features에 맞게 전처리, 학습 코드 수정
  - 추론 코드 추가
  - 위험성 평가를 5단계에서 3단계로 변경
