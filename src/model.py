"""
pytorch-lightning을 이용한 RSR AutoEncoder 학습.

https://github.com/marrrcin/rsrlayer-pytorch 참고
"""
import torch
import pytorch_lightning as pl

from src.network import RSRAutoEncoderV1
from src.loss_function import L2p_Loss, RSRLoss

import warnings
warnings.filterwarnings(action="ignore")


class RSRAE(pl.LightningModule):
    """
    Description:
        Pytorch-lightning에 적용할 모델.
        각각 step 설정.
    """
    def __init__(self, hparams, ae, f_log):
        """
        Description:
            RSR AutoEncoder를 Pytorch-lightning 적용하기 위한 init.
            hyperparameters 저장은 train에서 별도로 진행 (재사용 가능하도록)
        Args:
            hparams: namedtuple, 하이퍼 파라미터
            ae: Model, RSR AutoEncoder 모델
            f_log: logging instance, 파일 및 스트림으로 찍을 로그 인스턴스
        Returns:
            None
        """
        super(RSRAE, self).__init__()
        # self.save_hyperparameters()   # Save all hyperparameters automatically
        # self.save_hyperparameters("hparams")   # Save selected arguments' hyperparameters automatically
        self.hyp = hparams
        self.ae = ae
        self.reconstruction_loss = L2p_Loss(p=1.0)
        self.rsr_loss = RSRLoss(
            self.hyp.lambda1, self.hyp.lambda2, self.hyp.d, self.hyp.D
        )
        self.cos_sim = torch.nn.CosineSimilarity(dim=1, eps=1e-6)
        self.f_log = f_log

    def forward(self, x):
        """
        Description:
            RSR AutoEncoder 결과.
        Args:
            x: tensor, Input data, (batch_size, input_features)
            f_log: logging instance, 파일 및 스트림으로 찍을 로그 인스턴스
        Returns:
            enc: tensor, Encoder result, (batch_size, D)
            dec: tensor, Decoder result, (batch_size, input_features)
            latent: tensor, RSR result, (batch_size, d)
            A: tensor, orthogonal initial, (batch_size, d, D)
        """
        return self.ae(x)

    def training_step(self, batch, batch_idx):
        """
        Description:
            * Override
            Train 중에 반영할 로직 (trainer.fit).
            전체 loss, reconstruction loss, rsr loss를 산출.
            각각 loss 및 learning rate scheduler에 의한 learning rate를 저장.
                - 각각 on_step, on_epoch, progress_bar 표출 처리
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            loss_dict: loss 결과 딕셔너리
        """
        loss, rec_loss, rsr_loss = self._shared_step(batch, batch_idx, "train")

        # Add log some usefull stuff
        # on_step: save logger on step / on_epoch: save logger on epoch
        self.log(
            "reconstruction_loss",
            rec_loss.item(),
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        self.log(
            "rsr_loss",
            rsr_loss.item(),
            on_step=False,
            on_epoch=True,
            prog_bar=False
        )
        self.log(
            "loss",
            loss.item(),
            on_step=False,
            on_epoch=True,
            prog_bar=False
        )
        self.log(
            "lr_onecycle",
            self.lr_schedulers().get_last_lr()[0],
            on_step=False,
            on_epoch=True,
            prog_bar=False
        )
        return {"loss": loss}
    
    def on_train_end(self):
        """
        Description:
            * Override
            Train 종료 시 로그.
        Args:
            None
        Returns:
            None
        """
        super().on_train_end()
        self.f_log.info(">>> Train End")
    
    def validation_step(self, batch, batch_idx):
        """
        Description:
            * Override
            Train 중에 Validation에 반영할 로직 (trainer.fit).
            전체 val_data에 대해 loss, reconstruction loss, rsr loss를 산출.
            각각 loss를 저장.
                - 각각 on_step, on_epoch, progress_bar 표출 처리
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            loss_dict: loss 결과 딕셔너리
        """
        loss, rec_loss, rsr_loss = self._shared_step(batch, batch_idx, "train")
        
        # Add log some usefull stuff
        # on_step: save logger on step / on_epoch: save logger on epoch
        self.log(
            "val_reconstruction_loss",
            rec_loss.item(),
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        self.log(
            "val_rsr_loss",
            rsr_loss.item(),
            on_step=False,
            on_epoch=True,
            prog_bar=False
        )
        self.log(
            "val_loss",
            loss.item(),
            on_step=False,
            on_epoch=True,   # Recognized from EarlyStopping
            prog_bar=True
        )
        
        return {"val_loss": loss}
    
    def test_step(self, batch, batch_idx):
        """
        Description:
            * Override
            Test 중 반영할 로직 (trainer.test).
            전체 test_data에 대해 loss, reconstruction loss, rsr loss를 산출.
            코사인 유사도도 산출.
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            loss: tensor, 최종 loss 값
            rec_loss: tensor, Reconstruction loss, input 값과 output 값 간의 차이
            rsr_loss: tensor, RSR loss, Projection loss와 PCA loss의 가중합
            cos_sim_value: tensor, 모델 input과 output 간의 Cosine Similarity
        """
        loss, rec_loss, rsr_loss, x, x_hat = self._shared_step(batch, batch_idx, "eval")
        cos_sim_value = torch.mean(self.cos_sim(x, x_hat))
        return loss, rec_loss, rsr_loss, cos_sim_value
    
    def test_epoch_end(self, test_outputs):
        """
        Description:
            * Override
            Test 중 epoch 종료 시 로그.
            최종적으로 Loss 값 및 코사인 유사도 값을 로그로 출력.
        Args:
            test_outputs: list, test_step의 결과 값 (batch_size 별로 1개 원소)
        Returns:
            None
        """
        # # x_hat은 shape이 다르므로 별도 처리
        # x_hat_list = []
        # for i, test_output in enumerate(test_outputs):
        #     x_hat_list.append(test_output[-1])
        #     test_outputs[i] = test_output[:4]
        test_losses = torch.as_tensor(test_outputs)
        loss = torch.mean(test_losses[::, 0])
        rec_loss = torch.mean(test_losses[::, 1])
        rsr_loss = torch.mean(test_losses[::, 2])
        cos_sim_value = torch.mean(test_losses[::, 3])

        self.f_log.info(">>> Total Loss Mean: {0:.5f}".format(loss))
        self.f_log.info(">>> Total Reconstruction Loss Mean: {0:.5f}".format(rec_loss))
        self.f_log.info(">>> Total RSR Loss Mean: {0:.5f}".format(rsr_loss))
        self.f_log.info(">>> Total Cosine Similarity: {0:.5f}".format(cos_sim_value))
    
    def on_test_end(self):
        """
        Description:
            * Override
            Test 종료 시 로그.
        Args:
            None
        Returns:
            None
        """
        super().on_test_end()
        self.f_log.info(">>> Test End")
    
    def predict_step(self, batch, batch_idx):
        """
        Description:
            * Override
            Predict 중 반영할 로직 (trainer.predict).
            입력한 데이터에 대해 모델 결과값 산출.
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            x_hat: numpy array, 모델 결과 값, (batch_size, input_features)
        """
        x = batch
        x = x.to(torch.float32)   # default dataloader dtype: torch.float64
        _, dec, _, _ = self.ae(x)
        x_hat = torch.sigmoid(dec)
        return x_hat.cpu().detach().numpy()   # convert to numpy array
    
    def _shared_step(self, batch, batch_idx, mode):
        """
        Description:
            * Override
            Train, Validation, Test 중 공통 로직.
            전체 test_data에 대해 loss, reconstruction loss, rsr loss를 산출.
            eval일때는 모델 input, ouput도 산출.
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
            mode: str, 학습/검증은 train, 테스트는 eval
        Returns:
            loss: tensor, 최종 loss 값
            rec_loss: tensor, Reconstruction loss, input 값과 output 값 간의 차이
            rsr_loss: tensor, RSR loss, Projection loss와 PCA loss의 가중합
            x: tensor, model input, (batch_size, input_features)
            x_hat: tensor, model output, (batch_size, input_features)
        """
        x = batch
        x = x.to(torch.float32)   # default dataloader dtype: torch.float64
        enc, dec, latent, A = self.ae(x)
        x_hat = torch.sigmoid(dec)
        
        rec_loss = self.reconstruction_loss(x_hat, x)
        rsr_loss = self.rsr_loss(enc, A)
        loss = rec_loss + rsr_loss
        
        if mode == "train":
            return loss, rec_loss, rsr_loss
        elif mode == "eval":
            return loss, rec_loss, rsr_loss, x, x_hat
        else:
            return None

    def configure_optimizers(self):
        """
        Description:
            * Override
            학습에서 Optimizer 반영.
            Learning rate Scheduler 이용.
        Args:
            batch: tensor, Input data, (batch_size, input_features)
            batch_idx: int, 배치 인덱스 값
        Returns:
            opt_list: list, 옵티마이저
            lr_scheduler_list: list, Learning rate Scheduler가 개별 딕셔너리로 구성
        """
        # default: manage learning rate by epochs
        # setting "interval": "step": manage learning rate by steps
        opt = torch.optim.AdamW(self.parameters(), lr=self.hyp.lr)
        # Fast.AI's best practices :)
        scheduler = torch.optim.lr_scheduler.OneCycleLR(
            opt,
            max_lr=self.hyp.lr,
            epochs=self.hyp.epochs,
            steps_per_epoch=self.hyp.steps_per_epoch,
            div_factor=self.hyp.div_factor   # initial_lr = max_lr/div_factor, default=25.0
        )
        return [opt], [{"scheduler": scheduler, "name": "lr_onecycle", "interval": "step"}]