import os
import datetime
import time
import yaml
import torch
from torchinfo import summary
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.loggers.csv_logs import CSVLogger
from pytorch_lightning import Trainer

from src.model import RSRAE
from utils.code_utils import return_dir, unpack
from utils.log_module import print_elapsed_time

import warnings
warnings.filterwarnings(action="ignore")


class TrainModel(object):
    """모델 학습."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            Pytorch-lightning 이용한 학습.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(TrainModel, self).__init__()
        cfg = cfg.TRAIN
        self.cfg_hyp = cfg.HPARAMS
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.train_accel = cfg.TRAIN_ACCEL
        self.model_name = cfg.MODEL_NAME
        self.model_version = cfg.MODEL_VER
        self.net_version = cfg.NETWORK_VER
        self.model_filename = cfg.MODEL_FILENAME
        self.summary_filename = cfg.SUMMARY_FILENAME
        self.hparams_filename = cfg.CFG_HPARAMS_FILENAME
        
        # 추가 생성 변수
        train_date = datetime.datetime.now().strftime("%Y%m%d")
        date_ver = "{0}_{1}".format(train_date, cfg.MODEL_VER)
        self.ckpt_dir = return_dir(True, cfg.CKPT_DIR, date_ver)

    def __call__(self,
                 model,
                 add_hparams,
                 train_dl,
                 valid_dl,
                 device,
                 load_ckpt_path=None):
        """
        Description:
            Model, Dataloader를 이용하여 학습 진행.
            모델, 체크포인트, 학습로그, 하이퍼파라미터 저장.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            add_hparams: dict, hparams에 추가할 딕셔너리
            train_dl: dataloader, 학습 데이터 dataloader
            valid_dl: dataloader, 검증 데이터 dataloader
            device: torch.device, 할당된 장치
            load_ckpt_path: str, 이어서 학습할 체크포인트 경로 (default=None)
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to Train model")
        hyp = model.hyp
        
        # Model summary & Save hyperparameters (same as original yaml file)
        summary_dir, summary_path = self.log_model_summary(model, hyp)
        self.save_cfg_hparams(self.cfg_hyp, add_hparams, summary_dir)
        
        # Make Callbacks
        self.make_callbacks(hyp)
        
        # Make trainer & Train model
        trainer = self.make_trainer(model, device)
        self.train_model(
            trainer,
            model, 
            train_dl,
            valid_dl,
            load_ckpt_path
        )

        # Load best checkpoint & Save best model
        model = self.load_best_model(model)
        self.save_model(model)
        
        # Modify model log
        h, m, s = print_elapsed_time(start_time)
        self.modify_model_log(
            summary_path,
            trainer.current_epoch,
            trainer.max_epochs,
            (h, m, s)
        )
        self.log.info(">>> End to Train model: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def log_model_summary(self, model, hyp, remain_log=True):
        """
        Description:
            모델 summary를 로그로 남김.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            hyp: easyDict, 하이퍼 파라미터 딕셔너리 (model에서 생성)
            remain_log: bool, 모델 summary를 log에 찍을지 여부 (default=True)
        Returns:
            summary_dir: str, 모델 summary 로그 파일 저장 디렉토리
            summary_path: str, 모델 summary 로그 파일 저장 경로
        """
        summary_ipt_size = (hyp.batch_size, hyp.input_dim)
        model_summary = summary(model.ae, input_size=summary_ipt_size)
        if remain_log:
            self.log.info("Model Summary\n{0}".format(model_summary))
        
        # Same as csv_logs path
        summary_dir = return_dir(
            True,
            self.ckpt_dir,
            hyp.csv_logger_name,
            self.net_version
        )
        summary_path = os.path.join(summary_dir, self.summary_filename)
        try:
            with open(summary_path, "w", encoding="utf-8") as f:
                f.write("Model name: {0}\n".format(self.model_filename))
                f.write("Model version: {0}\n".format(self.model_version))
                f.write("Network version: {0}\n".format(self.net_version))
                f.write("Model summary\n")
                f.write("{0}\n".format(model_summary))
        except Exception as e:
            self.log.info("Save Model summary file incompletely")
            self.err_log.error("Save Model summary file incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save Model summary file completely")
        
        return summary_dir, summary_path
    
    def save_cfg_hparams(self, cfg_hyp, add_hparams, summary_dir):
        """
        Description:
            추후 재사용 위해 yaml 파일의 하이퍼 파라미터를 별도 저장.
            namedtuple을 딕셔너리로 변환 후 yaml 파일로 저장.
        Args:
            cfg_hyp: namedtuple, 하이퍼 파라미터 (yaml 파일에 있던 값)
            add_hparams: dict, hparams에 추가할 딕셔너리
            summary_dir: str, 모델 summary 로그 파일 저장 디렉토리
        Returns:
            None
        """
        cfg_hyp = unpack(cfg_hyp)
        cfg_hyp.update(add_hparams)
        hparams_path = os.path.join(summary_dir, self.hparams_filename)
        try:
            with open(hparams_path, "w", encoding="utf-8") as f:
                yaml.dump(cfg_hyp, f)
        except Exception as e:
            self.log.info("Save Reusing hyperparameters file incompletely")
            self.err_log.error("Save Reusing hyperparameters file incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save Reusing hyperparameters file completely")
    
    def make_callbacks(self, hyp):
        """
        Description:
            학습에 사용할 Callbacks 생성.
                - Checkpoint Callback
                    - dirpath: 체크포인트 파일 저장경로
                    - monitor: 저장기준이 되는 지표
                    - mode: 저장기준, min 또는 max
                    - save_top_k: 최대 k개까지 저장
                    - filename: 체크포인트 파일명
                - EarlyStopping Callback
                    - monitor: 조기종료 기준이 되는 지표
                    - min_delta: 해당 값 미만이면 개선되었다고 처리되지 않음
                    - patience: 해당 값까지 개선되지 않으면 조기종료
                    - mode: 개선기준, min 또는 max
                - LearningRate Monitor Callback
                    - logging_interval: 저장 간격, epoch 또는 step
                - CSV logger Callback
                    - save_dir: CSV 로그 저장 상위 디렉토리
                    - name: save_dir 하위에 CSV 로그 저장할 곳
                    - version: name 하위에 CSV 로그 저장할 곳
                    - save_dir/name/version 경로로 저장됨
        Args:
            hyp: easyDict, 하이퍼 파라미터 딕셔너리 (model에서 생성)
        Returns:
            None
        """
        # Init ModelCheckpoint callback, monitoring 'val_loss'
        self.checkpoint_callback = ModelCheckpoint(
            dirpath=self.ckpt_dir,
            monitor=hyp.ckpt_monitor,
            mode=hyp.ckpt_mode,
            save_top_k=hyp.ckpt_top_k,
            filename=self.model_name
        )
        self.early_stop_callback = EarlyStopping(
            monitor=hyp.es_monitor,
            min_delta=hyp.es_min_delta,
            patience=hyp.es_patience,
            verbose=False,
            mode=hyp.es_mode
        )
        self.lr_monitor = LearningRateMonitor(
            logging_interval=hyp.lr_log_interval
        )
        self.csv_logger = CSVLogger(
            self.ckpt_dir,
            name=hyp.csv_logger_name,
            version=self.net_version
        )
    
    def make_trainer(self, model, device):
        """
        Description:
            Pytorch-lightning의 trainer 생성.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            device: torch.device, 할당된 장치
        Returns:
            trainer: Trainer, Pytorch-lightning의 trainer
        """
        trainer = Trainer(
            default_root_dir=self.ckpt_dir,
            max_epochs=model.hyp.epochs,
            callbacks=[
                self.checkpoint_callback,
                self.early_stop_callback,
                self.lr_monitor
            ],
            logger=self.csv_logger,
            accelerator=self.train_accel,
            devices=[device.index]
        )
        return trainer
    
    def train_model(self,
                    trainer,
                    model, 
                    train_dl,
                    valid_dl,
                    load_ckpt_path):
        """
        Description:
            학습 진행.
        Args:
            trainer: Trainer, Pytorch-lightning의 trainer
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            train_dl: dataloader, 학습 데이터 dataloader
            valid_dl: dataloader, 검증 데이터 dataloader
            load_ckpt_path: str, 이어서 학습할 체크포인트 경로
        Returns:
            None
        """
        if load_ckpt_path is None:
            # Start training
            trainer.fit(model, train_dl, valid_dl)
        else:
            # Resume training state
            # automatically restores model, epoch, step, LR schedulers, etc...
            self.log.info("Using Checkpoint: {0}".format(os.path.basename(load_ckpt_path)))
            trainer.fit(model, train_dl, valid_dl, ckpt_path=load_ckpt_path)
    
    def load_best_model(self, model):
        """
        Description:
            Best checkpoint 불러오기.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
        Returns:
            best_model: LightningModule, 학습 결과가 best인 모델
        """
        try:
            best_model = model.load_from_checkpoint(
                self.checkpoint_callback.best_model_path,
                hparams=model.hyp,
                ae=model.ae,
                f_log=model.f_log
            )
        except Exception as e:
            self.log.info("Load Best Model incompletely")
            self.err_log.error("Load Best Model incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Load Best Model completely")
        return best_model
    
    def save_model(self, model):
        """
        Description:
            Model 파일 저장.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
        Returns:
            None
        """
        model_path = os.path.join(self.ckpt_dir, self.model_filename)
        try:
            torch.save(model.state_dict(), model_path)
        except Exception as e:
            self.log.info("Save Model incompletely")
            self.err_log.error("Save Model incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save Model completely")
    
    def modify_model_log(self,
                         summary_path,
                         current_epoch,
                         max_epochs,
                         elapsed_time):
        """
        Description:
            학습 종료 후 모델 summary 로그에 소요시간, 학습한 epoch 추가.
        Args:
            summary_path: str, 모델 summary 로그 파일 저장 경로
            current_epoch: int, 모델 학습 종료 시점의 epoch
            max_epochs: int, 설정된 모델 학습 최대 epoch
            elapsed_time: tuple, 소요시간 (h, m, s) 꼴
        Returns:
            None
        """
        h, m, s = elapsed_time
        try:
            with open(summary_path, "a", encoding="utf-8") as f:
                f.write("Training epochs: {0} / {1}\n".format(current_epoch, max_epochs))
                f.write("Training times (exclude making dataloader): {0} hours {1} minutes {2} seconds\n".format(h, m, s))
        except Exception as e:
            self.log.info("Modify Model summary file incompletely")
            self.err_log.error("Modify Model summary file incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Modify Model summary file completely")
