"""
RSR AutoEncoder 에 사용할 손실 함수.

https://github.com/marrrcin/rsrlayer-pytorch 참고
"""
import torch
from torch import nn


class L2p_Loss(nn.Module):
    """
    Description:
        Reconstruction loss에 사용.
        input 값과 output 값 간의 차이.
        sigma{ L2norm{ (y - y_hat) } }
    """
    def __init__(self, p=1.0):
        """
        Description:
            L2p Loss fuction init.
        Args:
            p: float, L_p norm 값에 제곱할 수
        Returns:
            None
        """
        super().__init__()
        self.p = p

    def forward(self, y_hat, y):
        """
        Description:
            L2p Loss 값 계산.
        Args:
            y_hat: tensor, model output, (batch_size, input_features)
            y: tensor, model input, (batch_size, input_features)
        Returns:
            L2p Loss: tensor, L2p loss 결과
        """
        return torch.sum(torch.pow(torch.norm(y - y_hat, p=2), self.p))


class RSRLoss(nn.Module):
    """
    Description:
        RSR loss로 Projection Loss와 PCA Loss의 합.
        A: orthogonal initial, (batch_size, d, D)
        Z: Encoder result
        Z_hat: A * Z
        Projection Loss: L2norm{ (A * A_transpose - I) }
        PCA Loss: sigma{ L2norm{ (Z - A_transpose * Z_hat) } }
           -> Z_hat = A * Z
        RSR loss: lambda1 * PCA Loss + lambda2 * Projection Loss
    """
    def __init__(self, lambda1, lambda2, d, D):
        """
        Description:
            RSR Loss function init.
        Args:
            lambda1: float, PCA Loss의 추가 가중치
            lambda2: float, Projection Loss의 추가 가중치
            d: int, RSR layer의 output units
            D: int, RSR layer의 input units
        Returns:
            None
        """
        super().__init__()
        self.lambda1 = lambda1
        self.lambda2 = lambda2
        self.d = d
        self.D = D
        # Not parameter. Save in state_dict, but do not proceed with backpropagation
        self.register_buffer("Id", torch.eye(d))

    def forward(self, z, A):
        """
        Description:
            RSR Loss 값 계산
        Args:
            z: tensor, Encoder result
            A: tensor, RSR layer의 orthogonal initial, (batch_size, d, D)
        Returns:
            RSR loss: tensor, lambda1 * PCA Loss + lambda2 * Projection Loss
        """
        z_hat = A @ z.view(z.size(0), self.D, 1)   # Same as RSR layer forward
        AtAz = (A.T @ z_hat).squeeze(2)
        term1 = torch.sum(torch.norm(z - AtAz, p=2))
        term2 = torch.norm(A @ A.T - self.Id, p=2) ** 2
        return self.lambda1 * term1 + self.lambda2 * term2
