import os
import time
import json
from collections import defaultdict
import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
from pytorch_lightning import Trainer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import precision_recall_curve
from scipy.stats import hmean

from utils.log_module import print_elapsed_time

import warnings
warnings.filterwarnings(action="ignore")


class TestModel(object):
    """모델 테스트."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            Pytorch-lightning 이용한 테스트.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(TestModel, self).__init__()
        self.cfg_prep = cfg.PREPROCESS.PREP
        cfg = cfg.TEST
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.load_model_path = cfg.LOAD_MODEL_PATH
        self.test_accel = cfg.TEST_ACCEL
        self.plot_y = cfg.PLOT_Y
        self.label_col = cfg.LABEL_COL
        self.color_col = cfg.COLOR_COL
        self.size_col = cfg.SIZE_COL
        self.normal_color = cfg.NORMAL_COLOR
        self.normal_size = cfg.NORMAL_SIZE
        self.abnormal_color = cfg.ABNORMAL_COLOR
        self.abnormal_size = cfg.ABNORMAL_SIZE
        self.scatter_marker = cfg.SCATTER_MARKER
        self.title_fontsize = cfg.TITLE_FONTSIZE
        self.label_fontsize = cfg.LABEL_FONTSIZE
        self.plot_dpi = cfg.PLOT_DPI
        self.plot_filename = cfg.PLOT_FILENAME
        
        self.USE_ERRCOLS = cfg.USE_ERRCOLS
        self.pr_curve_filename = cfg.PR_CURVE_FILENAME
        self.metrics_filename = cfg.METRICS_FILENAME
        
        # 추가 생성 변수
        self.plot_dir = os.path.dirname(cfg.LOAD_MODEL_PATH)
        self.plot_figsize = tuple(cfg.PLOT_FIGSIZE)

    def __call__(self,
                 model,
                 test_input_df,
                 test_label,
                 test_dl,
                 device):
        """
        Description:
            Model, Dataloader를 이용하여 테스트 진행.
            모델 불러온 뒤, 테스트 결과 로그 생성.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            test_input_df: dataframe, 테스트용 입력 데이터
            test_label: numpy array, 테스트용 입력 데이터의 label
            test_dl: dataloader, 테스트 데이터 dataloader
            device: torch.device, 할당된 장치
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to Test model")
        
        # Load model
        self.load_model_state(model)
        
        # Make trainer & Test model
        trainer = self.make_trainer(device)
        self.test_model(trainer, model, test_dl)
        
        # Predict model & Make plots
        results = self.predict_model(trainer, model, test_dl)
        yn_cols, ord_cols, num_cols, ctgy_df_cols = self.arrange_cols(test_input_df)
        plot_df = self.make_plot_df(
            test_input_df,
            test_label,
            results,
            yn_cols,
            ord_cols,
            num_cols,
            ctgy_df_cols
        )
        
        for USE_ERRCOL in self.plot_y:
            self.log.info("--- {0}".format(USE_ERRCOL))
            # Compute Threshold using P-R Curve
            pr, rc, f1, thr = self.compute_pr_curve(plot_df, USE_ERRCOL)
            THRESHOLD = self.extract_thr(f1, thr)
            if USE_ERRCOL in self.USE_ERRCOLS:
                # Compute Confusion matrix using Threshold
                TP, FN, FP, TN = self.compute_cfm(plot_df, USE_ERRCOL, THRESHOLD)
                metrics_dict = self.compute_metrics(TP, FN, FP, TN)
                metrics_dict["THRESHOLD"] = round(THRESHOLD, 5)
                # Save files
                self.save_plots(plot_df, USE_ERRCOL, THRESHOLD)
                self.save_pr_curve(pr, rc, f1, thr, USE_ERRCOL)
                self.save_metrics(metrics_dict, USE_ERRCOL)
            else:
                self.save_plots(plot_df, USE_ERRCOL, THRESHOLD)

        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to Test model: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def load_model_state(self, model):
        """
        Description:
            학습된 모델 불러오기.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
        Returns:
            None
        """
        model.load_state_dict(torch.load(self.load_model_path))
        model.eval()   # inference mode
        net_name = model.ae.__class__.__name__
        model_ver = "_".join(self.load_model_path.split("/")[-2].split("_")[1:])
        self.log.info("Model's network version: {0}".format(net_name))
        self.log.info("Model version: {0}".format(model_ver))

    def make_trainer(self, device):
        """
        Description:
            Pytorch-lightning의 trainer 생성.
        Args:
            device: torch.device, 할당된 장치
        Returns:
            trainer: Trainer, Pytorch-lightning의 trainer
        """
        trainer = Trainer(
            accelerator=self.test_accel,
            devices=[device.index],
            enable_checkpointing=False,
            logger=False
        )
        return trainer

    def test_model(self,
                   trainer,
                   model, 
                   test_dl):
        """
        Description:
            테스트 진행.
        Args:
            trainer: Trainer, Pytorch-lightning의 trainer
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            test_dl: dataloader, 테스트 데이터 dataloader
        Returns:
            None
        """
        trainer.test(model, test_dl)

    def predict_model(self,
                      trainer,
                      model, 
                      pred_dl):
        """
        Description:
            추론 진행.
        Args:
            trainer: Trainer, Pytorch-lightning의 trainer
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            pred_dl: dataloader, 추론 데이터 dataloader
        Returns:
            results: numpy array, 추론 결과 raw 데이터
        """
        results = trainer.predict(model, pred_dl)
        results = np.concatenate(results, axis=0)
        return results
    
    def arrange_cols(self, test_input_df):
        """
        Description:
            입력 데이터 칼럼 분류별 정리.
        Args:
            trainer: Trainer, Pytorch-lightning의 trainer
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            pred_dl: dataloader, 추론 데이터 dataloader
        Returns:
            yn_cols: list, Y, N인 범주형 항목 칼럼명 리스트
            ord_cols: list, 순서형 범주형 항목 칼럼명 리스트
            num_cols: list, 수치형 항목 칼럼명 리스트
            ctgy_df_cols: dict, 범주형 항목 딕셔너리
                - key: Binary encoding 이전 칼럼
                - value: Binary encoding 이후 칼럼 (뒤에 _숫자 붙음)
        """
        yn_cols = self.cfg_prep.YN_COLS
        ord_cols = self.cfg_prep.ORD_COLS
        num_cols = self.cfg_prep.NUM_QUANTILE_COLS
        ctgy_df_cols = defaultdict(list)
        for ctgy_col in self.cfg_prep.CTGY_COLS:
            ctgy_col_splits = [col for col in test_input_df.columns if col.startswith(ctgy_col)]
            ctgy_df_cols[ctgy_col].extend(ctgy_col_splits)
        return yn_cols, ord_cols, num_cols, ctgy_df_cols
    
    def make_plot_df(self,
                     test_input_df,
                     test_label,
                     results,
                     yn_cols,
                     ord_cols,
                     num_cols,
                     ctgy_df_cols):
        """
        Description:
            테스트 데이터를 이용하여 그래프 확인용 dataframe 생성.
            모델 input 및 output 차이 그래프용 데이터 생성.
                - Categorical 항목은 Binary encoding 수만큼 1/n 비율 씩만 반영
                - yn_cols, ord_cols, num_cols 는 그대로 사용
            정상 데이터는 'Blue', 비정상 데이터는 'Red'로 설정.
        Args:
            test_input_df: dataframe, 테스트용 입력 데이터
            test_label: numpy array, 테스트용 입력 데이터의 label
            results: numpy array, 추론 결과 데이터
            yn_cols: list, Y, N인 범주형 항목 칼럼명 리스트
            ord_cols: list, 순서형 범주형 항목 칼럼명 리스트
            num_cols: list, 수치형 항목 칼럼명 리스트
            ctgy_df_cols: dict, 범주형 항목 딕셔너리
                - key: Binary encoding 이전 칼럼
                - value: Binary encoding 이후 칼럼 (뒤에 _숫자 붙음)
        Returns:
            plot_df: dataframe, 그래프 확인용 dataframe
        """
        result_df = pd.DataFrame(results, columns=test_input_df.columns)

        # 단순 차이 계산 (Weight 반영)
        diff = list()
        passthrough_cols = yn_cols + ord_cols + num_cols
        diff.append(test_input_df[passthrough_cols] - result_df[passthrough_cols])
        for _, ctgy_df_col in ctgy_df_cols.items():
            diff.append((test_input_df[ctgy_df_col] - result_df[ctgy_df_col]) / len(ctgy_df_col))

        # Plot할 부분 추출하여 계산
        test_input = test_input_df.values
        diff = pd.concat(diff, axis=1).values
        l1_norm = np.linalg.norm(diff, ord=1, axis=1)
        l2_norm = np.linalg.norm(diff, ord=2, axis=1)
        cos_sim = np.diag(cosine_similarity(test_input, results))   # 전체 cos_sim 구하고 대각성분만 추출
        
        fft_abs = abs(np.fft.fft(diff))   # 푸리에 변환
        fft_abs_l1_norm = np.linalg.norm(fft_abs, ord=1, axis=1)
        fft_abs_l2_norm = np.linalg.norm(fft_abs, ord=2, axis=1)
        
        # Dataframe 생성
        plot_y_values = [
            l1_norm,
            l2_norm,
            cos_sim,
            fft_abs_l1_norm,
            fft_abs_l2_norm
        ]
        assert len(self.plot_y) == len(plot_y_values), "Check same length of config's PLOT_Y & definition's plot_y_values"
        plot_dict = {
            k: v
            for k, v
            in zip(self.plot_y, plot_y_values)
        }
        plot_dict[self.label_col] = test_label
        plot_df = pd.DataFrame(plot_dict)
        
        # 크기, 색깔 설정
        color_lambda = lambda row: self.abnormal_color if row[self.label_col] == 1 else self.normal_color
        size_lambda = lambda row: self.abnormal_size if row[self.label_col] == 1 else self.normal_size
        plot_df[self.color_col] = plot_df.apply(color_lambda, axis=1)
        plot_df[self.size_col] = plot_df.apply(size_lambda, axis=1)
        
        return plot_df

    def compute_pr_curve(self, plot_df, USE_ERRCOL):
        """
        Description:
            P-R Curve 계산.
        Args:
            plot_df: dataframe, 그래프 확인용 dataframe
            USE_ERRCOL: str, 계산한 에러 계산식 종류
                - l1_norm
                - l2_norm
                - cos_sim
                - fft_abs_l1_norm
                - fft_abs_l2_norm
        Returns:
            pr: numpy array, Precision 배열
            rc: numpy array, Recall 배열
            f1: numpy array, F1 score 배열
            thr: numpy array, Thresholds 배열
        """
        assert USE_ERRCOL in self.plot_y, "Just use USE_ERRCOL in {0}".format(self.plot_y)
        pr, rc, thr = precision_recall_curve(
            plot_df["label"], plot_df[USE_ERRCOL], pos_label=1
        )
        f1 = hmean(
            np.concatenate((pr[:, None], rc[:, None]), axis=1),
            axis=1
        )
        return pr, rc, f1, thr
    
    def extract_thr(self, f1, thr):
        """
        Description:
            P-R Curve로부터 Threshold 값 추출.
            F1 score가 최대인 지점의 Threshold 이용.
        Args:
            f1: numpy array, F1 score 배열
            thr: numpy array, Thresholds 배열
        Returns:
            THRESHOLD: float, Confusion Matrix 만들 때 사용할 Threshold
        """
        THRESHOLD = thr[np.argmax(f1)]
        self.log.info("Using Threshold: {0:.5f}".format(THRESHOLD))
        return THRESHOLD
    
    def compute_cfm(self, plot_df, USE_ERRCOL, THRESHOLD):
        """
        Description:
            Threshold 이용하여 Confusion Matrix 계산.
        Args:
            plot_df: dataframe, 그래프 확인용 dataframe
            USE_ERRCOL: str, 계산한 에러 계산식 종류
            THRESHOLD: float, Confusion Matrix 만들 때 사용할 Threshold
        Returns:
            TP: int, True Positive
            FN: int, False Negative
            FP: int, False Positive
            TN: int, True Negative
        """
        use_data = plot_df[USE_ERRCOL]
        label = plot_df["label"]
        TP = len(use_data[(use_data >= THRESHOLD) & (label == 1)])
        FN = len(use_data[(use_data < THRESHOLD) & (label == 1)])
        FP = len(use_data[(use_data >= THRESHOLD) & (label == 0)])
        TN = len(use_data[(use_data < THRESHOLD) & (label == 0)])
        return TP, FN, FP, TN
    
    def compute_metrics(self, TP, FN, FP, TN):
        """
        Description:
            Confusion Matrix로 Metrics 계산.
        Args:
            TP: int, True Positive
            FN: int, False Negative
            FP: int, False Positive
            TN: int, True Negative
        Returns:
            metrics_dict: dict, CFM과 Metrics
        """
        # Metrics 계산
        Accuracy = (TP + TN) / (TP + FN + FP + TN)
        Precision = (TP) / (TP + FP)
        Recall = (TP) / (TP + FN)
        F1score = 2 * Precision * Recall / (Precision + Recall)
        Specificity = (TN) / (FP + TN)
        FalsePositiveRate = 1 - Specificity
        BalancedAccuracy = (Recall + Specificity) / 2
        # CFM과 Metrics 저장하기 위해 딕셔너리로 변환
        metrics_dict = dict()
        metrics_dict["TP"] = TP
        metrics_dict["FN"] = FN
        metrics_dict["FP"] = FP
        metrics_dict["TN"] = TN
        metrics_dict["Accuracy"] = round(Accuracy, 5)
        metrics_dict["Precision"] = round(Precision, 5)
        metrics_dict["Recall"] = round(Recall, 5)
        metrics_dict["F1score"] = round(F1score, 5)
        metrics_dict["Specificity"] = round(Specificity, 5)
        metrics_dict["FalsePositiveRate"] = round(FalsePositiveRate, 5)
        metrics_dict["BalancedAccuracy"] = round(BalancedAccuracy, 5)
        self.log.info("Confusion Matrix & Metrics:")
        for k, v in metrics_dict.items():
            self.log.info("   {0}: {1}".format(k, v))
        return metrics_dict
    
    def save_plots(self, plot_df, USE_ERRCOL, THRESHOLD):
        """
        Description:
            모델 input 및 output 차이 그래프 생성 후 저장.
        Args:
            plot_df: dataframe, 그래프 확인용 dataframe
            USE_ERRCOL: str, 계산한 에러 계산식 종류
            THRESHOLD: float, Confusion Matrix 만들 때 사용할 Threshold
        Returns:
            None
        """
        plt.cla()
        plt.clf()

        fig = plt.figure(figsize=self.plot_figsize)
        ax = fig.add_subplot(111)

        ax.scatter(
            x=plot_df.index,
            y=plot_df[USE_ERRCOL],
            marker=self.scatter_marker,
            s=plot_df[self.size_col],
            c=plot_df[self.color_col]
        )
        ax.plot(
            plot_df.index,
            np.ones(len(plot_df)) * THRESHOLD,
            "r--",
            label="Threshold: {0:.5f}".format(THRESHOLD)
        )
        ax.legend(loc="upper right")
        
        plot_name = USE_ERRCOL.upper()
        plt.title("Scatter Plot of Chemical Risk", fontsize=self.title_fontsize)
        plt.xlabel("Business Establishment", fontsize=self.label_fontsize)
        plt.ylabel("{0} from RSRAE".format(plot_name), fontsize=self.label_fontsize)
        
        # 이미지 파일로 저장
        save_path = os.path.join(self.plot_dir, self.plot_filename.format(plot_name))
        try:
            plt.savefig(save_path, dpi=self.plot_dpi)
        except Exception as e:
            self.log.info("Save Plot image [{0}] incompletely".format(plot_name))
            self.err_log.error("Save Plot image [{0}] incompletely: {1}".format(plot_name, e))
            raise e
        else:
            self.log.info("Save Plot image [{0}] completely".format(plot_name))
    
    def save_pr_curve(self, pr, rc, f1, thr, USE_ERRCOL):
        """
        Description:
            P-R Curve 그래프 도식 후 저장.
        Args:
            pr: numpy array, Precision 배열
            rc: numpy array, Recall 배열
            f1: numpy array, F1 score 배열
            thr: numpy array, Thresholds 배열
            USE_ERRCOL: str, 계산한 에러 계산식 종류
        Returns:
            None
        """
        plt.cla()
        plt.clf()

        fig = plt.figure(figsize=self.plot_figsize)
        
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        
        thr_plot = np.zeros_like(pr)
        thr_plot[1:] = thr
        ax1.plot(thr_plot, pr, "r--", label="Precision")
        ax1.plot(thr_plot, rc, "b--", label="Recall")
        ax1.plot(thr_plot, f1, "g--", label="F1score")
        ax1.legend(loc="upper right")
        
        ax2.plot(rc, pr)
        
        ax1.set_title("Precison & Recall", fontsize=self.label_fontsize-2)
        ax1.set_xlabel("Threshod", fontsize=self.label_fontsize-4)
        ax1.set_ylabel("Score", fontsize=self.label_fontsize-4)

        ax2.set_title("P-R Curve", fontsize=self.label_fontsize-2)
        ax2.set_xlabel("Recall", fontsize=self.label_fontsize-4)
        ax2.set_ylabel("Precison", fontsize=self.label_fontsize-4)
        
        # 이미지 파일로 저장
        USE_ERRCOL = USE_ERRCOL.upper()
        save_path = os.path.join(
            self.plot_dir, self.pr_curve_filename.format(USE_ERRCOL)
        )
        try:
            plt.savefig(save_path, dpi=self.plot_dpi)
        except Exception as e:
            self.log.info("Save P-R Curve image [{0}] incompletely".format(USE_ERRCOL))
            self.err_log.error("Save P-R Curve image [{0}] incompletely: {1}".format(USE_ERRCOL, e))
            raise e
        else:
            self.log.info("Save P-R Curve image [{0}] completely".format(USE_ERRCOL))
    
    def save_metrics(self,
                     metrics_dict,
                     USE_ERRCOL,
                     encoding="utf-8",
                     indent="\t"):
        """
        Description:
            Metrics json 저장 (utf-8 인코딩으로 저장).
        Args:
            metrics_dict: dict, CFM과 Metrics
            USE_ERRCOL: str, 계산한 에러 계산식 종류
            encoding: str, json 파일 인코딩 방식 (default="utf-8")
            indent: str, json 파일의 indent (default="\t")
        Returns:
            None
        """
        USE_ERRCOL = USE_ERRCOL.upper()
        save_path = os.path.join(
            self.plot_dir, self.metrics_filename.format(USE_ERRCOL)
        )
        try:
            with open(save_path, "w", encoding=encoding) as p:
                json.dump(metrics_dict, p, ensure_ascii=False, indent=indent)
        except Exception as e:
            self.log.info("Save Metrics json file [{0}] incompletely".format(USE_ERRCOL))
            self.err_log.error("Save Metrics json file [{0}] incompletely: {1}".format(USE_ERRCOL, e))
            raise e
        else:
            self.log.info("Save Metrics json file [{0}] completely".format(USE_ERRCOL))
