import time
import numpy as np
import pandas as pd
import torch
from pytorch_lightning import Trainer

from utils.code_utils import unpack
from utils.log_module import print_elapsed_time

import warnings
warnings.filterwarnings(action="ignore")


class PredictModel(object):
    """모델 추론 및 결과 생성."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            Pytorch-lightning 이용한 테스트.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(PredictModel, self).__init__()
        self.cfg_prep = cfg.PREPROCESS
        cfg = cfg.PREDICT
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.load_model_path = cfg.LOAD_MODEL_PATH
        self.pred_accel = cfg.PRED_ACCEL
        api_ipt_cvt = unpack(cfg.API_IPT_CVT)
        self.api_ipt_cvt_cols = {k: v[0] for k, v in api_ipt_cvt.items()}
        self.api_ipt_cols_type = {v[0]: v[1] for _, v in api_ipt_cvt.items()}

    def __call__(
        self, model, trainer, pred_input_df
        ):
        """
        Description:
            Model, 입력된 데이터를 이용하여 추론 진행.
            모델 불러온 뒤, 전처리 후 추론 결과 생성.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            pred_input_df: dataframe, 추론용 입력 데이터
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to Predict model")
        
        # Preprocess data
        pred_dl = None
        
        # Make trainer & Predict model
        results = self.predict_model(trainer, model, pred_dl)
        
        # Postprocess & Save results
        

        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to Predict model: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def load_model_state(self, model):
        """
        Description:
            학습된 모델 불러오기.
        Args:
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
        Returns:
            None
        """
        model.load_state_dict(torch.load(self.load_model_path))
        model.eval()   # inference mode
        net_name = model.ae.__class__.__name__
        model_ver = "_".join(self.load_model_path.split("/")[-2].split("_")[1:])
        self.log.info("Model's network version: {0}".format(net_name))
        self.log.info("Model version: {0}".format(model_ver))
    
    def make_trainer(self, device):
        """
        Description:
            Pytorch-lightning의 trainer 생성.
        Args:
            device: torch.device, 할당된 장치
        Returns:
            trainer: Trainer, Pytorch-lightning의 trainer
        """
        trainer = Trainer(
            accelerator=self.test_accel,
            devices=[device.index],
            enable_checkpointing=False,
            logger=False
        )
        return trainer
    
    def predict_model(
            self, trainer, model, pred_dl
        ):
        """
        Description:
            추론 진행.
        Args:
            trainer: Trainer, Pytorch-lightning의 trainer
            model: LightningModule, Pytorch-lightning의 trainer로 생성할 모델
            pred_dl: dataloader, 추론 데이터 dataloader
        Returns:
            results: numpy array, 추론 결과 raw 데이터
        """
        results = trainer.predict(model, pred_dl)
        results = np.concatenate(results, axis=0)
        return results
