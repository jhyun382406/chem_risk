"""
RSR AutoEncoder 네트워크 구성.
RSR Layer: Robust Subspace Recovery Layer

https://github.com/marrrcin/rsrlayer-pytorch 참고
"""
from easydict import EasyDict as edict
import torch
from torch import nn


class RSRLayer(nn.Module):
    """
    Description:
        AutoEncoder에서 latent vector에 반영.
    """
    def __init__(self, d: int, D: int):
        """
        Description:
            RSR Layer init.
        Args:
            d: int, RSR layer의 output units
            D: int, RSR layer의 input units
        Returns:
            None
        """
        super().__init__()
        self.d = d
        self.D = D
        self.A = nn.Parameter(torch.nn.init.orthogonal_(torch.empty(d, D)))

    def forward(self, z):
        """
        Description:
            RSR Layer 출력결과.
        Args:
            z: tensor, Encoder result, (batch_size, D)
        Returns:
            z_hat: tensor, RSR result, (batch_size, d)
        """
        # z is the output from the encoder
        z_hat = self.A @ z.view(z.size(0), self.D, 1)
        return z_hat.squeeze(2)


class RSRAutoEncoderV1(nn.Module):
    """
    Description:
        RSR AutoEncoder에서 기본 형태.
    """
    def __init__(self, cfg_hparams, add_hparams=None):
        """
        Description:
            RSR AutoEncoder init.
            Encoder와 Decoder 는 거울구조, >< 모양.
        Args:
            cfg_hparams: Namedtuple, YAML 파일에서 읽어온 설정 중 hyperparameters
            add_hparams: dict, Dataloader에서 생성된 hparams에 추가할 딕셔너리 (default=None)
        Returns:
            None
        """
        super().__init__()
        # Setting hyperparameters
        self.hparams = self.make_hparams(cfg_hparams, add_hparams)
        
        # Put your encoder network here, remember about the output D-dimension
        self.encoder = nn.Sequential(
            nn.Linear(self.hparams.e_units[0], self.hparams.e_units[1]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.e_units[1], self.hparams.e_units[2]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.e_units[2], self.hparams.e_units[3]),
        )

        self.rsr = RSRLayer(self.hparams.d, self.hparams.D)

        # Put your decoder network here, rembember about the input d-dimension
        self.decoder = nn.Sequential(
            nn.Linear(self.hparams.d_units[0], self.hparams.d_units[1]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.d_units[1], self.hparams.d_units[2]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.d_units[2], self.hparams.d_units[3]),
        )

    def forward(self, x):
        """
        Description:
            RSR Layer 출력결과.
            Encoder -> RSR -> Decoder 순.
                        └-> A 가져옴
        Args:
            x: tensor, Encoder input, (batch_size, input_features)
        Returns:
            enc: tensor, Encoder result, (batch_size, D)
            dec: tensor, Decoder result, (batch_size, input_features)
            latent: tensor, RSR result, (batch_size, d)
            A: tensor, orthogonal initial, (batch_size, d, D)
        """
        enc = self.encoder(x)   # obtain the embedding from the encoder
        latent = self.rsr(enc)   # RSR manifold
        dec = self.decoder(latent)   # obtain the representation in the input space
        return enc, dec, latent, self.rsr.A
    
    def make_hparams(self, cfg, add_hparams):
        """
        Description:
            Trainer에 반영할 하이퍼 파라미터 생성.
            YAML 파일 외에도 계산이 필요한 부분이 있어 별도 생성.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            add_hparams: dict, Dataloader에서 생성된 hparams에 추가할 딕셔너리
        Returns:
            hparams: easyDict, 하이퍼 파라미터 딕셔너리
        """
        cfg_cb = cfg.CALLBACKS
        if add_hparams is None:
            input_dim = cfg.input_dim
            steps_per_epoch = cfg.steps_per_epoch
        else:
            input_dim = add_hparams["input_dim"]
            steps_per_epoch = add_hparams["steps_per_epoch"]
        e_units = [
            input_dim,
            input_dim // 2,
            input_dim // 4,
            cfg.NETWORK.ENCODER_LAST
        ]   # Encoder units
        d_units = [
            cfg.NETWORK.LATENT_UNIT,
            cfg.NETWORK.ENCODER_LAST,
            input_dim // 2,
            input_dim
        ]   # Decoder units
        hparams = dict(
            random_seed = cfg.RANDOM_SEED,
            dl_workers = cfg.DL_WORKERS,
            batch_size = cfg.BATCH_SIZE,
            epochs = cfg.EPOCHS,
            input_dim = input_dim,
            d = cfg.NETWORK.LATENT_UNIT,
            D = cfg.NETWORK.ENCODER_LAST,
            e_units = e_units,   # Encoder units
            d_units = d_units,   # Decoder units
            lr = cfg.LR_SCHEDULER.LR,   # Peak learning rate
            div_factor = cfg.LR_SCHEDULER.DIV_FACTOR,   # Using LR Scheduler
            steps_per_epoch = steps_per_epoch,
            lambda1 = cfg.RSR_LOSS.LAMBDA1,   # Coefficient PCA Loss
            lambda2 = cfg.RSR_LOSS.LAMBDA2,   # Coefficient Projection Loss
            ckpt_monitor = cfg_cb.CKPT.MONITOR,
            ckpt_mode = cfg_cb.CKPT.MODE,
            ckpt_top_k = cfg_cb.CKPT.TOP_K,
            es_monitor = cfg_cb.EARLYSTOP.MONITOR,
            es_min_delta = cfg_cb.EARLYSTOP.MIN_DELTA,
            es_patience = cfg_cb.EARLYSTOP.PATIENCE,
            es_mode = cfg_cb.EARLYSTOP.MODE,
            lr_log_interval = cfg_cb.LR_MONITOR.INTERVAL,
            csv_logger_name = cfg_cb.CSV_LOGGER.NAME
        )
        return edict(hparams)


class RSRAutoEncoderV2(nn.Module):
    """
    Description:
        RSR AutoEncoder에서 기본 형태.
        Layer의 hidden state 조정
    """
    def __init__(self, cfg_hparams, add_hparams=None):
        """
        Description:
            RSR AutoEncoder init.
            Encoder와 Decoder 는 거울구조, >< 모양.
        Args:
            cfg_hparams: Namedtuple, YAML 파일에서 읽어온 설정 중 hyperparameters
            add_hparams: dict, Dataloader에서 생성된 hparams에 추가할 딕셔너리 (default=None)
        Returns:
            None
        """
        super().__init__()
        # Setting hyperparameters
        self.hparams = self.make_hparams(cfg_hparams, add_hparams)
        
        # Put your encoder network here, remember about the output D-dimension
        self.encoder = nn.Sequential(
            nn.Linear(self.hparams.e_units[0], self.hparams.e_units[1]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.e_units[1], self.hparams.e_units[2]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.e_units[2], self.hparams.e_units[3]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.e_units[3], self.hparams.e_units[4]),
        )

        self.rsr = RSRLayer(self.hparams.d, self.hparams.D)

        # Put your decoder network here, rembember about the input d-dimension
        self.decoder = nn.Sequential(
            nn.Linear(self.hparams.d_units[0], self.hparams.d_units[1]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.d_units[1], self.hparams.d_units[2]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.d_units[2], self.hparams.d_units[3]),
            nn.LeakyReLU(),
            nn.Linear(self.hparams.d_units[3], self.hparams.d_units[4]),
        )

    def forward(self, x):
        """
        Description:
            RSR Layer 출력결과.
            Encoder -> RSR -> Decoder 순.
                        └-> A 가져옴
        Args:
            x: tensor, Encoder input, (batch_size, input_features)
        Returns:
            enc: tensor, Encoder result, (batch_size, D)
            dec: tensor, Decoder result, (batch_size, input_features)
            latent: tensor, RSR result, (batch_size, d)
            A: tensor, orthogonal initial, (batch_size, d, D)
        """
        enc = self.encoder(x)   # obtain the embedding from the encoder
        latent = self.rsr(enc)   # RSR manifold
        dec = self.decoder(latent)   # obtain the representation in the input space
        return enc, dec, latent, self.rsr.A
    
    def make_hparams(self, cfg, add_hparams):
        """
        Description:
            Trainer에 반영할 하이퍼 파라미터 생성.
            YAML 파일 외에도 계산이 필요한 부분이 있어 별도 생성.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            add_hparams: dict, Dataloader에서 생성된 hparams에 추가할 딕셔너리
        Returns:
            hparams: easyDict, 하이퍼 파라미터 딕셔너리
        """
        cfg_cb = cfg.CALLBACKS
        if add_hparams is None:
            input_dim = cfg.input_dim
            steps_per_epoch = cfg.steps_per_epoch
        else:
            input_dim = add_hparams["input_dim"]
            steps_per_epoch = add_hparams["steps_per_epoch"]
        e_units = [
            input_dim,
            input_dim * 4,
            input_dim * 2,
            input_dim,
            cfg.NETWORK.ENCODER_LAST
        ]   # Encoder units
        d_units = [
            cfg.NETWORK.LATENT_UNIT,
            cfg.NETWORK.ENCODER_LAST,
            input_dim * 2,
            input_dim * 4,
            input_dim
        ]   # Decoder units
        hparams = dict(
            random_seed = cfg.RANDOM_SEED,
            dl_workers = cfg.DL_WORKERS,
            batch_size = cfg.BATCH_SIZE,
            epochs = cfg.EPOCHS,
            input_dim = input_dim,
            d = cfg.NETWORK.LATENT_UNIT,
            D = cfg.NETWORK.ENCODER_LAST,
            e_units = e_units,   # Encoder units
            d_units = d_units,   # Decoder units
            lr = cfg.LR_SCHEDULER.LR,   # Peak learning rate
            div_factor = cfg.LR_SCHEDULER.DIV_FACTOR,   # Using LR Scheduler
            steps_per_epoch = steps_per_epoch,
            lambda1 = cfg.RSR_LOSS.LAMBDA1,   # Coefficient PCA Loss
            lambda2 = cfg.RSR_LOSS.LAMBDA2,   # Coefficient Projection Loss
            ckpt_monitor = cfg_cb.CKPT.MONITOR,
            ckpt_mode = cfg_cb.CKPT.MODE,
            ckpt_top_k = cfg_cb.CKPT.TOP_K,
            es_monitor = cfg_cb.EARLYSTOP.MONITOR,
            es_min_delta = cfg_cb.EARLYSTOP.MIN_DELTA,
            es_patience = cfg_cb.EARLYSTOP.PATIENCE,
            es_mode = cfg_cb.EARLYSTOP.MODE,
            lr_log_interval = cfg_cb.LR_MONITOR.INTERVAL,
            csv_logger_name = cfg_cb.CSV_LOGGER.NAME
        )
        return edict(hparams)


class RSRAutoEncoderV3(nn.Module):
    """
    Description:
        RSR AutoEncoder에서 기본 형태.
        Skip connection 추가(concatenate).
    """
    def __init__(self, cfg_hparams, add_hparams=None):
        """
        Description:
            RSR AutoEncoder init.
            Encoder와 Decoder 는 거울구조, >< 모양.
        Args:
            cfg_hparams: Namedtuple, YAML 파일에서 읽어온 설정 중 hyperparameters
            add_hparams: dict, Dataloader에서 생성된 hparams에 추가할 딕셔너리 (default=None)
        Returns:
            None
        """
        super().__init__()
        # Setting hyperparameters
        self.hparams = self.make_hparams(cfg_hparams, add_hparams)
        
        # Put your encoder network here, remember about the output D-dimension
        self.e1 = self.linear_lrelu(self.hparams.e_units[0], self.hparams.e_units[1])
        self.e2 = self.linear_lrelu(self.hparams.e_units[1], self.hparams.e_units[2])
        self.e3 = self.linear_lrelu(self.hparams.e_units[2], self.hparams.e_units[3])
        self.e4 = nn.Linear(self.hparams.e_units[3], self.hparams.e_units[4])

        self.rsr = RSRLayer(self.hparams.d, self.hparams.D)

        # Put your decoder network here, rembember about the input d-dimension
        self.d4 = self.linear_lrelu(self.hparams.d_units[0], self.hparams.d_units[1])
        self.d3 = self.linear_lrelu(self.hparams.d_units[1] * 2, self.hparams.d_units[2])
        self.d2 = self.linear_lrelu(self.hparams.d_units[2] * 2, self.hparams.d_units[3])
        self.d1 = self.linear_lrelu(self.hparams.d_units[3] * 2, self.hparams.d_units[4])
        self.d0 = nn.Linear(self.hparams.d_units[4] * 2, self.hparams.d_units[5])

    def forward(self, x):
        """
        Description:
            RSR Layer 출력결과.
            Encoder -> RSR -> Decoder 순.
                        └-> A 가져옴
        Args:
            x: tensor, Encoder input, (batch_size, input_features)
        Returns:
            enc: tensor, Encoder result, (batch_size, D)
            dec: tensor, Decoder result, (batch_size, input_features)
            latent: tensor, RSR result, (batch_size, d)
            A: tensor, orthogonal initial, (batch_size, d, D)
        """
        # Encoder
        x_e1 = self.e1(x)
        x_e2 = self.e2(x_e1)
        x_e3 = self.e3(x_e2)
        x_e4 = self.e4(x_e3)
        
        # RSR manifold (latent)
        latent = self.rsr(x_e4)
        
        # Decoder adapting Skip conncection (Concatenate)
        x_d4 = self.d4(latent)
        x_d3 = self.d3(torch.concat((x_e4, x_d4), axis=-1))
        x_d2 = self.d2(torch.concat((x_e3, x_d3), axis=-1))
        x_d1 = self.d1(torch.concat((x_e2, x_d2), axis=-1))
        rst = self.d0(torch.concat((x_e1, x_d1), axis=-1))
        
        return x_e4, rst, latent, self.rsr.A
    
    def linear_lrelu(self, in_units, out_units):
        """
        Description:
            Linear -> LeakyRelu 묶음.
        Args:
            in_units: int, Linear layer's input units
            out_units: int, Linear layer's output units
        Returns:
            layer: torch modules, nn.Sequential
        """
        return nn.Sequential(
            nn.Linear(in_units, out_units),
            nn.LeakyReLU()
        )
    
    def make_hparams(self, cfg, add_hparams):
        """
        Description:
            Trainer에 반영할 하이퍼 파라미터 생성.
            YAML 파일 외에도 계산이 필요한 부분이 있어 별도 생성.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            add_hparams: dict, Dataloader에서 생성된 hparams에 추가할 딕셔너리
        Returns:
            hparams: easyDict, 하이퍼 파라미터 딕셔너리
        """
        cfg_cb = cfg.CALLBACKS
        if add_hparams is None:
            input_dim = cfg.input_dim
            steps_per_epoch = cfg.steps_per_epoch
        else:
            input_dim = add_hparams["input_dim"]
            steps_per_epoch = add_hparams["steps_per_epoch"]
        e_units = [
            input_dim,
            input_dim * 4,
            input_dim * 2,
            input_dim,
            cfg.NETWORK.ENCODER_LAST
        ]   # Encoder units
        d_units = [
            cfg.NETWORK.LATENT_UNIT,
            cfg.NETWORK.ENCODER_LAST,
            input_dim,
            input_dim * 2,
            input_dim * 4,
            input_dim
        ]   # Decoder units
        hparams = dict(
            random_seed = cfg.RANDOM_SEED,
            dl_workers = cfg.DL_WORKERS,
            batch_size = cfg.BATCH_SIZE,
            epochs = cfg.EPOCHS,
            input_dim = input_dim,
            d = cfg.NETWORK.LATENT_UNIT,
            D = cfg.NETWORK.ENCODER_LAST,
            e_units = e_units,   # Encoder units
            d_units = d_units,   # Decoder units
            lr = cfg.LR_SCHEDULER.LR,   # Peak learning rate
            div_factor = cfg.LR_SCHEDULER.DIV_FACTOR,   # Using LR Scheduler
            steps_per_epoch = steps_per_epoch,
            lambda1 = cfg.RSR_LOSS.LAMBDA1,   # Coefficient PCA Loss
            lambda2 = cfg.RSR_LOSS.LAMBDA2,   # Coefficient Projection Loss
            ckpt_monitor = cfg_cb.CKPT.MONITOR,
            ckpt_mode = cfg_cb.CKPT.MODE,
            ckpt_top_k = cfg_cb.CKPT.TOP_K,
            es_monitor = cfg_cb.EARLYSTOP.MONITOR,
            es_min_delta = cfg_cb.EARLYSTOP.MIN_DELTA,
            es_patience = cfg_cb.EARLYSTOP.PATIENCE,
            es_mode = cfg_cb.EARLYSTOP.MODE,
            lr_log_interval = cfg_cb.LR_MONITOR.INTERVAL,
            csv_logger_name = cfg_cb.CSV_LOGGER.NAME
        )
        return edict(hparams)


class RSRAutoEncoderV4(nn.Module):
    """
    Description:
        RSR AutoEncoder에서 기본 형태.
        Skip connection 추가(add).
    """
    def __init__(self, cfg_hparams, add_hparams=None):
        """
        Description:
            RSR AutoEncoder init.
            Encoder와 Decoder 는 거울구조, >< 모양.
        Args:
            cfg_hparams: Namedtuple, YAML 파일에서 읽어온 설정 중 hyperparameters
            add_hparams: dict, Dataloader에서 생성된 hparams에 추가할 딕셔너리 (default=None)
        Returns:
            None
        """
        super().__init__()
        # Setting hyperparameters
        self.hparams = self.make_hparams(cfg_hparams, add_hparams)
        
        # Put your encoder network here, remember about the output D-dimension
        self.e1 = self.linear_lrelu(self.hparams.e_units[0], self.hparams.e_units[1])
        self.e2 = self.linear_lrelu(self.hparams.e_units[1], self.hparams.e_units[2])
        self.e3 = self.linear_lrelu(self.hparams.e_units[2], self.hparams.e_units[3])
        self.e4 = nn.Linear(self.hparams.e_units[3], self.hparams.e_units[4])

        self.rsr = RSRLayer(self.hparams.d, self.hparams.D)

        # Put your decoder network here, rembember about the input d-dimension
        self.d4 = self.linear_lrelu(self.hparams.d_units[0], self.hparams.d_units[1])
        self.d3 = self.linear_lrelu(self.hparams.d_units[1], self.hparams.d_units[2])
        self.d2 = self.linear_lrelu(self.hparams.d_units[2], self.hparams.d_units[3])
        self.d1 = self.linear_lrelu(self.hparams.d_units[3], self.hparams.d_units[4])
        self.d0 = nn.Linear(self.hparams.d_units[4], self.hparams.d_units[5])

    def forward(self, x):
        """
        Description:
            RSR Layer 출력결과.
            Encoder -> RSR -> Decoder 순.
                        └-> A 가져옴
        Args:
            x: tensor, Encoder input, (batch_size, input_features)
        Returns:
            enc: tensor, Encoder result, (batch_size, D)
            dec: tensor, Decoder result, (batch_size, input_features)
            latent: tensor, RSR result, (batch_size, d)
            A: tensor, orthogonal initial, (batch_size, d, D)
        """
        # Encoder
        x_e1 = self.e1(x)
        x_e2 = self.e2(x_e1)
        x_e3 = self.e3(x_e2)
        x_e4 = self.e4(x_e3)
        
        # RSR manifold (latent)
        latent = self.rsr(x_e4)
        
        # Decoder adapting Skip conncection (Add)
        x_d4 = self.d4(latent)
        x_d3 = self.d3(torch.add(x_e4, x_d4))
        x_d2 = self.d2(torch.add(x_e3, x_d3))
        x_d1 = self.d1(torch.add(x_e2, x_d2))
        rst = self.d0(torch.add(x_e1, x_d1))
        
        return x_e4, rst, latent, self.rsr.A
    
    def linear_lrelu(self, in_units, out_units):
        """
        Description:
            Linear -> LeakyRelu 묶음.
        Args:
            in_units: int, Linear layer's input units
            out_units: int, Linear layer's output units
        Returns:
            layer: torch modules, nn.Sequential
        """
        return nn.Sequential(
            nn.Linear(in_units, out_units),
            nn.LeakyReLU()
        )
    
    def make_hparams(self, cfg, add_hparams):
        """
        Description:
            Trainer에 반영할 하이퍼 파라미터 생성.
            YAML 파일 외에도 계산이 필요한 부분이 있어 별도 생성.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            add_hparams: dict, Dataloader에서 생성된 hparams에 추가할 딕셔너리
        Returns:
            hparams: easyDict, 하이퍼 파라미터 딕셔너리
        """
        cfg_cb = cfg.CALLBACKS
        if add_hparams is None:
            input_dim = cfg.input_dim
            steps_per_epoch = cfg.steps_per_epoch
        else:
            input_dim = add_hparams["input_dim"]
            steps_per_epoch = add_hparams["steps_per_epoch"]
        e_units = [
            input_dim,
            input_dim * 4,
            input_dim * 2,
            input_dim,
            cfg.NETWORK.ENCODER_LAST
        ]   # Encoder units
        d_units = [
            cfg.NETWORK.LATENT_UNIT,
            cfg.NETWORK.ENCODER_LAST,
            input_dim,
            input_dim * 2,
            input_dim * 4,
            input_dim
        ]   # Decoder units
        hparams = dict(
            random_seed = cfg.RANDOM_SEED,
            dl_workers = cfg.DL_WORKERS,
            batch_size = cfg.BATCH_SIZE,
            epochs = cfg.EPOCHS,
            input_dim = input_dim,
            d = cfg.NETWORK.LATENT_UNIT,
            D = cfg.NETWORK.ENCODER_LAST,
            e_units = e_units,   # Encoder units
            d_units = d_units,   # Decoder units
            lr = cfg.LR_SCHEDULER.LR,   # Peak learning rate
            div_factor = cfg.LR_SCHEDULER.DIV_FACTOR,   # Using LR Scheduler
            steps_per_epoch = steps_per_epoch,
            lambda1 = cfg.RSR_LOSS.LAMBDA1,   # Coefficient PCA Loss
            lambda2 = cfg.RSR_LOSS.LAMBDA2,   # Coefficient Projection Loss
            ckpt_monitor = cfg_cb.CKPT.MONITOR,
            ckpt_mode = cfg_cb.CKPT.MODE,
            ckpt_top_k = cfg_cb.CKPT.TOP_K,
            es_monitor = cfg_cb.EARLYSTOP.MONITOR,
            es_min_delta = cfg_cb.EARLYSTOP.MIN_DELTA,
            es_patience = cfg_cb.EARLYSTOP.PATIENCE,
            es_mode = cfg_cb.EARLYSTOP.MODE,
            lr_log_interval = cfg_cb.LR_MONITOR.INTERVAL,
            csv_logger_name = cfg_cb.CSV_LOGGER.NAME
        )
        return edict(hparams)