"""
공공 데이터에서 1차 추출.
공공 데이터는 공공 데이터 포털(https://www.data.go.kr/)에서 가져옴.

1. 근로복지공단
  - 고용 산재보험 가입 현황 (17~21년)
    https://www.data.go.kr/data/15002150/fileData.do
2. 경기데이터드림
  - 경기도 유해화학물질 취급사업장 현황
    https://data.gg.go.kr/portal/data/service/selectServicePage.do?page=1&rows=10&sortColumn=&sortDirection=&infId=M37YD49AM5UFN6VJM2CZ29567068&infSeq=3
  - 경기도 유해화학물질 사고현황
    https://data.gg.go.kr/portal/data/service/selectServicePage.do?page=1&rows=10&sortColumn=&sortDirection=&infId=2524M2F42782BYTNVU2612259983&infSeq=1
  - 경기도 업종별 사업체 현황 (16~20년)
    https://data.gg.go.kr/portal/data/service/selectServicePage.do?page=1&sortColumn=&sortDirection=&infId=Q5FEF7YLDX69L0Z9A1PL25845033&infSeq=1&searchWord=%EC%97%85%EC%A2%85%EB%B3%84+%EC%82%AC%EC%97%85%EC%B2%B4
  - 경기도 공장등록 현황
    https://data.gg.go.kr/portal/data/service/selectServicePage.do?page=1&rows=10&sortColumn=&sortDirection=&infId=794V0223GQQ9O1P4WAP221637079&infSeq=1&order=&searchWord=%EA%B3%B5%EC%9E%A5%EB%93%B1%EB%A1%9D
"""
import os
import time
import glob
from collections import defaultdict
from itertools import repeat, chain
import pandas as pd
import dask.dataframe as dd

from utils.log_module import print_elapsed_time
from utils.code_utils import convert_cols_type
from utils.df_utils import DataframeUtils

import warnings
warnings.filterwarnings(action="ignore")


class Comwel(object):
    """고용 산재보험 가입 현황."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            고용 산재보험 가입 현황 파일 불러온 뒤 필요 칼럼만 저장.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(Comwel, self).__init__()
        cfg = cfg.COMWEL
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.comwel_dir = cfg.COMWEL_DIR
        self.cols_type = convert_cols_type(cfg.COLS_TYPE)
        self.use_cols = cfg.USE_COLS
        self.yr_col = cfg.YR_COL
        self.fillna_strs = cfg.FILLNA_STRS
        self.addr_col = cfg.ADDR_COL
        self.save_filepath = cfg.SAVE_FILEPATH
    
    def __call__(self):
        """
        Description:
            경기도 고용 산재보험 가입 현황에서 사용 칼럼만 분리하여 파일 저장.
        Args:
            None
        Returns:
            None
        """
        start_time = time.time()        
        self.log.info(">>> Start to process Comwel data")
        
        comwel_file_dict = self.make_file_dict()
        comwel_df = self.load_comwel_df(comwel_file_dict)
        comwel_df = self.filter_gyeonggi(comwel_df)
        self.save_csv(comwel_df)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to process Comwel data: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def make_file_dict(self):
        """
        Description:
            연도 별로 파일경로 구분한 딕셔너리 산출.
        Args:
            None
        Returns:
            comwel_file_dict: dict, 연도 별로 파일경로 구분한 딕셔너리
        """
        comwel_file_list = glob.glob(os.path.join(self.comwel_dir, "*"))
        comwel_file_dict = defaultdict(list)
        for comwel_file in comwel_file_list:
            yr = comwel_file.split(".")[-2].split("_")[-1][:4]
            comwel_file_dict[yr].append(comwel_file)
        return comwel_file_dict
    
    def load_comwel_df(self, comwel_file_dict):
        """
        Description:
            원본 파일 불러오기.
            용량이 크기 때문에 dask로 로직만 결정.
        Args:
            comwel_file_dict: dict, 연도 별로 파일경로 구분한 딕셔너리
        Returns:
            comwel_df: dask.dataframe, 사용 칼럼만 가져온 전체 데이터
        """
        comwel_dfs = []
        for yr, file_list in comwel_file_dict.items():
            self.log.info("{0}'s data".format(yr))
            yr_list = []
            for file in file_list:
                one_df = dd.read_csv(
                    file,
                    dtype=self.cols_type,
                    encoding="cp949"
                )
                one_df = one_df[self.use_cols]
                yr_list.append(one_df)
            yr_df = dd.concat(yr_list)
            yr_df[self.yr_col] = yr   # 병합용으로 신규 칼럼 생성
            comwel_dfs.append(yr_df)
        comwel_df = dd.concat(comwel_dfs)
        return comwel_df
    
    def filter_gyeonggi(self, comwel_df):
        """
        Description:
            문자열 칼럼 NaN을 우선 모두 "None"으로 변경 후 진행.
            경기도에 해당하는 row만 가져오고 주소 칼럼 공백 모두 제거.
            고용보험은 "경기"로 시작하나 다른 파일 주소가 "경기도"로 시작하므로 변경.
            용량이 크기 때문에 dask로 로직만 결정.
        Args:
            comwel_df: dask.dataframe, 사용 칼럼만 가져온 전체 데이터
        Returns:
            comwel_df: dask.dataframe, 사용 칼럼, 경기도만 가져온 전체 데이터
        """
        comwel_df[self.fillna_strs] = comwel_df[self.fillna_strs].fillna("None")
        comwel_df = comwel_df[comwel_df[self.addr_col].str.startswith("경기")]
        comwel_df[self.addr_col] = comwel_df[self.addr_col].str.replace("경기 ", "경기도", 1)
        comwel_df[self.addr_col] = comwel_df[self.addr_col].str.replace(r"\s", "", regex=True)
        comwel_df[self.fillna_strs] = comwel_df[self.fillna_strs].replace({"None": None})   # restore None
        self.log.info("Filtering Gyeonggi datas & Remove spaces on address")
        return comwel_df
    
    def save_csv(self, comwel_df):
        """
        Description:
            CSV 파일로 저장.
            1개의 파일로 묶어 저장.
        Args:
            comwel_df: dask.dataframe, 사용 칼럼, 경기도만 가져온 전체 데이터
        Returns:
            None
        """
        try:
            comwel_df.to_csv(
                self.save_filepath,
                index=False,
                encoding="utf-8",
                mode="w",
                single_file=True
            )
        except Exception as e:
            self.log.info("Save csv file incompletely")
            self.err_log.error("Save csv file incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save csv file completely")


class GyeonggiCompany(object):
    """경기데이터드림 - 경기도 업종별 사업체 현황."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            경기도 업종별 사업체 현황 파일 불러온 뒤 필요 칼럼만 저장.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(GyeonggiCompany, self).__init__()
        cfg = cfg.GG_COMPY
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.gg_dir = cfg.GG_DIR
        gg_compy_file_list_1 = cfg.GG_COMPY_FILE_LIST_1
        gg_compy_file_list_2 = cfg.GG_COMPY_FILE_LIST_2
        cols_type_1 = convert_cols_type(cfg.COLS_TYPE_1)
        cols_type_2 = convert_cols_type(cfg.COLS_TYPE_2)
        self.use_cols = cfg.USE_COLS
        self.fillna_strs = cfg.FILLNA_STRS
        self.addr_col = cfg.ADDR_COL
        self.code_col = cfg.CODE_COL
        self.cvt_cols = cfg.CVT_COLS._asdict()   # dictionary로 변환
        self.save_filepath = cfg.SAVE_FILEPATH
        
        # 추가 변수
        self.gg_compy_file_lists = [gg_compy_file_list_1, gg_compy_file_list_2]
        self.cols_types = [cols_type_1, cols_type_2]

    def __call__(self):
        """
        Description:
            경기도 업종별 사업체 현황에서 사용 칼럼만 분리하여 파일 저장.
        Args:
            None
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to process Gyeonggi Company data")
        
        gg_compy_path_list = self.make_path_list()
        gg_compy_df = self.load_gg_compy_df(gg_compy_path_list)
        gg_compy_df = self.prepare_merging(gg_compy_df)
        self.save_csv(gg_compy_df)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to process Gyeonggi Company data: {0} hr {1} min {2:.2f} sec".format(h, m, s))
        
    def make_path_list(self):
        """
        Description:
            파일경로 리스트 산출.
        Args:
            None
        Returns:
            gg_compy_path_list: list, 원본 칼럼 별로 구분한 파일경로 리스트
        """
        gg_compy_path_list = []
        for file_list in self.gg_compy_file_lists:
            gg_compy_path_list.append([os.path.join(self.gg_dir, file) for file in file_list])
        return gg_compy_path_list
        
    def load_gg_compy_df(self, gg_compy_path_list):
        """
        Description:
            원본 파일 불러오기.
            용량이 크기 때문에 dask로 로직만 결정.
        Args:
            gg_compy_path_list: list, 원본 칼럼 별로 구분한 파일경로 리스트
        Returns:
            gg_compy_df: dask.dataframe, 사용 칼럼만 가져온 전체 데이터
        """
        gg_compy_dfs = []
        for path_list, cols_type in zip(gg_compy_path_list, self.cols_types):
            for file_path in path_list:
                one_df = dd.read_csv(
                    file_path,
                    dtype=cols_type,
                    encoding="cp949"
                    )
                gg_compy_dfs.append(one_df[self.use_cols])
        gg_compy_df = dd.concat(gg_compy_dfs)
        return gg_compy_df
    
    def prepare_merging(self, gg_compy_df):
        """
        Description:
            문자열 칼럼 NaN을 우선 모두 "None"으로 변경 후 진행.
            주소 칼럼 공백 모두 제거.
            주소가 '경기'로 시작하는 데이터만 남김 + 고용보험업종코드 있는 데이터만 남김.
            병합하기 위해 칼럼명 변경.
            용량이 크기 때문에 dask로 로직만 결정.
        Args:
            gg_compy_df: dask.dataframe, 사용 칼럼만 가져온 전체 데이터
        Returns:
            gg_compy_df: dask.dataframe, 칼럼명 변경한 전체 데이터
        """
        gg_compy_df[self.fillna_strs] = gg_compy_df[self.fillna_strs].fillna("None")
        gg_compy_df[self.addr_col] = gg_compy_df[self.addr_col].str.replace(r"\s", "", regex=True)
        gg_compy_df = gg_compy_df[gg_compy_df[self.addr_col].str.startswith("경기")]
        gg_compy_df = gg_compy_df[~gg_compy_df[self.code_col].isna()]
        gg_compy_df[self.fillna_strs] = gg_compy_df[self.fillna_strs].replace({"None": None})   # restore None
        gg_compy_df = gg_compy_df.rename(columns=self.cvt_cols)
        self.log.info("Remove spaces on address")
        return gg_compy_df
    
    def save_csv(self, gg_compy_df):
        """
        Description:
            CSV 파일로 저장.
            1개의 파일로 묶어 저장.
        Args:
            gg_compy_df: dask.dataframe, 칼럼명 변경한 전체 데이터
        Returns:
            None
        """
        try:
            gg_compy_df.to_csv(
                self.save_filepath,
                index=False,
                encoding="utf-8",
                mode="w",
                single_file=True
            )
        except Exception as e:
            self.log.info("Save csv file incompletely")
            self.err_log.error("Save csv file incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save csv file completely")


class GyeonggiFactory(DataframeUtils):
    """경기데이터드림 - 경기도 공장등록 현황."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            경기도 공장등록 현황 파일 불러온 뒤 필요 칼럼만 저장.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(GyeonggiFactory, self).__init__(log, err_log)
        cfg = cfg.GG_FAC
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.gg_fac_path = cfg.GG_FAC_PATH
        self.cols_type = convert_cols_type(cfg.COLS_TYPE)
        self.use_cols = cfg.USE_COLS
        self.addr_col = cfg.ADDR_COL
        self.code_col = cfg.CODE_COL
        self.cvt_cols = cfg.CVT_COLS._asdict()   # dictionary로 변환
        self.save_filepath = cfg.SAVE_FILEPATH
    
    def __call__(self):
        """
        Description:
            경기도 공장등록 현황에서 사용 칼럼만 분리하여 파일 저장.
        Args:
            None
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to process Gyeonggi Factory data")
        
        gg_fac_df = self.load_csv(self.gg_fac_path,
                                  self.cols_type,
                                  "cp949")
        gg_fac_df = gg_fac_df[self.use_cols]
        
        # 주소, 업종코드에 빈칸 없애기
        for col in self.cvt_cols.keys():
            gg_fac_df[col] = self.remove_space(gg_fac_df[col])
        self.log.info("Remove spaces on address")
        
        # Row 별로 업종코드 1개만 들어가도록 행 분리
        gg_fac_df[self.code_col] = gg_fac_df[self.code_col].str.split(",")
        split_code = self.split_onerow_onecode(gg_fac_df)
        gg_fac_df = self.arrange_df(gg_fac_df, split_code)
        self.log.info("Split Industry Code separately")
        
        # 빈 값 등은 None으로 변경
        gg_fac_df = self.reprocess_none(gg_fac_df)
        
        # CSV 파일 저장
        self.save_csv(gg_fac_df, self.save_filepath)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to process Gyeonggi Factory data: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def remove_space(self, series):
        """
        Description:
            문자열 칼럼 데이터 공백 모두 제거.
        Args:
            series: series, 공백 지우기 전 칼럼 데이터
        Returns:
            series: series, 공백 지운 후 칼럼 데이터
        """
        return series.str.replace(r"\s", "", regex=True)
    
    def split_onerow_onecode(self, gg_fac_df):
        """
        Description:
            업종코드가 ,로 구분되어 있어 개별 행으로 분리.
            원래 dataframe 의 index를 유지(병합용).
        Args:
            gg_fac_df: dataframe, 사용 칼럼만 가져온 전체 데이터
        Returns:
            split_code: dataframe, 업종코드가 1개씩만 있는 전체 데이터
        """
        idx_col = "tmp_idx"
        split_code = gg_fac_df[[self.code_col]]
        split_code[idx_col] = split_code.apply(lambda row: list(repeat(row.name, len(row[self.code_col]))), axis=1)
        split_code_idxs = list(chain(*split_code[idx_col].to_list()))
        split_code_values = list(chain(*split_code[self.code_col].to_list()))
        split_code = pd.DataFrame({self.code_col: split_code_values}, index=split_code_idxs)
        return split_code
    
    def arrange_df(self, gg_fac_df, split_code):
        """
        Description:
            2개의 dataframe을 병합 후 칼럼명 정리.
        Args:
            gg_fac_df: dataframe, 사용 칼럼만 가져온 전체 데이터
            split_code: dataframe, 업종코드가 1개씩만 있는 전체 데이터
        Returns:
            gg_fac_df: dataframe, 개별 행 분리, 칼럼 및 데이터 정리된 전체 데이터
        """
        gg_fac_df = pd.merge(gg_fac_df[[col for col in gg_fac_df.columns if not col == self.code_col]],
                             split_code,
                             how="right",
                             left_index=True,
                             right_index=True)
        gg_fac_df = gg_fac_df.rename(columns=self.cvt_cols)
        return gg_fac_df
    
    def reprocess_none(self, gg_fac_df):
        """
        Description:
            빈 값 등을 None으로 재처리.
        Args:
            gg_fac_df: dataframe, 개별 행 분리, 칼럼 및 데이터 정리된 전체 데이터
        Returns:
            gg_fac_df: dataframe, 개별 행 분리, 칼럼 및 데이터 정리된 전체 데이터
        """
        for col in self.cvt_cols.values():
            gg_fac_df[col] = gg_fac_df[col].replace({"": None})
        for col in gg_fac_df.columns:
            gg_fac_df[col] = gg_fac_df[col].replace({"None": None})
        return gg_fac_df


class GyeonggiChemicalHandling(DataframeUtils):
    """경기데이터드림 - 경기도 유해화학물질 취급사업장 현황."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            경기도 유해화학물질 취급사업장 현황 파일 불러온 뒤 필요 칼럼만 저장.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(GyeonggiChemicalHandling, self).__init__(log, err_log)
        cfg = cfg.GG_CHEMFAC
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.gg_chem_handle_path = cfg.GG_CHEM_HANDLE_PATH
        self.cols_type = convert_cols_type(cfg.COLS_TYPE)
        self.restore_cols = cfg.RESTORE_COLS
        self.use_cols = cfg.USE_COLS
        self.addr_col = cfg.ADDR_COL
        self.cvt_cols = cfg.CVT_COLS._asdict()   # dictionary로 변환
        self.save_filepath = cfg.SAVE_FILEPATH
    
    def __call__(self):
        """
        Description:
            경기도 유해화학물질 취급사업장 현황에서 사용 칼럼만 분리하여 파일 저장.
        Args:
            None
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to process Gyeonggi Chemical Handling data")
        
        # 데이터 불러오기
        self.modify_cols_type()
        gg_chem_handle_df = self.load_csv(self.gg_chem_handle_path,
                                          self.cols_type,
                                          "cp949")
        gg_chem_handle_df = gg_chem_handle_df[self.use_cols]
        
        # 처리
        gg_chem_handle_df[self.addr_col] = self.remove_space(gg_chem_handle_df[self.addr_col])
        self.log.info("Remove spaces on address")
        gg_chem_handle_df = gg_chem_handle_df.rename(columns=self.cvt_cols)
        self.save_csv(gg_chem_handle_df, self.save_filepath)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to process Gyeonggi Chemical Handling data: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def modify_cols_type(self):
        """
        Description:
            Namedtuple에서 숫자로 시작하는 key 사용 불가하여 원래대로 cols_type 수정.
        Args:
            None
        Returns:
            None
        """
        for k, v in self.restore_cols._asdict().items():
            self.cols_type[v] = self.cols_type.pop(k)
    
    def remove_space(self, series):
        """
        Description:
            문자열 칼럼 데이터 공백 모두 제거.
        Args:
            series: series, 공백 지우기 전 칼럼 데이터
        Returns:
            series: series, 공백 지운 후 칼럼 데이터
        """
        return series.str.replace(r"\s", "", regex=True)


class GyeonggiChemicalAccident(DataframeUtils):
    """경기데이터드림 - 경기도 유해화학물질 사고현황."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            경기도 유해화학물질 사고현황 파일 불러온 뒤 필요 칼럼만 저장.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(GyeonggiChemicalAccident, self).__init__(log, err_log)
        cfg = cfg.GG_CHEMACCI
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.gg_chem_acci_path = cfg.GG_CHEM_ACCI_PATH
        self.cols_type = convert_cols_type(cfg.COLS_TYPE)
        self.use_cols = cfg.USE_COLS
        self.addr_col = cfg.ADDR_COL
        self.cvt_cols = cfg.CVT_COLS._asdict()   # dictionary로 변환
        self.save_filepath = cfg.SAVE_FILEPATH
    
    def __call__(self):
        """
        Description:
            경기도 유해화학물질 사고현황에서 사용 칼럼만 분리하여 파일 저장.
        Args:
            None
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to process Gyeonggi Chemical Accident data")
        
        # 데이터 불러오기
        gg_chem_acci_df = self.load_csv(self.gg_chem_acci_path,
                                        self.cols_type,
                                        "cp949")
        gg_chem_acci_df = gg_chem_acci_df[self.use_cols]
        
        # 처리
        gg_chem_acci_df[self.addr_col] = self.remove_space(gg_chem_acci_df[self.addr_col])
        gg_chem_acci_df = gg_chem_acci_df.rename(columns=self.cvt_cols)
        self.save_csv(gg_chem_acci_df, self.save_filepath)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to process Gyeonggi Chemical Accident data: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def remove_space(self, series):
        """
        Description:
            문자열 칼럼 데이터 공백 모두 제거.
            괄호가 있는 경우, 괄호와 괄호 안의 내용 모두 삭제.
        Args:
            series: series, 공백 지우기 전 칼럼 데이터
        Returns:
            series: series, 공백 지운 후 칼럼 데이터
        """
        series = series.fillna("None")
        series = series.str.replace(r"\([^()]+\)", "", regex=True)
        series = series.str.replace(r"\s", "", regex=True)
        series = series.replace({"None": None})   # restore None
        return series
