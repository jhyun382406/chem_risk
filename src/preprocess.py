import os
import time
import joblib
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import QuantileTransformer
from category_encoders.binary import BinaryEncoder
from category_encoders.ordinal import OrdinalEncoder

from utils.log_module import print_elapsed_time
from utils.code_utils import (
    convert_cols_type, save_pickle, load_pickle,
    concat_dict, unpack
    )
from utils.df_utils import DataframeUtils

import warnings
warnings.filterwarnings(action="ignore")


class MergeDatas(DataframeUtils):
    """필요 칼럼만 추출한 파일 병합 후 저장."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            유해화학물질 파일 불러온 뒤 파일 병합 후 저장.
                1. 고용 산재보험 가입 현황
                2. 경기도 업종별 사업체 현황
                3. 경기도 공장등록 현황
                4. 경기도 유해화학물질 취급사업장 현황
                5. 경기도 유해화학물질 사고현황
            병합 기준
                - 사업장주소(빈칸x) 일치
                - 1.와 2.는 조사년도, 고용보험업종코드도 일치(Inner join)
                - 3 ← (1, 2) ← 4 ← 5 식으로 Join (,: Inner join / ←: Left join)
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(MergeDatas, self).__init__(log, err_log)
        cfg_p = cfg.PREPROCESS.PREP
        cfg_m = cfg.PREPROCESS.MERGE
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.comwel_path = cfg_m.COMWEL_PATH
        self.comwel_cols_type = convert_cols_type(cfg_m.COMWEL_COLS_TYPE)
        self.gg_compy_path = cfg_m.GG_COMPY_PATH
        self.gg_compy_cols_type = convert_cols_type(cfg_m.GG_COMPY_COLS_TYPE)
        self.gg_fac_path = cfg_m.GG_FAC_PATH
        self.gg_fac_cols_type = convert_cols_type(cfg_m.GG_FAC_COLS_TYPE)
        self.gg_chemfac_path = cfg_m.GG_CHEMFAC_PATH
        self.gg_chemfac_cols_type = convert_cols_type(cfg_m.GG_CHEMFAC_COLS_TYPE)
        self.gg_chemfac_restore_cols = cfg_m.GG_CHEMFAC_RESTORE_COLS
        for k, v in self.gg_chemfac_restore_cols._asdict().items():
            self.gg_chemfac_cols_type[v] = self.gg_chemfac_cols_type.pop(k)
        self.gg_chemacci_path = cfg_m.GG_CHEMACCI_PATH
        self.gg_chemacci_cols_type = convert_cols_type(cfg_m.GG_CHEMACCI_COLS_TYPE)
        
        self.yr_col = cfg_m.YR_COL
        self.addr_col = cfg_m.ADDR_COL
        self.code_col = cfg_m.CODE_COL
        self.tgt_merge_col1 = cfg_m.TGT_MERGE_COL1
        self.tgt_merge_col2 = cfg_m.TGT_MERGE_COL2
        self.addr_encoder_path = cfg_m.ADDR_ENCODER_PATH
        self.code_ctgy_digits = cfg_p.CODE_CTGY_DIGITS
        self.save_jsonpath = cfg_m.SAVE_JSONPATH
        self.save_filepath = cfg_m.SAVE_FILEPATH
    
    def __call__(self):
        """
        Description:
            유해화학물질 파일 불러온 뒤 병합 후 파일 저장.
            주소를 그래도 이용하여 merge하면 메모리 이슈 발생하여 encoder 사용.
            고용보험업종코드는 5자리에서 앞의 2자리만 추출하여 사용.
        Args:
            None
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to Merge datas")
        
        # 주소 Label encoding부터 진행
        gg_compy_df = self.load_csv(self.gg_compy_path, self.gg_compy_cols_type)
        addr_encoder, gg_compy_df = self.make_addr_encoder(gg_compy_df)
        self.log.info("Make Address Label encoder")
        self.save_addr_encoder(addr_encoder)
        
        # Inner join
        comwel_df = self.load_csv(self.comwel_path, self.comwel_cols_type, addr_encoder)
        join_cols = [self.addr_col, self.code_col, self.yr_col]
        merge_df = self.inner_join(gg_compy_df, comwel_df, join_cols)
        
        # Left join
        gg_fac_df = self.load_csv(self.gg_fac_path, self.gg_fac_cols_type, addr_encoder)
        gg_chemfac_df = self.load_csv(self.gg_chemfac_path, self.gg_chemfac_cols_type, addr_encoder)
        gg_chemacci_df = self.load_csv(self.gg_chemacci_path, self.gg_chemacci_cols_type, addr_encoder)
        merge_df = self.left_join(gg_fac_df, merge_df, [self.addr_col, self.code_col])
        merge_df = self.left_join(merge_df, gg_chemfac_df, [self.addr_col])
        merge_df = self.target_join(merge_df, gg_chemacci_df)
        
        # 고용보험업종코드 추가 처리
        merge_df = self.additional_prep_code_col(merge_df)
        nums, col_nums = merge_df.shape
        self.log.info("Merge all datas: numbers - {0} columns - {1}".format(nums, col_nums))
        
        # Save file
        merge_cols_type = self.make_cols_type(merge_df)
        self.save_csv(merge_df, self.save_filepath)
        self.save_cols_type(merge_cols_type, self.save_jsonpath)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to Merge datas: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    def load_csv(self, data_path, cols_type, addr_encoder=None):
        """
        Description:
            * Override
            원본 파일 불러오기.
            주소는 Label encoding 결과만 남으면 됨.
        Args:
            data_path: str, 불러올 csv 파일 경로
            cols_type: dict, 불러올 csv 칼럼 별 타입
            addr_encoder: LabelEncoder, 사업장 주소 Label encoder (default=None)
        Returns:
            df: dataframe, 주소 인코딩된 전체 데이터
        """
        try:
            filename = os.path.basename(data_path)
            if addr_encoder is None:
                df = pd.read_csv(data_path, encoding="utf-8", dtype=cols_type)
            else:
                df = pd.read_csv(data_path, encoding="utf-8", dtype=cols_type)
                df = df[df[self.addr_col].isin(addr_encoder.classes_)]
                df[self.addr_col] = addr_encoder.transform(df[self.addr_col])
        except Exception as e:
            self.log.info("Load csv file incompletely: {0}".format(filename))
            self.err_log.error("Load csv file incompletely: {0}".format(filename))
            self.err_log.error("{0}".format(e))
            raise e
        else:
            self.log.info("Load csv file completely: {0}".format(filename))
            return df
    
    def make_addr_encoder(self, gg_compy_df):
        """
        Description:
            사업장주소에 대해 Label encoding 진행.
        Args:
            gg_compy_df: dataframe, 경기도 업종별 사업체 현황
        Returns:
            addr_encoder: LabelEncoder, 경기도 업종별 사업체 현황의 주소 Label encoder
            gg_compy_df: dataframe, 주소 인코딩된 경기도 업종별 사업체 현황
        """
        addr_encoder = LabelEncoder()
        addr_encoder.fit(gg_compy_df[self.addr_col])
        gg_compy_df[self.addr_col] = addr_encoder.transform(gg_compy_df[self.addr_col])
        return addr_encoder, gg_compy_df
    
    def save_addr_encoder(self, addr_encoder):
        """
        Description:
            사업장주소에 대한 Label encoder 파일 저장.
        Args:
            addr_encoder: LabelEncoder, 경기도 업종별 사업체 현황의 주소 Label encoder
        Returns:
            None
        """
        try:
            save_pickle(addr_encoder, self.addr_encoder_path)
        except Exception as e:
            self.log.info("Save Address Label encoder incompletely")
            self.err_log.error("Save Address Label encoder incompletely: {0}".format(e))
            raise e
        else:
            self.log.info("Save Address Label encoder completely")

    def inner_join(self, df1, df2, join_cols):
        """
        Description:
            2개 dataframe에 대해 Inner join 진행.
        Args:
            df1: dataframe, 주소 인코딩된 dataframe1
            df2: dataframe, 주소 인코딩된 dataframe2
            join_cols: list, inner join에 사용할 칼럼
        Returns:
            merge_df: dataframe, inner join 결과 dataframe
        """
        # 서로 2개 dataframe에 있는 주소만 있도록 처리 (메모리 문제 해결)
        df1 = df1[df1[self.addr_col].isin(set(df2[self.addr_col]))]
        df2 = df2[df2[self.addr_col].isin(set(df1[self.addr_col]))]
        
        # 병합 후 중복 데이터 제거
        merge_df = pd.merge(df1, df2, how="inner", on=join_cols)
        merge_df = merge_df.drop_duplicates(subset=join_cols, ignore_index=True, keep="last")
        
        return merge_df
    
    def left_join(self, base_df, add_df, join_cols):
        """
        Description:
            2개 dataframe에 대해 Left join 진행.
            Not null인 칼럼에서 값이 중복되지 않도록 추가 처리.
        Args:
            base_df: dataframe, 기준의 되는 주소 인코딩된 dataframe
            add_df: dataframe, 병합시킬 주소 인코딩된 dataframe
            join_cols: list, left join에 사용할 칼럼
        Returns:
            merge_df: dataframe, left join 결과 dataframe
        """
        merge_df = pd.merge(base_df, add_df, how="left", on=join_cols)
        # 중복 제거
        notnull_cols = self.return_notnull_cols(merge_df)
        merge_df = merge_df.drop_duplicates(subset=notnull_cols, ignore_index=True, keep="last")
        return merge_df
    
    def target_join(self, merge_df, target_df):
        """
        Description:
            target dataframe을 Left join 진행.
            Not null인 칼럼에서 값이 중복되지 않도록 추가 처리.
        Args:
            merge_df: dataframe, 기준의 되는 주소 인코딩된 병합 dataframe
            target_df: dataframe, 병합시킬 주소 인코딩된 타겟 dataframe
        Returns:
            merge_df: dataframe, left join 결과 dataframe
        """
        # 병합용 임시 칼럼 생성
        tmp_col = "tmp"
        merge_df[tmp_col] = merge_df[self.tgt_merge_col1].str.slice(stop=4)
        target_df[tmp_col] = target_df[self.tgt_merge_col2].str.slice(stop=4)
        # 병합 후 임시 칼럼 삭제
        merge_df = self.left_join(merge_df, target_df, [self.addr_col, tmp_col])
        notnull_cols = self.return_notnull_cols(merge_df)
        merge_df = merge_df.drop_duplicates(subset=notnull_cols, ignore_index=True, keep="last")
        merge_df = merge_df.drop(tmp_col, axis=1)
        return merge_df
    
    def additional_prep_code_col(self, merge_df):
        """
        Description:
            고용보험업종코드 추가 처리.
                - NaN 있는 데이터 행 제외
                - 5자리 코드를 맨 앞의 2자리(self.code_ctgy_digits)만 가져옴
        Args:
            merge_df: dataframe, 병합 dataframe
        Returns:
            merge_df: dataframe, 추가 전처리 완료된 dataframe
        """
        merge_df = merge_df.dropna(axis=0, subset=[self.code_col])
        merge_df[self.code_col] = merge_df[self.code_col].str.slice(start=0, stop=self.code_ctgy_digits)
        return merge_df


class PrepDatas(DataframeUtils):
    """학습 데이터 결측 처리, 인코딩 및 스케일링 진행."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            결측 처리, 인코딩 및 스케일링 진행 후 파일 저장.
                1. 결측값 처리
                2. 범주형 features: Binary Encoding
                3. 순서 필요 범주형 features(수치 취급): Ordinal Encoding
                4. 수치형 feautures를 Scaling
                5. 사고 여부 target 생성
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(PrepDatas, self).__init__(log, err_log)
        cfg_m = cfg.PREPROCESS.MERGE
        cfg_p = cfg.PREPROCESS.PREP
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.load_jsonpath = cfg_m.SAVE_JSONPATH
        self.load_filepath = cfg_m.SAVE_FILEPATH
        self.addr_encoder_path = cfg_m.ADDR_ENCODER_PATH
        self.addr_col = cfg_p.ADDR_COL
        self.worker_num_col = cfg_p.WORKER_NUM_COL
        self.fill_latlon = cfg_p.FILL_LATLON._asdict()
        self.fill_value = cfg_p.FILL_VALUE._asdict()
        self.fill_random = cfg_p.FILL_RANDOM
        self.ctgy_cols = cfg_p.CTGY_COLS
        self.yn_cols = cfg_p.YN_COLS
        self.ord_cols = cfg_p.ORD_COLS
        self.num_quantile_cols = cfg_p.NUM_QUANTILE_COLS
        self.tgt_cols = cfg_p.TGT_COLS
        self.tgt_final = cfg_p.TGT_FINAL
        self.sigun_latlon_mean_path = cfg_p.SIGUN_LATLON_MEAN_PATH
        # self.ctgy_cols 와 같은 길이
        self.ctgy_lists = [
            # cfg_p.ADDR_CTGY,
            ["{0:02d}".format(i) for i in range(10 ** cfg_p.CODE_CTGY_DIGITS)],
            cfg_p.FOUND_CTGY,
            cfg_p.KIND_CTGY
        ]
        self.yn_ord = cfg_p.YN_ORD._asdict()
        self.facsize_ord = cfg_p.FACSIZE_ORD._asdict()
        self.emit_ord = cfg_p.EMIT_ORD._asdict()
        self.encoder_path = cfg_p.ENCODER_PATH
        self.scaler_path = cfg_p.SCALER_PATH
        self.save_jsonpath = cfg_p.SAVE_JSONPATH
        self.save_filepath = cfg_p.SAVE_FILEPATH
        
        # 추가 처리
        self.yn_ord[np.nan] = -2
        self.facsize_ord[np.nan] = -2
        self.emit_ord[np.nan] = -2
        for k, v in cfg_p.EMIT_ORD_RESTORE._asdict().items():   # namedtuple key에 숫자 처리
            self.emit_ord[v] = self.emit_ord.pop(k)
        # self.ord_cols 와 같은 길이
        self.ord_dicts = [
            self.facsize_ord,
            self.emit_ord,
            self.emit_ord
        ]
        
        np.random.seed(cfg_p.RANDOM_SEED)
    
    def __call__(self):
        """
        Description:
            유해화학물질 병합 파일에서 학습 전처리 진행 후 파일 저장.
                1. 결측값 처리
                2. 범주형 features: Binary Encoding
                3. 순서 필요 범주형 features(수치 취급): Ordinal Encoding
                4. 수치형 feautures를 [0, 1] 구간으로 Scaling
                5. 사고 여부 target 생성
        Args:
            None
        Returns:
            None
        """
        start_time = time.time()
        self.log.info(">>> Start to Preprocess datas")
        
        self.log.info("Preprocess datas on training")
        # 파일 불러오기
        cols_type = self.load_cols_type(self.load_jsonpath)
        merge_df = self.load_csv(self.load_filepath, cols_type)
        
        # 결측값 처리 및 Target 생성 + 필요 칼럼만 추출
        filled_df = self.fill_missing_value(merge_df)
        filled_df = self.make_target(filled_df)
        filled_df = self.select_cols(filled_df)
        
        # Encoding, Scaling 진행 및 encoder와 scaler 저장
        encoded_df, encoder_dict = self.encode_cols(filled_df)
        self.save_dict_pkl(encoder_dict, self.encoder_path, "Encoder")
        scaled_df, scaler_dict = self.scale_cols(
            encoded_df,
            filled_df[self.num_quantile_cols]
        )
        self.save_dict_pkl(scaler_dict, self.scaler_path, "Scaler")
        
        # 전체 데이터 병합 후 저장
        non_scaled_df = self.make_non_scaled_df(encoded_df)
        total_df = self.make_total_df(scaled_df, non_scaled_df)
        total_cols_type = self.make_cols_type(total_df)
        self.save_csv(total_df, self.save_filepath)
        self.save_cols_type(total_cols_type, self.save_jsonpath)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to Preprocess datas: {0} hr {1} min {2:.2f} sec".format(h, m, s))
    
    # def load_addr_encoder(self):
    #     """
    #     Description:
    #         사업장 주소 Label encoder 불러오기.
    #     Args:
    #         None
    #     Returns:
    #         addr_encoder: LabelEncoder, 사업장 주소 Label encoder
    #     """
    #     try:
    #         addr_encoder = joblib.load(self.addr_encoder_path)
    #     except Exception as e:
    #         self.log.info("Load Address Label encoder incompletely")
    #         self.err_log.error("Load Address Label encoder incompletely: {0}".format(e))
    #         raise e
    #     else:
    #         self.log.info("Load Address Label encoder completely")
    #         return addr_encoder
    
    # def convert_addr_sigun(self, addr_series, addr_encoder):
    #     """
    #     Description:
    #         사업장 주소를 Label encoder로 복원 후 경기도 시/군만 추출.
    #     Args:
    #         addr_series: series, 사업장 주소 데이터
    #         addr_encoder: LabelEncoder, 사업장 주소 Label encoder
    #     Returns:
    #         addr_encoder: LabelEncoder, 사업장 주소 Label encoder
    #     """
    #     addr_series = pd.Series(addr_encoder.inverse_transform(addr_series))
    #     addr_series = addr_series.str.slice(start=3, stop=7).str.extract(r"([가-힇]{2,3}[시군])").squeeze()
    #     addr_series = addr_series.str.extract(r"([가-힇]+[^시군])")
    #     self.log.info("Convert full address to sigun")
    #     return addr_series
    
    def fill_missing_value(self, merge_df):
        """
        Description:
            결측 처리.
        Args:
            merge_df: dataframe, 불러온 병합 데이터
        Returns:
            filled_df: dataframe, 결측 처리 완료한 데이터
        """
        # # 위/경도
        # latlon_cols = list(self.fill_latlon.keys()) + list(self.fill_latlon.values())
        # latlon_cols.append(self.addr_col)   # 주소 추가
        # latlon_df, sigun_latlon_mean = self.fill_latlon_df(merge_df[latlon_cols])
        # self.save_sigun_latlon_mean(sigun_latlon_mean)
        # merge_df[latlon_df.columns] = latlon_df
        # self.log.info("Fill Lat & Lon's missnig value")
        
        # 상수 값으로 대체
        constant_df = self.fill_constant_df(merge_df[self.fill_value.keys()])
        merge_df[constant_df.columns] = constant_df
        self.log.info("Fill constant data's missnig value")
        
        # 무작위 값으로 대체
        random_cols = self.fill_random + [self.worker_num_col]
        random_df = self.fill_random_df(merge_df[random_cols])
        merge_df[random_df.columns] = random_df
        self.log.info("Fill random data's missnig value")
        
        filled_df = merge_df

        return filled_df
    
    # def fill_latlon_df(self, latlon_df):
    #     """
    #     Description:
    #         위/경도 데이터 결측 처리.
    #             - WGS84위도: 정제WGS84위도 로 대체
    #             - WGS84경도: 정제WGS84경도 로 대체
    #             - 그래도 값이 없다면 동일 사업장주소 시/군의 평균값으로 대체
    #     Args:
    #         latlon_df: dataframe, 위/경도 데이터
    #     Returns:
    #         latlon_df: dataframe, 결측 처리 완료된 위/경도 데이터
    #         sigun_latlon_mean: dataframe, 시군별 위/경도 데이터 평균
    #     """
    #     for base_col, fill_col in self.fill_latlon.items():
    #         latlon_df[base_col] = latlon_df[base_col].fillna(latlon_df[fill_col])
    #     # 시/군 별 평균 계산 후 대체
    #     latlon_df = latlon_df.drop(self.fill_latlon.values(), axis=1)
    #     sigun_latlon_mean = latlon_df.groupby(self.addr_col).mean()
    #     latlon_df = latlon_df.set_index(self.addr_col)
    #     for sigun in sigun_latlon_mean.index:
    #         try:
    #             latlon_df.loc[sigun] = latlon_df.loc[sigun].fillna(sigun_latlon_mean.loc[sigun])
    #         except Exception as e:
    #             pass
    #     return latlon_df.reset_index(drop=True), sigun_latlon_mean
    
    # def save_sigun_latlon_mean(self, sigun_latlon_mean):
    #     """
    #     Description:
    #         사업장주소에 대한 Label encoder 파일 저장.
    #     Args:
    #         sigun_latlon_mean: dataframe, 시군별 위/경도 데이터 평균
    #     Returns:
    #         None
    #     """
    #     try:
    #         joblib.dump(sigun_latlon_mean, self.sigun_latlon_mean_path)
    #     except Exception as e:
    #         self.log.info("Save Sigun latlon mean incompletely")
    #         self.err_log.error("Save Sigun latlon mean incompletely: {0}".format(e))
    #         raise e
    #     else:
    #         self.log.info("Save Sigun latlon mean completely")
    
    def fill_constant_df(self, constant_df):
        """
        Description:
            상수 값으로 대체 가능한 데이터 결측 처리.
                - 업종명: 유해화학물질미취급
                - 대기배출시설규모: 0
                - 수질배출시설규모: 0
                - 유독물질취급여부: N
                - 제한물질취급여부: N
                - 금지물질취급여부: N
                - 사고대비물질취급여부: N
                - 연간취급량상한선: -1
        Args:
            constant_df: dataframe, 상수 값으로 결측 처리할 데이터
        Returns:
            constant_df: dataframe, 상수 값으로 결측 처리한 데이터
        """
        return constant_df.fillna(self.fill_value)
    
    def fill_random_df(self, random_df):
        """
        Description:
            특정 값 이하의 무작위 수로 결측처리
                - 산재보험상시근로자수: 종업원수 이하의 무작위 정수
                - 고용보험상시근로자수: 종업원수 이하의 무작위 정수
        Args:
            random_df: dataframe, 무작위 값으로 결측 처리할 데이터
        Returns:
            random_df: dataframe, 무작위 값으로 결측 처리한 데이터
        """
        for col in self.fill_random:
            func_random = lambda row: np.random.randint(0, row[self.worker_num_col]+1)
            random_df[col] = random_df.apply(func_random, axis=1)
        return random_df[self.fill_random]
    
    def make_target(self, filled_df):
        """
        Description:
            Target으로 사용할 '사고여부' 데이터 생성.
            행마다 1개 칼럼이라도 값이 있으면 True, 모두 빈 값이면 False
        Args:
            filled_df: dataframe, 결측 처리 완료한 데이터
        Returns:
            filled_df: dataframe, target 데이터 처리한 데이터
        """
        filled_df[self.tgt_final] = np.any(filled_df[self.tgt_cols], axis=1)
        self.log.info("Make Accident status (target)")
        return filled_df
    
    def select_cols(self, filled_df, pred_mode=False):
        """
        Description:
            결측 처리 완료된 데이터에서 사용 칼럼만 선별.
        Args:
            filled_df: dataframe, 결측 처리 + target 데이터 처리한 데이터
            pred_mode: bool, 추론 모드 여부 (default=False)
        Returns:
            filled_df: dataframe, 사용 칼럼만 선별한 데이터
        """
        # use_cols = self.ctgy_cols \
        #            + self.yn_cols \
        #            + self.ord_cols \
        #            + self.num_minmax_cols \
        #            + self.num_quantile_cols
        use_cols = self.ctgy_cols \
                   + self.yn_cols \
                   + self.ord_cols \
                   + self.num_quantile_cols
        tgt_cols = [self.tgt_final]
        self.log.info("Select using columns (Input): {0}".format(len(use_cols)))
        if pred_mode:
            return filled_df[use_cols]
        else:
            self.log.info("Select using columns (Target): {0}".format(len(tgt_cols)))
            return filled_df[use_cols + tgt_cols]
    
    def encode_cols(self, filled_df):
        """
        Description:
            결측 처리 완료된 데이터에서 칼럼별 인코딩.
            Binary Encoding
                - 고용보험업종코드
                - 설립구분명
                - 업종명
            Ordinal Encoding: nan은 -2가 되도록 설정됨
                - 유독물질취급여부
                - 제한물질취급여부
                - 금지물질취급여부
                - 사고대비물질취급여부
                - 공장규모구분명
                - 대기배출시설규모
                - 수질배출시설규모
        Args:
            filled_df: dataframe, 결측 처리 + target 데이터 처리한 데이터
        Returns:
            encoded_df: dataframe, 범주형 칼럼에 대해 인코딩 완료한 데이터
            encoder_dict: dict, 인코더 딕셔너리
        """
        # 범주형 항목 중 Binary Encoding
        ctgy_df = filled_df[self.ctgy_cols]
        ctgy_df, be_dict = self.use_bn_encoder(ctgy_df)
        
        # 범주형 항목 중 Ordinal Encoding
        ord_df = filled_df[self.yn_cols + self.ord_cols]
        ord_df, oe_dict = self.use_ord_encoder(ord_df)
        
        # Target은 bool -> int
        target_df = filled_df[[self.tgt_final]]
        target_df = self.encode_target(target_df)
        
        # 범주형 항목, target 병합, Encoder 저장
        encoded_df = pd.concat([ctgy_df, ord_df, target_df], axis=1)
        encoder_dict = concat_dict(be_dict, oe_dict)
        
        return encoded_df, encoder_dict
    
    def use_bn_encoder(self, ctgy_df):
        """
        Description:
            Binary Encoding 처리
                - 고용보험업종코드: 앞의 2자리만 사용
                    - 00 ~ 99
                - 설립구분명
                    - 일반산업단지, 국가산업단지, 일반,
                      창업, 지식산업센터, 농공단지,
                      도시첨단산업단지, 자유무역지역
                - 업종명
                    - 유해화학물질미취급, 알선판매업, 사용업,
                      판매업(취급시설有), 제조업, 운반업, 보관저장업
        Args:
            ctgy_df: dataframe, Binary Encoding 처리할 데이터
        Returns:
            ctgy_df: dataframe, Binary Encoding 완료한 데이터
            be_dict: dict, 칼럼 별 Binary Encoder
        """
        ctgy_df_list = []
        be_dict = dict()
        for ctgy_col, ctgy_list in zip(self.ctgy_cols, self.ctgy_lists):
            bn_encoder = self.make_bn_encoder(ctgy_col, ctgy_list)
            ctgy_df_list.append(bn_encoder.transform(ctgy_df[ctgy_col]))
            be_dict[ctgy_col] = bn_encoder
        ctgy_df = pd.concat(ctgy_df_list, axis=1)
        return ctgy_df, be_dict
    
    def make_bn_encoder(self, ctgy_col, ctgy_list):
        """
        Description:
            Binary Encoder 생성
        Args:
            ctgy_col: str, Binary Encoding 할 칼럼 명
            ctgy_list: list, Binary Encoding 할 범주 리스트
        Returns:
            bn_encoder: BinaryEncoder, Binary Encoder
        """
        bn_encoder = BinaryEncoder()
        bn_encoder_df = pd.DataFrame({ctgy_col: ctgy_list})
        bn_encoder.fit(bn_encoder_df)
        mapping_df = bn_encoder.mapping[0]["mapping"]
        origin_cls_num = len(mapping_df) - 2   # NaN과 None은 제외
        bn_cls_num = len(mapping_df.columns)
        self.log.info("Split Binary encoding columns: {0} / {1} -> {2}".format(ctgy_col, origin_cls_num, bn_cls_num))
        return bn_encoder
        
    def use_ord_encoder(self, ord_df):
        """
        Description:
            Ordinal Encoding 처리
            nan은 -2가 되도록 init에서 설정함
                - 유독물질취급여부: N은 0, Y는 1
                - 제한물질취급여부: N은 0, Y는 1
                - 금지물질취급여부: N은 0, Y는 1
                - 사고대비물질취급여부: N은 0, Y는 1
                - 공장규모구분명: 소, 중, 중견, 대 순으로 0 ~ 3
                - 대기배출시설규모: 0 ~ 5 문자열을 숫자로
                - 수질배출시설규모: 0 ~ 5 문자열을 숫자로
        Args:
            ord_df: dataframe, Ordinal Encoding 처리할 데이터
        Returns:
            ord_df: dataframe, Ordinal Encoding 완료한 데이터
            oe_dict: dict, 칼럼 별 Ordinal Encoder
        """
        oe_dict = dict()
        for yn_col in self.yn_cols:
            ord_encoder = self.make_ord_encoder(yn_col, self.yn_ord)
            ord_df[yn_col] = ord_encoder.transform(ord_df[yn_col])
            oe_dict[yn_col] = ord_encoder
        for ord_col, ord_dict in zip(self.ord_cols, self.ord_dicts):
            ord_encoder = self.make_ord_encoder(ord_col, ord_dict)
            ord_df[ord_col] = ord_encoder.transform(ord_df[ord_col])
            oe_dict[ord_col] = ord_encoder
        return ord_df, oe_dict

    def make_ord_encoder(self, ord_col, ord_dict):
        """
        Description:
            Ordinal Encoder 생성
        Args:
            ord_col: str, Ordinal Encoding 할 칼럼 명
            ord_dict: dict, Ordinal Encoding mapping할 범주 딕셔너리
        Returns:
            ord_encoder: OrdinalEncoder, Ordinal Encoder
        """
        mapping_dict = {"col": ord_col, "mapping": ord_dict}
        ord_encoder = OrdinalEncoder(mapping=[mapping_dict])
        ord_encoder_df = pd.DataFrame({ord_col: ord_dict.keys()})
        ord_encoder.fit(ord_encoder_df)
        self.log.info("Ordinal encoding columns: {0}".format(ord_col))
        return ord_encoder
    
    def encode_target(self, target_df):
        """
        Description:
            Target data의 True, False를 정수 1, 0으로 변환.
        Args:
            target_df: dataframe, target 데이터 (bool)
        Returns:
            target_df: dataframe, target 데이터 (int)
        """
        target_df[self.tgt_final] = target_df[self.tgt_final].astype(int)
        self.log.info("Convert target type: Boolean -> Integer")
        return target_df
    
    def scale_cols(self, encoded_df, numeric_df):
        """
        Description:
            인코딩 완료된 데이터에서 칼럼별 Scaling.
            [0, 1] 구간으로 데이터를 Scaling 진행.
            추후 추론 전처리 시에 결측은 - 값이 되도록 처리 예정.
            값이 0 또는 1인 칼럼은 Scaling 하지 않음
                - Binary Encoding 한 칼럼
                - Ordinal Encoding 한 칼럼 중 YN만 있는 칼럼
            MinMax Scaling
                - Ordinal Encoding 한 칼럼
                - 수치형 칼럼
            Quantile transformation (Uniform PDF)
                - 수치형 칼럼
        Args:
            encoded_df: dataframe, 범주형 칼럼에 대해 인코딩 완료한 데이터
            numeric_df: dataframe, 수치형 칼럼 데이터
        Returns:
            scale_df: dataframe, 칼럼별로 Scaling 완료한 데이터
            scaler_dict: dict, 스케일러 딕셔너리
        """
        # MinMax Scaling
        # minmax_df = pd.concat(
        #     [encoded_df[self.ord_cols], numeric_df[self.num_minmax_cols]],
        #     axis=1
        # )
        minmax_df = pd.concat(
            [encoded_df[self.ord_cols]],
            axis=1
        )
        minmax_df, minmax_dict = self.use_minmax_scaler(minmax_df)
        
        # Quantile transformation (Uniform PDF)
        quantile_df = numeric_df[self.num_quantile_cols]
        quantile_df, quantile_dict = self.use_quantile_scaler(quantile_df)
        
        # Merge
        scale_df = pd.concat([minmax_df, quantile_df], axis=1)
        scaler_dict = concat_dict(minmax_dict, quantile_dict)
        
        return scale_df, scaler_dict

    def use_minmax_scaler(self, minmax_df):
        """
        Description:
            MinMax Scaling (min값을 0으로)
                - Ordinal Encoding 한 칼럼
                    - 공장규모구분명
                    - 대기배출시설규모
                    - 수질배출시설규모
        Args:
            minmax_df: dataframe, MinMax Scaling 처리할 데이터
        Returns:
            minmax_df: dataframe, MinMax Scaling 완료한 데이터
            minmax_dict: dict, 칼럼 별 MinMax Scaler
        """
        minmax_dict = dict()
        for minmax_col in minmax_df.columns:
            minmax_ary = minmax_df[minmax_col].values.reshape(-1, 1)
            minmax_scaler = self.make_minmax_scaler(minmax_col, minmax_ary)
            minmax_df[minmax_col] = minmax_scaler.transform(minmax_ary)
            minmax_dict[minmax_col] = minmax_scaler
        return minmax_df, minmax_dict

    def make_minmax_scaler(self, minmax_col, minmax_ary):
        """
        Description:
            MinMax Scaler 생성
        Args:
            minmax_col: str, MinMax Scaling 할 칼럼 명
            minmax_ary: numpy array, MinMax Scaling 할 데이터
        Returns:
            minmax_scaler: MinMaxScaler, 해당 칼럼 MinMax Scaler
        """
        minmax_scaler = MinMaxScaler()
        minmax_scaler.fit(minmax_ary.clip(min=0))
        self.log.info("MinMax Scaling columns: {0}".format(minmax_col))
        return minmax_scaler
    
    def use_quantile_scaler(self, quantile_df):
        """
        Description:
            Quantile transformation (Uniform PDF) Scaling
                - 수치형 칼럼
                    - 용지면적
                    - 건축면적
                    - 종업원수
                    - 산재보험상시근로자수
                    - 고용보험상시근로자수
                    - 연간취급량상한선
        Args:
            quantile_df: dataframe, Quantile transformation 처리할 데이터
        Returns:
            quantile_df: dataframe, Quantile transformation 완료한 데이터
            quantile_dict: dict, 칼럼 별 Quantile transformation (Uniform PDF) Scaler
        """
        quantile_dict = dict()
        for quantile_col in quantile_df.columns:
            quantile_ary = quantile_df[quantile_col].values.reshape(-1, 1)
            quantile_scaler = self.make_quantile_scaler(quantile_col, quantile_ary)
            quantile_df[quantile_col] = quantile_scaler.transform(quantile_ary)
            quantile_dict[quantile_col] = quantile_scaler
        return quantile_df, quantile_dict
    
    def make_quantile_scaler(self, quantile_col, quantile_ary):
        """
        Description:
            Quantile transformation (Uniform PDF) Scaler 생성
        Args:
            quantile_col: str, Quantile transformation (Uniform PDF) Scaling 할 칼럼 명
            quantile_ary: numpy array, Quantile transformation (Uniform PDF) Scaling 할 데이터
        Returns:
            quantile_scaler: QuantileTransformer, 해당 칼럼 Quantile transformation (Uniform PDF) Scaler
        """
        quantile_scaler = QuantileTransformer()
        quantile_scaler.fit(quantile_ary)
        self.log.info("Quantile transformation (Uniform PDF) Scaling columns: {0}".format(quantile_col))
        return quantile_scaler
    
    def save_dict_pkl(self, saved_dict, pkl_path, dict_name):
        """
        Description:
            딕셔너리를 Pickle 파일로 저장.
        Args:
            saved_dict: dict, 저장할 딕셔너리
            pkl_path: str, pickle 파일 저장경로
            dict_name: str, 로그로 남길 딕셔너리 이름
        Returns:
            None
        """
        try:
            save_pickle(saved_dict, pkl_path)
        except Exception as e:
            self.log.info("Save {0} incompletely".format(dict_name))
            self.err_log.error("Save {0} incompletely: {1}".format(dict_name, e))
            raise e
        else:
            self.log.info("Save {0} completely".format(dict_name))
    
    def make_non_scaled_df(self, encoded_df):
        """
        Description:
            Scaling 하지 않은 데이터만 추출.
        Args:
            encoded_df: dataframe, 범주형 칼럼에 대해 인코딩 완료한 데이터
        Returns:
            non_scaled_df: dataframe, Scaling 하지 않은 데이터
        """
        non_scaled_cols = [col for col in encoded_df.columns
                           if not col in self.ord_cols]
        non_scaled_df = encoded_df[non_scaled_cols]
        return non_scaled_df
    
    def make_total_df(self, scaled_df, non_scaled_df):
        """
        Description:
            Scaling & None-Scaling dataframe 병합.
        Args:
            scaled_df: dataframe, 칼럼별로 Scaling 완료한 데이터
            non_scaled_df: dataframe, Scaling 하지 않은 데이터
        Returns:
            total_df: dataframe, 최종 전처리 데이터
        """
        total_df = pd.concat([scaled_df, non_scaled_df], axis=1)
        self.log.info("Total Input columns: {0}".format(len(total_df.columns)-1))   # remove target col
        self.log.info("Total Data length: {0}".format(len(total_df)))
        return total_df


class PrepPredDatas(PrepDatas):
    """추론 데이터 인코딩 및 스케일링 진행."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            추론 데이터 인코딩 및 스케일링 진행.
                1. 범주형 features: Binary Encoding
                2. 순서 필요 범주형 features(수치 취급): Ordinal Encoding
                3. 수치형 feautures를 Scaling
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(PrepPredDatas, self).__init__(cfg, log, err_log)
        cfg_p = cfg.PREDICT
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        api_ipt_cvt = unpack(cfg_p.API_IPT_CVT)
        self.api_ipt_cvt_cols = {k: v[0] for k, v in api_ipt_cvt.items()}
        self.api_ipt_cols_type = {v[0]: v[1] for _, v in api_ipt_cvt.items()}
        self.encoder_path = cfg_p.ENCODER_PATH
        self.scaler_path = cfg_p.SCALER_PATH
    
    def __call__(self, input_data):
        """
        Description:
            유해화학물질 데이터 추론 전처리 진행.
            Encoder, Scaler는 학습 전처리 때 저장한 파일 사용.
                1. 범주형 features: Binary Encoding
                2. 순서 필요 범주형 features(수치 취급): Ordinal Encoding
                3. 수치형 feautures를 Scaling
        Args:
            input_data: dict, API로부터 받은 입력 데이터.
        Returns:
            input_df: dataframe, 추론 전처리 완료 데이터
        """
        start_time = time.time()
        self.log.info(">>> Start to Preprocess datas")
        
        self.log.info("Preprocess datas on prediction")
        # 입력데이터 json -> dataframe
        input_df = self.cvt_api_ipt(input_data)
        input_df, nan_indices = self.check_ipt_nan(input_df)
        
        # 입력데이터 칼럼 순서 정리
        input_df = self.select_cols(input_df, pred_mode=True)
        
        # 인코딩 및 스케일링
        encoder_dict = self.load_dict_pkl(self.encoder_path, "Encoder")
        scaler_dict = self.load_dict_pkl(self.scaler_path, "Scaler")
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to Preprocess datas: {0} hr {1} min {2:.2f} sec".format(h, m, s))
        
        return input_df, nan_indices
    
    def cvt_api_ipt(self, input_data):
        """
        Description:
            API로부터 받은 json을 dataframe으로 변환.
        Args:
            input_data: dict, API로부터 받은 추론 입력 데이터.
        Returns:
            input_df: dataframe, 변환한 추론 입력 데이터
        """
        input_df = pd.DataFrame(input_data["datas"])
        input_df = input_df.rename(columns=self.api_ipt_cvt_cols)
        input_df = input_df.astype(self.api_ipt_cols_type)
        self.log.info("Convert API input -> Model raw input ({0} nums)".format(len(input_df)))
        return input_df
    
    def check_ipt_nan(self, input_df):
        """
        Description:
            빈 값 있으면 해당 행 에러 메시지.
            문자열은 양 쪽 공백 없애고 길이 0이면 NaN으로 처리.
        Args:
            input_df: dataframe, 추론 입력 데이터
        Returns:
            input_df: dataframe, NaN 체크한 추론 입력 데이터
            nan_indices: numpy array, NaN 데이터 있는 데이터 행 index들
        """
        for col, typ in self.api_ipt_cols_type.items():
            if typ == "str":
                input_df[col] = input_df[col].str.strip()
                input_df[col] = input_df[col].apply(
                    lambda row: np.nan if len(row) == 0 else row
                )
        check_nan_rows = input_df.apply(lambda row: row.isna().any(), axis=1)
        nan_indices = input_df[check_nan_rows].index
        if len(nan_indices) > 0:
            self.err_log.error("Including Null values: {0}".format(list(nan_indices+1)))
        return input_df, nan_indices
    
    def encode_cols(self, filled_df):
        """
        Description:
            결측 처리 완료된 데이터에서 칼럼별 인코딩.
            Binary Encoding
                - 고용보험업종코드
                - 설립구분명
                - 업종명
            Ordinal Encoding: nan은 -2가 되도록 설정됨
                - 유독물질취급여부
                - 제한물질취급여부
                - 금지물질취급여부
                - 사고대비물질취급여부
                - 공장규모구분명
                - 대기배출시설규모
                - 수질배출시설규모
        Args:
            filled_df: dataframe, 결측 처리 + target 데이터 처리한 데이터
        Returns:
            encoded_df: dataframe, 범주형 칼럼에 대해 인코딩 완료한 데이터
        """
        # 범주형 항목 중 Binary Encoding
        ctgy_df = filled_df[self.ctgy_cols]
        ctgy_df, be_dict = self.use_bn_encoder(ctgy_df)
        
        # 범주형 항목 중 Ordinal Encoding
        ord_df = filled_df[self.yn_cols + self.ord_cols]
        ord_df, oe_dict = self.use_ord_encoder(ord_df)
        
        # Target은 bool -> int
        target_df = filled_df[[self.tgt_final]]
        target_df = self.encode_target(target_df)
        
        # 범주형 항목, target 병합
        encoded_df = pd.concat([ctgy_df, ord_df, target_df], axis=1)
        encoder_dict = concat_dict(be_dict, oe_dict)
        
        return encoded_df
    
    def load_dict_pkl(self, pkl_path, dict_name):
        """
        Description:
            Pickle 파일로 저장된 딕셔너리 불러오기.
        Args:
            pkl_path: str, pickle 파일 저장경로
            dict_name: str, 로그로 남길 딕셔너리 이름
        Returns:
            load_dict: dict, 불러온 딕셔너리
        """
        try:
            load_dict = load_pickle(pkl_path)
        except Exception as e:
            self.log.info("Load {0} incompletely".format(dict_name))
            self.err_log.error("Load {0} incompletely: {1}".format(dict_name, e))
            raise e
        else:
            self.log.info("Load {0} completely".format(dict_name))
            return load_dict
