import time
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader

from utils.log_module import print_elapsed_time
from utils.df_utils import DataframeUtils

import warnings
warnings.filterwarnings(action="ignore")


class RSRAeDataloader(DataframeUtils):
    """Dataloader 생성."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            정상/비정상 데이터 분리하여 dataloader 생성.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(RSRAeDataloader, self).__init__(log, err_log)
        cfg_prep = cfg.PREPROCESS
        cfg = cfg.TRAIN
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.load_jsonpath = cfg_prep.PREP.SAVE_JSONPATH
        self.load_filepath = cfg_prep.PREP.SAVE_FILEPATH
        self.tgt_final = cfg_prep.PREP.TGT_FINAL
        self.normal_ratio = cfg.NORMAL_RATIO
        self.abnormal_ratio = cfg.ABNORMAL_RATIO
        self.random_seed = cfg.HPARAMS.RANDOM_SEED
        self.batch_size = cfg.HPARAMS.BATCH_SIZE
        self.dl_workers = cfg.HPARAMS.DL_WORKERS

    def __call__(self, is_test=False):
        """
        Description:
            정상(사고 X) / 비정상(사고 O) 데이터로 분리.
            학습 / 검증 / 테스트 용으로 데이터 분리 후 dataloader 생성.
                - 학습: 정상 80%
                - 검증: 정상 10%
                - 테스트: 정상 10% + 비정상 100%
        Args:
            is_test: bool, test 모드인지 여부
        Returns:
            학습
                - train_dl: dataloader, 학습 데이터 dataloader
                - valid_dl: dataloader, 검증 데이터 dataloader
                - add_hparams: dict, hparams에 추가할 딕셔너리
            테스트
                - test_input_df: dataframe, test에서만 사용, 테스트용 입력 데이터
                - test_label: numpy array, test에서만 사용, 테스트용 입력 데이터의 label
                - test_dl: dataloader, 테스트 데이터 dataloader
        """
        start_time = time.time()
        self.log.info(">>> Start to Make dataloader")
        
        # Load data
        cols_type = self.load_cols_type(self.load_jsonpath)
        total_df = self.load_csv(self.load_filepath, cols_type)
        
        # Split normal / abnormal
        normal_df, abnormal_df = self.split_normal_abnormal(total_df)
        
        # Split train / valid / test & Make dataloader
        normals = self.split_train_valid_test(normal_df,
                                              self.normal_ratio,
                                              "Normal")
        abnormals = self.split_train_valid_test(abnormal_df,
                                                self.abnormal_ratio,
                                                "Abnormal")

        # Train에는 train, valid dataloader만 생성, Test에는 test dataloader만 생성
        if not is_test:
            train_df = self.merge_datas(normals[0], abnormals[0], "Train")
            valid_df = self.merge_datas(normals[1], abnormals[1], "Valid")
            train_dl = self.make_dataloader(train_df)
            valid_dl = self.make_dataloader(valid_df)
            add_hparams = self.make_add_hparams(cols_type, train_dl)
            
            h, m, s = print_elapsed_time(start_time)
            self.log.info(">>> End to Make dataloader: {0} hr {1} min {2:.2f} sec".format(h, m, s))
            
            return train_dl, valid_dl, add_hparams
            
        else:
            test_input_df, test_label = self.merge_datas(normals[2], abnormals[2], "Test")
            test_dl = self.make_dataloader(test_input_df, shuffle=False)
            
            h, m, s = print_elapsed_time(start_time)
            self.log.info(">>> End to Make dataloader: {0} hr {1} min {2:.2f} sec".format(h, m, s))
            
            return test_input_df, test_label, test_dl

    def split_normal_abnormal(self, total_df):
        """
        Description:
            정상/비정상 데이터로 분리.
            정상(사고 X)은 1, 비정상(사고 O)은 0
        Args:
            total_df: dataframe, 최종 전처리 데이터
        Returns:
            normal_df: dataframe, 정상(사고 X) 데이터
            abnormal_df: dataframe, 비정상(사고 O) 데이터
        """
        input_cols = [col for col in total_df.columns if col != self.tgt_final]
        normal_df = total_df[total_df[self.tgt_final] == False][input_cols]
        abnormal_df = total_df[total_df[self.tgt_final] == True][input_cols]
        self.log.info("Normal datas: {0}".format(len(normal_df)))
        self.log.info("Abnormal datas: {0}".format(len(abnormal_df)))
        return normal_df, abnormal_df
    
    def split_train_valid_test(self, df, ratio, log_txt):
        """
        Description:
            Dataframe을 학습 / 검증 / 테스트 데이터 셋으로 분리.
        Args:
            df: dataframe, 분리할 dataframe
            ratio: float, 분리할 데이터 비율, [0, 1] 구간
            log_txt: str, log에 덧붙일 텍스트
        Returns:
            train_df: dataframe, 학습 데이터
            valid_df: dataframe, 검증 데이터
            test_df: dataframe, 테스트 데이터
        """
        train_ratio, valid_ratio, test_ratio = ratio
        t_vt = train_ratio / (train_ratio + valid_ratio + test_ratio)
        v_t = valid_ratio / (valid_ratio + test_ratio)
        train_df, valid_test_df = self.split_data_using_ratio(df, t_vt)
        valid_df, test_df = self.split_data_using_ratio(valid_test_df, v_t)
        self.log.info("{0} ratio: train {1} / valid {2} / test {3}".format(log_txt, train_ratio, valid_ratio, test_ratio))
        return train_df, valid_df, test_df
        
    def split_data_using_ratio(self, df, ratio):
        """
        Description:
            Dataframe을 ratio에 따라 분리.
        Args:
            df: dataframe, 분리할 dataframe
            ratio: float, 분리할 데이터 비율, [0, 1] 구간
        Returns:
            ratio_df: dataframe, ratio 만큼의 데이터
            residual_df: dataframe, 1 - ratio 만큼의 데이터
        """
        if ratio == 0:
            ratio_df = df.iloc[0:0]
            residual_df = df
        elif ratio == 1:
            ratio_df = df
            residual_df = df.iloc[0:0]
        else:
            ratio_df, residual_df = train_test_split(df,
                                                     train_size=ratio,
                                                     random_state=self.random_seed)
        return ratio_df, residual_df

    def merge_datas(self, normal_df, abnormal_df, log_txt):
        """
        Description:
            학습 전 정상 데이터와 비정상 데이터 병합.
        Args:
            normal_df: dataframe, 정상(사고 X) 데이터
            abnormal_df: dataframe, 비정상(사고 O) 데이터
            log_txt: str, log에 덧붙일 텍스트
        Returns:
            Train, Valid
                - merge_df: dataframe, dataloader 직전 데이터
            Test
                - merge_input_df: dataframe, input data, dataloader 직전 데이터
                - merge_label_df: numpy array, label data
        """
        if log_txt == "Test":
            label_col = "label"
            ipt_cols = list(normal_df.columns)
            normal_df[label_col] = 0
            abnormal_df[label_col] = 1
            merge_df = pd.concat([normal_df, abnormal_df], axis=0)
            merge_df = merge_df.sample(frac=1).reset_index(drop=True)   # Shuffle rows
            self.log.info("{0} datas: {1}".format(log_txt, len(merge_df)))
            return merge_df[ipt_cols], merge_df[label_col].values
        else:
            merge_df = pd.concat([normal_df, abnormal_df], axis=0)
            self.log.info("{0} datas: {1}".format(log_txt, len(merge_df)))
            return merge_df.reset_index(drop=True)
    
    def make_dataloader(self, df, shuffle=True):
        """
        Description:
            Dataloader 생성.
        Args:
            df: dataframe, dataloader로 만들 dataframe
            shuffle: bool, 데이터 순서 섞는 여부 (default=True)
        Returns:
            dataloader: DataLoader, 모델에 사용할 Dataloader
        """
        return DataLoader(df.values.astype(np.float32),
                          batch_size=self.batch_size,
                          shuffle=shuffle,
                          num_workers=self.dl_workers,
                          drop_last=False)
    
    def make_add_hparams(self, cols_type, train_dl):
        """
        Description:
            hparams에 추가할 변수 생성.
        Args:
            cols_type: dict, 데이터 칼럼 별 타입
            train_dl: dataloader, 학습 데이터 dataloader
        Returns:
            add_hparams: dict, hparams에 추가할 딕셔너리
        """
        add_hparams = dict(
            input_dim = len(cols_type.keys()) - 1,   # remove target col
            steps_per_epoch = len(train_dl)
        )
        return add_hparams
    
    
class RSRAePredDataloader(RSRAeDataloader):
    """Dataloader 생성."""
    def __init__(self, cfg, log, err_log):
        """
        Description:
            입력된 데이터로부터 추론 dataloader 생성.
        Args:
            cfg: Namedtuple, YAML 파일에서 읽어온 설정
            log: logging.logger, 로그 객체
            err_log: logging.logger, 에러 로그 객체
        """
        super(RSRAePredDataloader, self).__init__(log, err_log)
        cfg_prep = cfg.PREPROCESS
        cfg = cfg.PREDICT
        self.log, self.err_log = log, err_log
        
        # 설정파일 변수
        self.random_seed = cfg.HPARAMS.RANDOM_SEED
        self.batch_size = cfg.HPARAMS.BATCH_SIZE
        self.dl_workers = cfg.HPARAMS.DL_WORKERS
    
    def __call__(self, pred_input_df):
        """
        Description:
            추론용 데이터로 dataloader 생성.
        Args:
            pred_input_df: dataframe, 추론용 입력 데이터
        Returns:
            pred_dl: dataloader, 추론 데이터 dataloader
        """
        start_time = time.time()
        self.log.info(">>> Start to Make dataloader")
        
        pred_dl = self.make_dataloader(pred_input_df, shuffle=False)
        
        h, m, s = print_elapsed_time(start_time)
        self.log.info(">>> End to Make dataloader: {0} hr {1} min {2:.2f} sec".format(h, m, s))
        
        return pred_dl
