import os
import logging
import argparse
import rsa


def return_dir(is_making=False, *args):
    """
    Description:
        디렉토리 경로 반환.
        해당 경로에 디렉토리가 없으면 생성.
    Args:
        *args: str, 디렉토리 생성할 개별 명칭
    Returns:
        dir_path: str, 디렉토리 경로
    """
    dir_path = os.path.join(*args)
    if not os.path.isdir(dir_path) and is_making:
        os.makedirs(dir_path)
    return dir_path


class RSA(object):
    """RSA 암호화."""
    def __init__(self, cfg, logger=None):
        """초기값."""
        super().__init__()
        self.logger = logger   # logger 모듈
        # self.keysize = 1024
        # self.num_procs = 6
        # self.encoding_method = "utf-8"
        # self.hash_method = "SHA-1"
        # rsa_dir = "./kma_api/api_key"
        # pubkey_file = "rsa_public_key.pem"
        # privkey_file = "rsa_private_key.pem"
        # encryped_file = "encrypted_{0}.bin"   # tail은 사용자입력
        # hash_sign_file = "hash_signature_{0}.bin"   # tail은 사용자입력
        self.keysize = cfg.API.KMA.KEY_SIZE
        self.num_procs = cfg.API.KMA.NUM_PROCS
        self.encoding_method = cfg.API.KMA.ENC_METHOD
        self.hash_method = cfg.API.KMA.HASH_METHOD
        rsa_dir = cfg.API.KMA.RSA_DIR
        pubkey_file = cfg.API.KMA.RSA_PUB_KEY
        privkey_file = cfg.API.KMA.RSA_PRIV_KEY
        encryped_file = cfg.API.KMA.ENCRYPT
        hash_sign_file = cfg.API.KMA.HASH_SIGN
        self.file_tail = cfg.API.KMA.FILE_TAIL
        
        self.rsa_dir = return_dir(True, rsa_dir)
        self.file_head = encryped_file.split("_")[0]
        self.pubkey_path = os.path.join(self.rsa_dir, pubkey_file)
        self.privkey_path = os.path.join(self.rsa_dir, privkey_file)
        self.encryped_path = os.path.join(self.rsa_dir, encryped_file)
        self.hash_sign_path = os.path.join(self.rsa_dir, hash_sign_file)
    
    def __call__(self, mode, file_tail=None):
        """
        Description:
            RSA 이용한 암호화/복호화.
                - RSA key 신규 생성:
                  rsa(mode="newkey")
                - 데이터 암호화 후 저장:
                  req_key = "~~~~~~~~"
                  rsa(mode="encrypt", data=req_key)
                - 저장된 암호 데이터 복호화:
                  req_key = rsa(mode="decrypt")
        Args:
            mode: str, 실행할 종류
                - newkey: 새로 RSA 공개키, 개인키 생성 후 저장
                - encrypt: 데이터 암호화 후 저장, Hash 저장
                - decrypt: 데이터 복호화 후 반환, Hash 검사
            file_tail: str, Decrypt 때만 사용하며 Decrypt할 파일 꼬리 부분
                - None: default로 경로 내 tail을 보여주고 복사해서 써야 함
                - [명칭]: 해당 명칭 파일을 복호화해서 가져 옴
        Returns:
            None: (newkey, encrypt)
            decode_data: str, 복호화된 데이터 (decrypt)
        """
        if mode == "newkey":
            pubkey, privkey = self.make_rsa_keys(self.keysize, self.num_procs)
            self.save_rsakey(pubkey, self.pubkey_path, "public")
            self.save_rsakey(privkey, self.privkey_path, "private")
        elif mode == "encrypt":
            pubkey = self.load_rsakey(self.pubkey_path, "public")
            privkey = self.load_rsakey(self.privkey_path, "private")
            data = self.check_secure_data()
            tail = self.check_filename_tail(mode)
            self.save_encrypted_data(data, pubkey, self.encryped_path.format(tail))
            self.save_hashsign(data, privkey, self.hash_method, self.hash_sign_path.format(tail))
        elif mode == "decrypt":
            pubkey = self.load_rsakey(self.pubkey_path, "public")
            privkey = self.load_rsakey(self.privkey_path, "private")
            if file_tail is None:
                tail = self.check_filename_tail(mode)
            else:
                tail = file_tail
            decode_data = self.load_decrypted_data(privkey, self.encryped_path.format(tail))
            hash_method = self.verify_hashsign(decode_data, pubkey, self.hash_sign_path.format(tail))
            if hash_method == self.hash_method:
                return decode_data
            else:
                self.print_str("Another hash method: {0} (Now) / {1} (Setting)".format(hash_method, self.hash_method))
                return decode_data
        else:
            self.print_str("You can use 'newkey' or 'encrypt' or 'decrypt' on mode")
    
    def print_str(self, string, loglevel="info"):
        """
        Description:
            스트림 로그 출력 또는 단순 print.
        Args:
            string: str, 출력할 문자열
        Returns:
            None
        """
        if isinstance(self.logger, logging.Logger):
            getattr(self.logger, loglevel)(string)
        else:
            print(string)

    def make_rsa_keys(self, keysize=1024, num_procs=6):
        """
        Description:
            새로운 RSA 공개키, 개인키 생성.
        Args:
            keysize: int, Key의 크기 (default=1024)
            num_procs: int, Key 생성시 사용할 프로세스 수 (default=6)
        Returns:
            pubkey: PublicKey, RSA 공개키
            privkey: PrivateKey, RSA 개인키
        """
        pubkey, privkey = rsa.newkeys(keysize, poolsize=num_procs)
        return pubkey, privkey
    
    def save_rsakey(self, key, saved_path, kinds):
        """
        Description:
            RSA 공개키, 개인키 .pem으로 저장.
        Args:
            key: PublicKey or PrivateKey, 저장할 RSA Key
            saved_path: str, 저장할 경로
            kinds: str, private or public
        Returns:
            None
        """
        try:
            with open(saved_path, mode="wb") as fp:
                fp.write(key.save_pkcs1(format="PEM"))
        except Exception as e:
            self.print_str("Error: {0}".format(e), loglevel="error")
        else:
            self.print_str("Success to save rsa key ({0})".format(kinds))
    
    def load_rsakey(self, saved_path, kinds):
        """
        Description:
            RSA 공개키, 개인키 불러오기.
        Args:
            saved_path: str, 저장된 경로
            kinds: str, private or public
        Returns:
            key: PublicKey or PrivateKey, 불러온 RSA Key
        """
        key = None
        try:
            with open(saved_path, mode="rb") as fp:
                keydata = fp.read()
        except Exception as e:
            self.print_str("Error: {0}".format(e), loglevel="error")
        else:
            if kinds == "private":
                key = rsa.PrivateKey.load_pkcs1(keydata)
            elif kinds == "public":
                key = rsa.PublicKey.load_pkcs1(keydata)
            else:
                key = None
        finally:
            if key is None:
                self.print_str("Use 'private' or 'public' on kinds argument")
            else:
                self.print_str("Success to load rsa key ({0})".format(kinds))
            return key
    
    def check_secure_data(self):
        """
        Description:
            암호화하여 저장할 데이터 입력 및 확인.
        Args:
            None
        Returns:
            data: str, 암호화하여 저장할 데이터
        """
        data = ""
        while len(data) < 2:
            data = input("Write your secure key: ")
            data = data.strip()
            data_yn = input("Is it input data length '{0}' correct ? [y/n] ".format(len(data)))
            if data_yn == "y":
                continue
            else:
                data = ""
                self.print_str("Write correct key once more")
        return data
    
    def check_filename_tail(self, mode):
        """
        Description:
            저장할/불러올 파일 이름 뒷부분 입력 및 확인.
            불러올 때는 디렉토리 내 저장된 파일의 tail을 먼저 확인 후 진행.
        Args:
            mode: str, 실행할 종류 (encrypt, decrypt)
        Returns:
            tail: str, 파일 이름 뒷부분
        """
        # 저장된 암호화 데이터의 tail만 추출하여 확인
        if mode == "decrypt":
            tail_kinds = [
                "_".join(elm.rstrip(".bin").split("_")[1:])
                for elm
                in os.listdir(self.rsa_dir)
                if elm.startswith(self.file_head)
            ]
            self.print_str("----- Tails of Encrypted file list:")
            for t in tail_kinds:
                self.print_str("-------> {0}".format(t))
        
        CHECK = True
        while CHECK:
            tail = input("Write your tail of filename: ")
            tail = tail.strip()
            if mode == "encrypt":
                tail_yn = input("Is it tail of filename '{0}' correct? [y/n] ".format(tail))
                if tail_yn == "y":
                    CHECK = False
                else:
                    self.print_str("Write correct tail of filename once more")
            elif mode == "decrypt":
                if tail in tail_kinds:
                    CHECK = False
                else:
                    self.print_str("Write correct tail of filename once more")
        return tail
    
    def save_encrypted_data(self, data, pubkey, saved_path):
        """
        Description:
            입력 데이터를 RSA 암호화 후 bin으로 저장.
        Args:
            data: str, 암호화할 데이터
            pubkey: PublicKey, RSA 공개키
            saved_path: str, 저장할 경로
        Returns:
            None
        """
        encode_data = data.encode(self.encoding_method)
        encrypted_data = rsa.encrypt(encode_data, pubkey)
        try:
            with open(saved_path, mode="wb") as fp:
                fp.write(encrypted_data)
        except Exception as e:
            self.print_str("Error: {0}".format(e), loglevel="error")
        else:
            self.print_str("Success to save encrypted data")
    
    def load_decrypted_data(self, privkey, saved_path):
        """
        Description:
            RSA 암호화된 데이터를 불러오고 복호화.
        Args:
            privkey: PrivateKey, RSA 개인키
            saved_path: str, 저장된 경로
        Returns:
            decode_data: str, 복호화한 데이터
        """
        try:
            with open(saved_path, mode="rb") as fp:
                encrypted_data = fp.read()
        except Exception as e:
            decode_data = None
            self.print_str("Error: {0}".format(e), loglevel="error")
        else:
            decrypted_data = rsa.decrypt(encrypted_data, privkey)
            decode_data = decrypted_data.decode(self.encoding_method)
            self.print_str("Success to load decrypted data")
        finally:
            return decode_data
    
    def save_hashsign(self, data, privkey, hash_method, saved_path):
        """
        Description:
            입력 데이터를 RSA 암호화 후 bin으로 저장.
        Args:
            data: str, 암호화할 데이터
            privkey: PrivateKey, RSA 개인키
            hash_method: str, Hash 알고리즘
                         MD5, SHA-1, SHA-224, SHA-256, SHA-384, SHA-512
            saved_path: str, 저장할 경로
        Returns:
            None
        """
        encode_data = data.encode(self.encoding_method)
        signature = rsa.sign(encode_data, privkey, hash_method)
        try:
            with open(saved_path, mode="wb") as fp:
                fp.write(signature)
        except Exception as e:
            self.print_str("Error: {0}".format(e), loglevel="error")
        else:
            self.print_str("Success to save hash-signature")
    
    def verify_hashsign(self, data, pubkey, saved_path):
        """
        Description:
            RSA 암호화된 데이터를 불러오고 복호화.
        Args:
            data: str, 암호화할 데이터
            pubkey: PublicKey, RSA 공개키
            saved_path: str, 저장된 경로
        Returns:
            hash_method: str, Hash 알고리즘
        """
        encode_data = data.encode(self.encoding_method)
        try:
            with open(saved_path, mode="rb") as fp:
                signature = fp.read()
            hash_method = rsa.verify(encode_data, signature, pubkey)
        except Exception as e:
            hash_method = None
            self.print_str("Error: {0}".format(e), loglevel="error")
        else:
            self.print_str("Success to verify")
        finally:
            return hash_method


if __name__ == "__main__":
    # Mode 종류
    #   - newkey: RSA 암호키 신규 생성
    #   - encrypt: 암호화
    #   - decrypt: 복호화
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m", "--mode", type=str,
        help="executition mode",
        choices=["newkey", "encrypt", "decrypt"]
    )
    args = parser.parse_args()
    
    rsa_ = RSA()
    result = rsa_(mode=args.mode)
    
    if args.mode == "decrypt":
        print(result)
